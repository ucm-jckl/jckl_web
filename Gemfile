source 'https://rubygems.org'

# Rails core
gem 'rails', '~> 5.0.0'

# Database connections
gem 'mysql2' # local db
gem 'sqlite3', '~> 1.3', '< 1.4', group: [:development, :test]
gem 'pg' # for Sierra connections

# Asset pipeline
gem 'sass-rails'
gem 'autoprefixer-rails'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'

# Assets
gem 'jquery-rails'
gem 'font-awesome-rails'
gem 'lazy_high_charts'
gem 'jquery-ui-rails'
gem 'chosen-rails'


# JS/CSS assets from Bower using rails-assets
gem 'rails-assets-bootstrap', '4.0.0.beta', source: 'https://rails-assets.org'
gem 'rails-assets-readmore', source: 'https://rails-assets.org'
gem 'rails-assets-snap.svg', source: 'https://rails-assets.org'
gem 'rails-assets-svg-pan-zoom', source: 'https://rails-assets.org'
gem 'rails-assets-owl.carousel', source: 'https://rails-assets.org'
gem 'rails-assets-highcharts', source: 'https://rails-assets.org'
gem 'rails-assets-chroma-js', source: 'https://rails-assets.org'
gem 'rails-assets-bootstrap-datepicker', source: 'https://rails-assets.org'
gem 'rails-assets-bootstrap-table', source: 'https://rails-assets.org'
gem 'rails-assets-tableexport.jquery.plugin', source: 'https://rails-assets.org'
gem 'rails-assets-momentjs', source: 'https://rails-assets.org'
gem 'rails-assets-lodash', source: 'https://rails-assets.org'
gem 'rails-assets-blazy', source: 'https://rails-assets.org'



# Administration and authentication tools
gem 'activeadmin', '1.0.0'
gem 'draper', '> 3.x'
gem 'formtastic'
gem 'formtastic-bootstrap'
gem 'paperclip'
gem 'active_admin-sortable_tree'
gem 'ancestry'
gem 'devise', '>= 4.2.0'
gem 'omniauth-google-oauth2'

# Screen scraping and crawling
gem 'nokogiri' # screen scraping
gem 'fastimage' # checking image size
gem 'spidr'


# Search and Solr indexing
gem 'summon'
gem 'rsolr'

# Other
gem 'delayed_job'
gem 'delayed_job_active_record'
gem 'daemons'
gem 'email_validator'


# Development tools
group :development, :test do
  gem 'capybara'
  gem 'poltergeist'
  gem 'byebug'
  gem 'pry'
  gem 'timecop'
end

group :development do
  gem 'rack-livereload'
  gem 'web-console'
  gem 'bullet'
  gem 'active_record_query_trace'
  gem 'rb-fsevent'
  gem 'wdm', '>= 0.1.0' if Gem.win_platform?
  gem 'certified'
  gem 'bootsnap'
  gem 'capistrano', '~> 3.6'
  gem 'capistrano-rails', '~> 1.2'
  gem 'capistrano-rbenv', '~> 2.0'
  gem 'capistrano-passenger'
  gem 'capistrano-secrets-yml', '~> 1.0.0'
  gem 'capistrano3-delayed-job', '~> 1.0'
  gem 'capistrano-file-permissions'
end

# Miscellaneous
gem 'dalli' # caching with memcached
gem 'chronic' # time functions
gem 'chronic_duration' # time duration functions
gem 'activerecord-import' # bulk insert SQL
gem 'rack-cors' # allow cross-domain requests
gem 'lcsort' # working with Library of Congress numbers
gem 'exception_notification' # email notifications for errors
gem 'json'
gem 'rest-client' # working with remote APIs
gem 'httparty'
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'google-api-client'
gem 'fixtures_dumper'
