
[1mFrom:[0m /mnt/c/Users/welker/git/jckl_web/app/jobs/build_new_books_collection_job.rb @ line 47 BuildNewBooksCollectionJob#delete_old_collections:

    [1;34m26[0m: [32mdef[0m [1;34mdelete_old_collections[0m(collections)
    [1;34m27[0m:   [1;34m# Get the last 12 months in YYYY-mm format[0m
    [1;34m28[0m:   months = []
    [1;34m29[0m:   pointer_month = [1;34;4mDate[0m.today.beginning_of_month
    [1;34m30[0m:   [1;34m12[0m.times [32mdo[0m
    [1;34m31[0m:     months << pointer_month.strftime([31m[1;31m'[0m[31m%Y-%m[1;31m'[0m[31m[0m)
    [1;34m32[0m:     pointer_month = pointer_month - [1;34m1[0m.month
    [1;34m33[0m:   [32mend[0m
    [1;34m34[0m: 
    [1;34m35[0m:   [1;34m# If a collection's month isn't in the list of last 12 months, remove bibs and delete collection[0m
    [1;34m36[0m:   collections.each [32mdo[0m |collection|
    [1;34m37[0m:     pid = collection.dig([31m[1;31m'[0m[31mpid[1;31m'[0m[31m[0m, [31m[1;31m'[0m[31m__content__[1;31m'[0m[31m[0m)
    [1;34m38[0m:     collection_month = collection[[31m[1;31m'[0m[31mname[1;31m'[0m[31m[0m].gsub([31m[1;31m'[0m[31mNew Resources [1;31m'[0m[31m[0m, [31m[1;31m'[0m[31m[1;31m'[0m[31m[0m)
    [1;34m39[0m:     [32mif[0m months.include?(collection_month) || [1;36mtrue[0m
    [1;34m40[0m:       [1;34m# First remove bibs from the collection[0m
    [1;34m41[0m:       loop [32mdo[0m
    [1;34m42[0m:         bibs = [1;34;4mAlmaApiService[0m.request([31m[1;31m'[0m[31mGET[1;31m'[0m[31m[0m, [31m[1;31m'[0m[31m/almaws/v1/bibs/collections/[1;31m'[0m[31m[0m + pid + [31m[1;31m'[0m[31m/bibs[1;31m'[0m[31m[0m, {[31m[1;31m'[0m[31mlimit[1;31m'[0m[31m[0m => [1;34m100[0m})
    [1;34m43[0m:         [32mbreak[0m [32mif[0m bibs.dig([31m[1;31m'[0m[31mbibs[1;31m'[0m[31m[0m, [31m[1;31m'[0m[31mtotal_record_count[1;31m'[0m[31m[0m) == [31m[1;31m'[0m[31m0[1;31m'[0m[31m[0m
    [1;34m44[0m:         bibs.dig([31m[1;31m'[0m[31mbibs[1;31m'[0m[31m[0m, [31m[1;31m'[0m[31mbib[1;31m'[0m[31m[0m)&.each [32mdo[0m |bib|
    [1;34m45[0m:           puts [31m[1;31m'[0m[31mdeleting [1;31m'[0m[31m[0m + bib[[31m[1;31m'[0m[31mmms_id[1;31m'[0m[31m[0m]
    [1;34m46[0m:           response = [1;34;4mAlmaApiService[0m.request([31m[1;31m'[0m[31mDELETE[1;31m'[0m[31m[0m, [31m[1;31m'[0m[31m/almaws/v1/bibs/collections/[1;31m'[0m[31m[0m + pid + [31m[1;31m'[0m[31m/bibs/[1;31m'[0m[31m[0m + bib[[31m[1;31m'[0m[31mmms_id[1;31m'[0m[31m[0m])
 => [1;34m47[0m:           binding.pry
    [1;34m48[0m:         [32mend[0m
    [1;34m49[0m:       [32mend[0m
    [1;34m50[0m:       [1;34m# Then we can delete the collection[0m
    [1;34m51[0m:       [1;34;4mAlmaApiService[0m.request([31m[1;31m'[0m[31mDELETE[1;31m'[0m[31m[0m, [31m[1;31m'[0m[31m/almaws/v1/bibs/collections/[1;31m'[0m[31m[0m + pid)
    [1;34m52[0m:     [32mend[0m
    [1;34m53[0m:   [32mend[0m
    [1;34m54[0m: [32mend[0m

