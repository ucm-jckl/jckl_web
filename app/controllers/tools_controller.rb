class ToolsController < ApplicationController

  protect_from_forgery unless: -> { request.format.json? }

  def callnumber_parser
    if params[:callnumbers_input].present?
      @callnumbers = []
      raw_callnumbers = params[:callnumbers_input].split("\n").map{|c| c.strip }
      raw_callnumbers.each do |c|
        next if c.blank?
        @callnumbers << {
          raw: c,
          clean: Book.stripped_call_number(c),
          sortable: Book.normalized_call_number(c),
          class: Book.call_number_class(c),
          class_number: Book.call_number_class_number(c)
        }
      end
    end

    @breadcrumb = ['Tools', 'Call Number Parser']
    @page_title = 'Call Number Parser'

    render
  end


  def calendar_sync
    # Create a hash map of team members
    team_members = {}
    params.dig('team', 'value')&.each do |m|
      team_members[m['id']] = {
        email: m['mail'],
        tasks: []
      }
    end

    # Create a hash map of tasks combined with task details
    tasks = {}
    params['tasks']&.each do |t|
      tasks[t['id']] = t.to_hash
    end
    params['taskDetails']&.each do |t|
      tasks[t['id']]['details'] = t.to_hash
    end

    # Merge tasks onto the team member hash
    tasks.each do |id, t|
      t['assignments']&.each do |k, v|
        next unless team_members[k].present?
        team_members[k][:tasks] << t
      end
    end

    # Queue a job to add to calendars
    team_members.each{|id, member| SyncPlannerToCalendarJob.perform_later(member) }

    render plain: "Success"
  end

end
