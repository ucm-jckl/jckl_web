class StatsController < ApplicationController
  def dashboard
    @start_time = params[:start_time] || (Time.now - 3.months).beginning_of_day.strftime('%Y-%m-%d %I:%M %P')
    @end_time = params[:end_time] || Time.now.end_of_day.strftime('%Y-%m-%d %I:%M %P')
    @interval = params[:interval] || 'day'
    @grouping = params[:grouping] || 'all'
    @page_title = 'JCKL Statistics Dashboard'
    @page_description = 'Statistics showing how students use the James C. Kirkpatrick Library.'
    #set_layout_option(:include_fixed_width_container, false)

    render
  end



  def headcount_form
    return access_denied unless current_user&.employee?

    @form = HeadcountForm.new(params[:headcount_form]&.permit!)
    @form.headcount_date ||= Chronic.parse(params[:date])&.to_date
    @form.headcount_date ||= Date.today

    if request.method == 'POST'
      @clear = params[:clear].present?
      if @clear
        @form.save(true)
      else
        @form.save(false)
      end
    end

    @page_title = 'Headcount Entry Form'
    set_layout_option(:include_fixed_width_container, false)
    render
  end

  def headcounts
    @start_time = Chronic.parse(params[:start_time]) || (Time.zone.now - 3.months).beginning_of_day
    @end_time = Chronic.parse(params[:end_time]) || Time.zone.now.end_of_day
    @interval = params[:interval] || 'days'
    @grouping = params[:grouping] || 'all'
    if %w(hour_averages weekday_averages weekday_hour_averages).include?(@interval)
      aggregation = 'avg'
    else
      aggregation = 'sum'
    end

    @highcharts_data = HeadcountEntry.to_highcharts(@start_time, @end_time, @interval, @grouping, aggregation)

    @chart = LazyHighCharts::HighChart.new('graph') do |f|
      f.chart(
        type: aggregation == 'avg' ? 'column' : 'line'
      )
      f.title(text: "JCKL Headcounts by #{unless @grouping == 'all' then @grouping.capitalize + ' and ' end} #{HighchartsDataSource::INTERVALS[@interval.to_sym]}")
      f.subtitle(text: "#{@start_time.to_s(:long_12hr)} to #{@end_time.to_s(:long_12hr)}")
      f.xAxis(
        type: @highcharts_data[:xAxis][:type],
        categories: @highcharts_data[:xAxis][:categories]
      )
      f.yAxis([{
        title: {
          text: "Headcount #{'(average)' if aggregation == 'avg'}"
        },
        min: 0
      }])

      @highcharts_data[:series].each do |series|
        f.series(name: series[:name], yAxis: 0, data: series[:data], turboThreshold: 0)
      end

      f.legend(
        layout: 'horizontal',
        enabled: @highcharts_data[:series].length != 1
      )
    end

    @table = HeadcountEntry.to_data_table(@highcharts_data, @start_time, @end_time, @interval, aggregation)
    @page_title = 'Headcount Statistics'
    set_layout_option(:include_fixed_width_container, false)
    render
  end




  def studyrooms
    @start_time = Chronic.parse(params[:start_time]) || (Time.zone.now - 3.months).beginning_of_day
    @end_time = Chronic.parse(params[:end_time]) || Time.zone.now.end_of_day

    respond_to do |f|
      f.html do
        @hours_data = StudyRoomBooking.hours_used_per_time_period(@start_time, @end_time, 'days')
        @percent_data = StudyRoomBooking.percent_used_per_time_period(@start_time, @end_time, 'days')
        @rooms_data = StudyRoomBooking.hours_used_per_room(@start_time, @end_time)
        @page_title = 'Study Room Usage Statistics'
        set_layout_option(:include_fixed_width_container, false)
        render
      end
      f.json do
        data = nil
        if params[:chart] == 'hours-used-chart'
          data = StudyRoomBooking.hours_used_per_time_period(@start_time, @end_time, params[:interval])
        elsif params[:chart] == 'percent-used-chart'
          data = StudyRoomBooking.percent_used_per_time_period(@start_time, @end_time, params[:interval])
        end
        render json: data.to_json
      end
    end
  end


  def computers
    @start_time = Chronic.parse(params[:start_time]) || (Time.zone.now - 3.months).beginning_of_day
    @end_time = Chronic.parse(params[:end_time]) || Time.zone.now.end_of_day
    @interval = params[:interval] || 'days'
    @grouping = params[:grouping] || 'all'
    if %w(hour_averages weekday_averages weekday_hour_averages).include?(@interval)
      aggregation = 'avg'
    else
      aggregation = 'sum'
    end

    @highcharts_data = Computer.to_highcharts(@start_time, @end_time, @interval, @grouping, aggregation)

    @chart = LazyHighCharts::HighChart.new('graph') do |f|
      f.chart(
        type: aggregation == 'avg' ? 'column' : 'line'
      )
      f.title(text: "JCKL Computer Usage by #{unless @grouping == 'all' then @grouping.capitalize + ' and ' end} #{HighchartsDataSource::INTERVALS[@interval.to_sym]}")
      f.subtitle(text: "#{@start_time.to_s(:long_12hr)} to #{@end_time.to_s(:long_12hr)}")
      f.xAxis(
        type: @highcharts_data[:xAxis][:type],
        categories: @highcharts_data[:xAxis][:categories]
      )
      f.yAxis([{
        title: {
          text: "Minutes Used #{'(average)' if aggregation == 'avg'}"
        },
        min: 0
      }])

      @highcharts_data[:series].each do |series|
        f.series(name: series[:name], yAxis: 0, data: series[:data], turboThreshold: 0)
      end

      f.legend(
        layout: 'horizontal',
        enabled: @highcharts_data[:series].length != 1
      )
    end

    @table = Computer.to_data_table(@highcharts_data, @start_time, @end_time, @interval, aggregation)
    @page_title = 'Computer Usage Statistics'
    set_layout_option(:include_fixed_width_container, false)
    render
  end

  private

  def chart_globals
    minTickInterval = case @interval
                      when 'days'
                        1.day.to_i * 1000
                      when 'weeks'
                        1.week.to_i * 1000
                      when 'months'
                        1.month.to_i * 1000
                      when 'years'
                        1.year.to_i * 1000
    end

    LazyHighCharts::HighChartGlobals.new do |f|
      f.global(useUTC: false)
      f.lang(thousandsSep: ',')
      f.credits(enabled: false)
      f.chart(
        zoomType: 'xy',
        panning: true,
        panKey: 'shift'
      )
      f.xAxis(
        dateTimeLabelFormats: {
          day: '%B %e',
          week: '%B %e'
        },
        minTickInterval: minTickInterval,
        tickPixelInterval: 20
      )
    end
  end
  helper_method :chart_globals
end
