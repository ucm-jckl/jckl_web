class AbsencesController < ApplicationController

  skip_before_action :verify_authenticity_token

  def by_date
    date = Chronic.parse(params[:date])&.to_date
    date ||= Time.zone.now.to_date
    @absence_dates = AbsenceDate.includes(absence: :employee).where(absence_date: date).where.not(absences: { approved: false})

    @page_title = "Library Absences: #{date}"
    @breadcrumb = ["<a href='#{portal_employees_url}'>Employee Portal</a>", @page_title]
    render
  end


  def index
    @employee = Employee.includes(:supervisor).find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @start_date = Chronic.parse(params[:start_date])&.to_date || Chronic.parse("last july 1").to_date
    @end_date = Chronic.parse(params[:end_date])&.to_date || Time.now.to_date + 1.year

    @absences = Absence.includes(:absence_type, :absence_dates, :employee)
        .where(employee_id: @employee.id)
        .order(created_at: :desc)
        .to_a.select{|a| a.has_absence_dates_between?(@start_date, @end_date)}

    @page_title = "Absence Requests: #{@employee.first_last}"
    @breadcrumb = ["<a href='#{portal_employees_url}'>Employee Portal</a>", @page_title]
    render
  end


  def show
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @absence = Absence.includes(:submitter, :employee).find_by(id: params[:id])
    return not_found if @absence.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@absence.employee)

    @page_title = "Absence Request: #{@absence.employee.first_last} - #{@absence.date_range}"
    @breadcrumb = ["<a href='#{portal_employees_url}'>Employee Portal</a>", "<a href='#{employee_absences_url(name: @absence.employee.url_slug)}'>Absences: #{@absence.employee.first_last}</a>", @page_title]
    render
  end


  def new
    @employee = Employee.find_by_url_slug(params[:employee_name]) || current_user.employee
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @absence = Absence.new
    @absence.employee = @employee
    @absence_date_list = params[:absence_dates]&.split('|').to_a
    @absence_date_list.each do |date|
      @absence.absence_dates.new(absence_date: date, is_all_day: true)
    end
    @absence.absence_dates_list = @absence_date_list.join("\n")

    @submit_for = @employee.subordinates.order(:last_name, :first_name)
    if current_user.employee.library_administrator?
      @submit_for = Employee.all.order(:last_name, :first_name)
    end

    @page_title = 'Submit an Absence Request'
    @breadcrumb = ["<a href='#{portal_employees_url}'>Employee Portal</a>", "<a href='#{employee_absences_url(employee_name: @absence.employee.url_slug)}'>Absences: #{@absence.employee.first_last}</a>", @page_title]
    render
  end


  def create
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @absence = Absence.new(params.require(:absence).permit!)
    @absence.employee = @employee
    @absence.submitter = current_user.employee
    @absence_date_list = @absence.absence_dates.map{|d| d.absence_date}
    @absence.absence_dates_list = @absence_date_list.to_a.join("\n")

    @submit_for = @employee.subordinates.order(:last_name, :first_name)
    if current_user.employee.library_administrator?
      @submit_for = Employee.all.order(:last_name, :first_name)
    end

    @absence.is_new = true

    if @absence.save
      AbsencesMailer.send_email(@absence).deliver_now
      flash[:notice] = 'Your absence request was submitted. You should receive confirmation via email.'
      return redirect_to employee_absences_url(employee_name: @absence.employee.url_slug)
    else
      @page_title = 'Submit an Absence Request'
      @breadcrumb = ["<a href='#{portal_employees_url}'>Employee Portal</a>", "<a href='#{employee_absences_url(name: @absence.employee.url_slug)}'>Absences: #{@absence.employee.first_last}</a>", @page_title]
      render :new
    end
  end


  def edit
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @absence = Absence.find_by(id: params[:id])
    return not_found if @absence.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@absence.employee)

    @absence_date_list = (((params[:absence_dates]&.split('|')&.map{|d| Chronic.parse(d).to_date} || []) + @absence.absence_dates.pluck(:absence_date)).to_a).uniq
    @absence_date_list.each do |date|
      @absence.absence_dates.new(absence_date: date) if @absence.absence_dates.find_by(absence_date: date).blank?
    end
    @absence.absence_dates_list = @absence_date_list.join("\n")

    @page_title = "Edit Absence Request: #{@absence.employee.first_last} - #{@absence.date_range}"
    @breadcrumb = ["<a href='#{portal_employees_url}'>Employee Portal</a>", "<a href='#{employee_absences_url(name: @absence.employee.url_slug)}'>Absences: #{@absence.employee.first_last}</a>", @page_title]
    render
  end


  def update
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@employee)

    @absence = Absence.includes(:employee).find_by(id: params[:id])
    return not_found if @absence.blank?
    return access_denied unless current_user.employee.can_view_absences_of?(@absence.employee)

    @absence.absence_dates.each do |d|
      d.original_data = d.attributes
    end

    initial_checksum = @absence.absence_dates.map{|d| [d.absence_date, d.start_time || Time.at(0), d.end_time || Time.at(0)]}&.flatten&.sort&.to_s
    @absence.assign_attributes(params.require(:absence).permit!)
    @absence.absence_dates.each do |d|
      d.duration_minutes_will_change!
    end

    result_checksum = @absence.absence_dates.map{|d| [d.absence_date, d.start_time || Time.at(0), d.end_time || Time.at(0)]}&.flatten&.sort&.to_s
    if initial_checksum != result_checksum
      if @absence.approved == true
        @absence.was_previously_approved = true
      elsif @absence.approved == false
        @absence.was_previously_rejected = true
      end
      @absence.approved = nil
      @absence.times_have_been_altered = true
    end

    @absence.editor = current_user.employee
    @absence.is_edited = true

    if @absence.save
      AbsencesMailer.send_email(@absence).deliver_now
      flash[:notice] = 'Your absence request was amended. You should receive confirmation via email.'
      return redirect_to employee_absences_url(employee_name: @absence.employee.url_slug)
    else
      @absence_date_list = @absence.absence_dates.map{|d| d.absence_date}
      params[:absence][:absence_dates_attributes].each do |k,ad|
        @absence_date_list << Chronic.parse(ad[:absence_date]).to_date
      end
      @absence_date_list.sort!.uniq!

      @absence.absence_dates_list = @absence_date_list.join("\n")
      @page_title = "Edit Absence Request: #{@absence.employee.first_last} - #{@absence.date_range}"
      @breadcrumb = ["<a href='#{employee_absences_url(name: @absence.employee.url_slug)}'>Absences: #{@absence.employee.first_last}</a>", @page_title]
      render :edit
    end
  end


  def destroy
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_approve_absences_of?(@employee)

    @absence = Absence.includes(:employee).find_by(id: params[:id])
    return not_found if @absence.blank?
    return access_denied unless current_user.employee.can_approve_absences_of?(@absence.employee)

    employee = @absence.employee
    @absence.is_deleted = true
    AbsencesMailer.send_email(@absence).deliver_now
    @absence.destroy!
    flash[:notice] = 'This absence request was deleted. You should receive confirmation via email.'
    redirect_to employee_absences_url(employee_name: employee.url_slug)
  end


  def approve
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_approve_absences_of?(@employee)

    @absence = Absence.includes(:employee).find_by(id: params[:id])
    return not_found if @absence.blank?
    return access_denied unless current_user.employee.can_approve_absences_of?(@absence.employee)

    @absence.approved = true
    @absence.reject_note = nil
    @absence.is_approved = true
    @absence.approver = current_user.employee
    if @absence.save
      flash[:notice] = 'This absence request was approved. You should receive confirmation via email.'
      AbsencesMailer.send_email(@absence).deliver_now
    else
      flash[:alert] = 'There was an error approving this absence request.'
    end
    redirect_to employee_absence_url(employee_name: @absence.employee.url_slug, id: @absence.id)
  end


  def reject
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_approve_absences_of?(@employee)

    @absence = Absence.includes(:employee).find_by(id: params[:id])
    return not_found if @absence.blank?
    return access_denied unless current_user.employee.can_approve_absences_of?(@absence.employee)

    @absence.approved = false
    @absence.approver = current_user.employee
    @absence.reject_note = params.dig(:absence, :reject_note)
    @absence.is_rejected = true
    if @absence.save
      flash[:notice] = 'This absence request was rejected. You should receive confirmation via email.'
      AbsencesMailer.send_email(@absence).deliver_now
    else
      flash[:alert] = 'There was an error rejecting this absence request.'
    end
    redirect_to employee_absence_url(employee_name: @absence.employee.url_slug, id: @absence.id)
  end


  def receive_note
    @employee = Employee.find_by_url_slug(params[:employee_name])
    return not_found if @employee.blank?
    return access_denied unless current_user.employee.can_approve_absences_of?(@employee)

    @absence = Absence.includes(:employee).find_by(id: params[:id])
    return not_found if @absence.blank?
    return access_denied unless current_user.employee.can_approve_absences_of?(@absence.employee)

    @absence.note_received = true
    @absence.is_note_received = true
    if @absence.save
      flash[:notice] = 'This absence request received a doctor\'s note.'
    else
      flash[:alert] = 'There was an error receiving a doctor\'s note for this absence request.'
    end
    redirect_to employee_absence_url(employee_name: @absence.employee.url_slug, id: @absence.id)
  end


  private


  def check_if_employee
    authenticate_user!
    access_denied unless current_user&.employee?
  end
  before_action :check_if_employee
end
