class AssetsController < ApplicationController

  skip_before_filter :verify_authenticity_token

  def css
    filename = params[:file]
    filename = 'application' if filename == 'styles'

    asset_path = ActionController::Base.helpers.asset_path("#{filename}.css")
    redirect_to asset_path
  end

  def js
    filename = params[:file]
    filename = 'application' if filename == 'main'

    asset_path = ActionController::Base.helpers.asset_path("#{filename}.js")
    redirect_to asset_path
  end

  def img
    filename = params[:file]

    asset_path = ActionController::Base.helpers.asset_path("#{filename}")
    redirect_to asset_path
  end


end
