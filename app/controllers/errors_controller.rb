class ErrorsController < ApplicationController

  skip_before_filter :verify_authenticity_token, only: [:ezproxy]

  def not_found
    @page_title = 'Page Not Found'
    render(status: 404)
  end

  def unprocessable
    @page_title = 'Could Not Process Request'
    render(status: 422)
  end

  def internal_server_error
    @page_title = 'Internal Server Error'
    render(status: 500)
  end

  def unauthorized
    @page_title = 'Unauthorized'
    render(status: 401)
  end

  def ezproxy
    EzproxyMailer.send_email(params[:host], params[:link], params[:referrer], request.remote_ip).deliver_now
    render json: {success: 'success'}
  end
end
