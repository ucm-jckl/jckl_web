class IndexController < ApplicationController
  def index
    @page_title = 'Home'
    @page_description = 'James C. Kirkpatrick Library home page.'
    set_layout_option(:include_breadcrumb, false)
    set_layout_option(:include_fixed_width_container, false)

    @bg_image = Random.new.rand(1..3)

    render
  end

  def sitemap
    @page_title = 'Sitemap'
    @page_description = 'View a listing of all pages for the James C. Kirkpatrick Library.'
    respond_to do |format|
      format.html { render }
      format.xml { render layout: false }
    end
  end
end
