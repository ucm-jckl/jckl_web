class ApiController < ApplicationController

  def nav
    tree = {}
    NavigationMenuItem.roots.order(:position).each do |root|
      tree[root.label] = {}
      root.children.order(:position).each do |child1|
        tree[root.label][child1.label] = {}
        child1.children.order(:position).each do |child2|
          tree[root.label][child1.label][child2.label] = child2.url
        end
      end
    end

    render json: tree
  end


  def ucm_header
    render file: Rails.application.root.to_s + '/tmp/cache/ucm_header.html', layout: false
  end

  def ucm_footer
    render file: Rails.application.root.to_s + '/tmp/cache/ucm_footer.html', layout: false  end

end
