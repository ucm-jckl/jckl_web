class ClfController < ApplicationController

  def books_feed
    newest_festival_year = ClfBook.all.order('festival_year desc').first.festival_year
    books = ClfBook.where(festival_year: newest_festival_year)
    render json: books
  end


  def index
    @order = get_order
    @page_title = "#{Time.now.year.to_s} Children's Literature Festival"
    @breadcrumb = [@page_title]
  end


  def browse
    @authors = ClfBook.select( :primary_author ).distinct.where( :festival_year => Time.now.year.to_i ).order( "primary_author asc" ).pluck( :primary_author )
    @books_by_author = {}
    @authors.each do |author|
      books = ClfBook.where( :festival_year => Time.now.year.to_i, :primary_author => author ).order( "title asc" )
      @books_by_author[ author ] = books
    end

    @order = get_order
    @page_title = "#{Time.now.year.to_s} Children's Literature Festival Books"
    @breadcrumb = ["<a href='#{clf_url}'>Children's Literature Festival</a>", @page_title]
  end


  def cart
    @order = get_order
    @page_title = "#{Time.now.year.to_s} Children's Literature Festival Pre-Order Shopping Cart"
    @breadcrumb = ["<a href='#{clf_url}'>Children's Literature Festival</a>", @page_title]
  end


  def order
    @order = get_order

    #redirect users to the browse page if they don't have enough items in their cart
    if @order.book_count < @order.minimum_books
      flash[:alert] = "There is a minimum of #{@order.minimum_books} books on all pre-orders. You do not have enough books in your cart to submit your order."
      return redirect_to clf_browse_url
    end

    if request.method == "POST"
      #redirect if the honeypot "content" field was filled by a spambot
      return redirect_to root_path unless params[:content].blank?

      #assign attributes from the form, save, and send email
      @order.assign_attributes( params.require( :clf_order ).permit! )
      @order.is_submitted = true
      if @order.save( :context => :form_submit )
        flash[:notice] = "Your order was submitted. You should receive confirmation via email."
        ClfOrderMailer.send_email( @order ).deliver_now
        return redirect_to clf_browse_url
      end
    end

    @page_title = "#{Time.now.year.to_s} Children's Literature Festival Pre-order"
    @breadcrumb = ["<a href='#{clf_url}'>Children's Literature Festival</a>", @page_title]
  end


  def add_to_cart

    #make the quantity 1 by default if not set
    quantity = params.dig(:clf_book_order_link, :quantity)&.to_i || 1

    #look up the book added
    @book = ClfBook.find_by( :id => params[:clf_book_order_link][:book_id] )

    #get the order
    @order = get_order

    #determine if the book exists
    if @book
      #add or remove the book
      if params[:remove] == "true"
        @success = @order.remove_book( @book )
      else
        @success = @order.add_book( @book, quantity )
      end
    end

    book_order = ClfBookOrderLink.find_by( :book_id => params.dig(:clf_book_order, :book_id), :order_id => @order.id )
    book_order||= ClfBookOrderLink.new

    response = {
      book_id: params.dig(:clf_book_order, :book_id),
      success: if @book.blank? then false else @success end,
      is_new: if book_order.created_at != book_order.updated_at and !book_order.id.blank? then false else true end,
      cart: {
        items: [],
        book_count: @order.book_count,
        subtotal: @order.subtotal,
        tax: @order.tax,
        total: @order.total
      }
    }

    @order.clf_book_order_links.each do |book_order|
      book = ClfBook.find_by( id: book_order.book_id )
      next unless book.present?
      response[:cart][:items]<< {
        id: book.id,
        title: book.title.squish,
        author:  book.combined_authors,
        price: book.price,
        quantity: book_order.quantity,
        image: book.full_image_url( true ),
        summary: book.summary || ''
      }
    end

    render json: response.to_json
  end

  private

  #Find an existing order or create a new one
  def get_order
    order = ClfOrder.find_by( :session_id => session[:session_id], :is_submitted => [false, nil] )
    if order.blank?
      order = ClfOrder.new
      order.session_id = session[:session_id]
      order.save
    end
    return order
  end

end
