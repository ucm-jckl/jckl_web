class FetchStudyRoomUsageJob < ApplicationJob
  queue_as :default

  def perform
    StudyRoomBooking.get_bookings_from_libcal
  end

end
