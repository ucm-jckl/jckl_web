class FetchUcmHeaderJob < ApplicationJob
  queue_as :default

  def perform
    response = HTTParty.get('https://www.ucmo.edu', verify: false)
    html = response.body
    xml_doc = Nokogiri::HTML(html)
    header_node = xml_doc.at_css('.header.header-wrapper')
    header_html_string = header_node.to_html
    header_html_string.gsub!('/_resources/img/', 'https://www.ucmo.edu/_resources/img/')
    header_html_string.gsub!('href="/', 'href="https://www.ucmo.edu/')
    FileUtils.rm_f(Rails.application.root.to_s + '/tmp/cache/ucm_header.html')
    File.open(Rails.application.root.to_s + '/tmp/cache/ucm_header.html', 'w'){ |f| f.puts("<div id='ucm-header'>#{header_html_string}</div>") }
  end

end
