class BuildNewBooksCollectionJob < ApplicationJob
  queue_as :default

  def perform()
    parent_collection_pid = '8164499610005571'

    # Get all subcollections of the parent New Books collection
    puts 'get collections'
    collections = AlmaApiService.request('GET', '/almaws/v1/bibs/collections/'+ parent_collection_pid, {'level' => '2'})
    subcollections = collections.dig('collection') || []

    # Delete collections older than 12 months
    puts 'delete collections'
    delete_old_collections(subcollections)

    # Check if a collection exists called "New Resources YYYY-MM" for this month. If not, create.
    puts 'create new'
    new_collection_pid = create_new_collection(subcollections, parent_collection_pid)

    # Retrieve an Alma Analytics query showing books received since the first of this month.
    puts 'retrieve analytics'
    new_books = get_new_books()

    # Add these books to the collection
    puts 'add to collection'
    add_to_collection(new_collection_pid, new_books)
  end


  # Delete collections older than 12 months on a rolling basis
  def delete_old_collections(collections)
    # Get the last 12 months in YYYY-mm format
    months = []
    pointer_month = Date.today.beginning_of_month
    12.times do
      months << pointer_month.strftime('%Y-%m')
      pointer_month = pointer_month - 1.month
    end

    # If a collection's month isn't in the list of last 12 months, remove bibs and delete collection
    collections.each do |collection|
      pid = collection.dig('pid', 'value')
      collection_month = collection['name'].gsub('New Resources ', '')
      if months.include?(collection_month)
        # First remove bibs from the collection
        loop do
          bibs = AlmaApiService.request('GET', '/almaws/v1/bibs/collections/' + pid + '/bibs', {'limit' => 100})
          break if bibs['total_record_count'] == 0
          bibs.dig('bibs', 'bib')&.each do |bib|
            response = AlmaApiService.request('DELETE', '/almaws/v1/bibs/collections/' + pid + '/bibs/' + bib['mms_id'])
          end
        end

        # Then we can delete the collection
        AlmaApiService.request('DELETE', '/almaws/v1/bibs/collections/' + pid)
      end
    end
  end



  #Check if a subcollection for this month already exists, and if not, create one
  def create_new_collection(collections, parent_collection_pid)
    new_collection_pid = nil

    # Check if it already exists
    this_month_collection_title = 'New Resources ' + Time.now.strftime('%Y-%m')
    collections.each do |collection|
      if collection['name'] == this_month_collection_title
        new_collection_pid = collection.dig('pid', 'value')
      end
    end

    # If so, return the existing one
    if new_collection_pid != nil
      return new_collection_pid
    end

    # If not, make a new one and return it.
    post_body = {
      'parent_pid' => {
        'value' => parent_collection_pid
      },
      'name' => this_month_collection_title,
      'description' => '',
      'library' => {
        'value' => 'jckl'
      },
      'collection' => [{}],
      'external_system' => '',
      'external_id' => '',
      'order' => '0'
    }
    response = AlmaApiService.request('POST', '/almaws/v1/bibs/collections', {'record_format' => 'marc21'}, post_body)
    new_collection_pid = response.dig('pid', 'value')
    return new_collection_pid
  end



   # Get a list of new books from the Alma Analytics API.The  Alma Analytics API is limited to 1000 results per API call, so
   # we have to use a ResumptionToken to make multiple calls in order to get all results.
  def get_new_books()
    book_mms_ids = []

    # Get physical items
    is_finished = false
    resumption_token = nil
    i = 0
    while i == 0 || is_finished == false do
      physical_items_response = AlmaApiService.request('GET', '/almaws/v1/analytics/reports', {
         'path' => '/shared/University of Central Missouri 01UCMO_INST/Reports/Website Feed Reports - do not edit/New Physical Items',
         'limit' => 1000,
         'token' => resumption_token
      })
      xml = Nokogiri::XML(physical_items_response.dig('anies', 0))

      xml.css('ResultXml rowset Row')&.each do |row|
        book_mms_ids << row.css('Column1').text
      end

      resumption_token = xml.css('ResumptionToken').text

      if(xml.css('IsFinished').text != 'false')
         is_finished = true
      end
      i = i+1
    end


    # Get electronic items
    is_finished = false
    resumption_token = nil
    i = 0
    while i == 0 || is_finished == false do
      electronic_portfolios_response = AlmaApiService.request('GET', '/almaws/v1/analytics/reports', {
        'path' => '/shared/University of Central Missouri 01UCMO_INST/Reports/Website Feed Reports - do not edit/New Electronic Portfolios',
        'limit' => 1000
      })

      electronic_portfolios_response.dig('ResultXml', 'rowset', 'Row')&.each do |row|
        book_mms_ids << row['Column1']
      end

      resumption_token = electronic_portfolios_response.dig('ResumptionToken')
      if electronic_portfolios_response.dig('IsFinished') != 'false'
         is_finished = true
      end

      i = i+1
    end

    # Return all MMS IDs with dupes removed
    return book_mms_ids.uniq
  end



  # Add a list of titles by MMS to a collection by PID.
  def add_to_collection(new_collection_pid, mms_ids)
    mms_ids.each do |mms|
      AlmaApiService.request('POST', '/almaws/v1/bibs/collections/'+ new_collection_pid +'/bibs', {}, {
        'mms_id' => mms
      })
    end
  end

end
