class SyncPlannerToCalendarJob < ApplicationJob
  queue_as :default

  def perform(*team_member)
    # Connect to Google API
    api_service = GoogleApiService.new

    # Loop through each task
    team_member[0][:tasks].each do |t|    # Note we use [0] because the Delayed Job library wraps the data in an outer array
      # If this task was completed over 3 months ago, don't do anything
      next if t['completedDateTime'].present? && Chronic.parse(t['completedDateTime']) < Time.now - 3.months

      # Calculate ID tag and URL
      tag = "[PLANNER ID: #{t['id'].gsub(/[-_]/,"")}]"
      url = "https://tasks.office.com/ucmo.edu/en-US/Home/Task/#{t['id']}"
      # Delete existing events from calendar
      begin
        events = api_service.get_gcal_events(team_member[0][:email], Time.now - 1.year, nil, tag)
        events.each do |e|
          api_service.delete_gcal_event(team_member[0][:email], e.id)
        end
      rescue Google::Apis::ClientError => e
        #puts e
      end

      # If this task has no due date, skip adding it to calendar
      next if t['dueDateTime'].blank?

      # If this task was due more than 3 months ago, skip adding it to calendar
      next if t['dueDateTime'] && Chronic.parse(t['dueDateTime']) < Time.now - 3.months

      # Otherwise, add to calendar
      date = Chronic.parse(t['dueDateTime']).to_date
      start_time = Chronic.parse(date.to_s + " 5 PM")
      end_time = Chronic.parse(date.to_s + " 6 PM")
      color = 10
      transparency = 'transparent'
      title = t['title']
      if t['completedDateTime'].present?
        title += ' [FINISHED]'
      end

      description = "#{tag}<br><br><b>URL: </b> #{url}<br><br>"
      description += "<b>Due: </b><br>#{t['dueDateTime'].to_date.to_s}<br><br>"
      if t.dig('details', 'description').present?
        description += "<b>Description</b>: <br>#{t['details']['description']}<br><br>"
      end
      if t.dig('details', 'checklist').present?
        description += "<b>Checklist: </b>:"
        description += "<ul>"
        t['details']['checklist'].each do |id, checklist_item|
          description += "<li>#{if checklist_item['isChecked'] then '[x]' else '[ ]' end} #{checklist_item['title']}</li>"
        end
        description += "</ul>"
      end

      begin
        api_service.add_gcal_event(team_member[0][:email], title, date, start_time, end_time, description, transparency, color)
      rescue Google::Apis::ClientError => e
        #puts e
      end



    end
  end
end
