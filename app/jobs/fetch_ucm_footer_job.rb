class FetchUcmFooterJob < ApplicationJob
  queue_as :default

  def perform
    response = HTTParty.get('https://www.ucmo.edu', verify: false)
    html = response.body
    xml_doc = Nokogiri::HTML(html)
    footer_node = xml_doc.at_css('.footer')
    footer_html_string = footer_node.to_html
    footer_html_string.gsub!('/_resources/img/', 'https://www.ucmo.edu/_resources/img/')
    footer_html_string.gsub!('href="/', 'href="https://www.ucmo.edu/')
    FileUtils.rm_f(Rails.application.root.to_s + '/tmp/cache/ucm_footer.html')
    File.open(Rails.application.root.to_s + '/tmp/cache/ucm_footer.html', 'w'){ |f| f.puts("<div id='ucm-footer'>#{footer_html_string}</div>") }
  end

end
