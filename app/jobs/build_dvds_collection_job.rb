class BuildDvdsCollectionJob < ApplicationJob
  queue_as :default

  def perform(*team_member)
    parent_collection_pid = '8195199400005571'

    # Get DVDs. The Alma Analytics API is limited to 1000 results per API call, so
    # we have to use a ResumptionToken to make multiple calls in order to get all results.
    dvd_mms_ids = []

    i = 0
    resumption_token = nil
    is_finished = false
    while i == 0 || is_finished == false do
      puts "looping #{i}"
      response = AlmaApiService.request('GET', '/almaws/v1/analytics/reports', {
        'path' => '/shared/University of Central Missouri 01UCMO_INST/Reports/Website Feed Reports - do not edit/DVDs',
        'limit' => 1000,
        'token' => resumption_token
      })
      xml = Nokogiri::XML(response.dig('anies', 0))

      xml.css('ResultXml rowset Row')&.each do |row|
        dvd_mms_ids << row.css('Column1').text
      end

      resumption_token ||= xml.css('ResumptionToken').text

      if(xml.css('IsFinished').text != 'false')
         is_finished = true
      end
      i = i+1
    end

    # Add them to the collection using the API
    dvd_mms_ids.each do |mms|
      puts "adding mms #{mms}"
      AlmaApiService.request('POST', '/almaws/v1/bibs/collections/'+ parent_collection_pid +'/bibs', {}, {
        'mms_id' => mms
      });
    end
  end

end
