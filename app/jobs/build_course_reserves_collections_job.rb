class BuildCourseReservesCollectionsJob < ApplicationJob
  queue_as :default

  def perform(*team_member)
    parent_collection_pid = '8164659250005571'

    # get course subcollections
    course_parent_collection = AlmaApiService.request('GET', '/almaws/v1/bibs/collections/' + parent_collection_pid, {'level' => 2})
    course_subcollections = course_parent_collection['collection'] #pid is ['pid']['value']

    # get courses
    courses = AlmaApiService.request('GET', '/almaws/v1/courses')['course']

    current_collections = {}
    # compare the two, delete old, add new
    courses.each do |course|
      # if not present, add
      if course_subcollections.select{|col| col['name'] == generate_collection_title_from_course(course)}.blank?
        post_body = {
          'parent_pid' => {
            'value' => parent_collection_pid
          },
          'name' => generate_collection_title_from_course(course),
          'description' => '',
          'library' => {
            'value' => 'jckl'
          },
          'collection' => [{}],
          'external_system' => '',
          'external_id' => '',
          'order' => '0'
        }
        new_collection_response = AlmaApiService.request('POST', '/almaws/v1/bibs/collections', {'record_format' => 'marc21'}, post_body)
        current_collections[generate_collection_title_from_course(course)] = new_collection_response.dig('pid', 'value')
      end
    end

    course_subcollections.each do |col|
      # if not present, empty and delete
      if courses.select{|course| col['name'] == generate_collection_title_from_course(course) }.blank?
        pid = col.dig('pid', 'value')

        # get collection bibs
        bibs_response = AlmaApiService.request('GET', '/almaws/v1/bibs/collections/' + pid + '/bibs')

        # remove bibs from collection
        bibs_response['bib']&.each do |bib|
          AlmaApiService.request('DELETE', '/almaws/v1/bibs/collections/' + pid + '/bibs/' + bib['mms_id'])
        end

        # delete collection
        AlmaApiService.request('DELETE', '/almaws/v1/bibs/collections/' + pid)
      else
        current_collections[col['name']] = col.dig('pid', 'value')
      end
    end

    courses.each do |course|
      # Get reading list MMS IDs
      mms_ids = []
      reading_lists = AlmaApiService.request('GET', '/almaws/v1/courses/' + course['id'] + '/reading-lists')
      reading_lists['reading_list']&.each do |list|
        list.dig('citations', 'citation')&.each do |citation|
          mms_ids << citation['id']
        end
      end

      # Add MMS IDs to subcollection
      mms_ids.each do |mms|
        AlmaApiService.request('POST', ('/almaws/v1/bibs/collections/' + collection_pid + '/bibs'), {}, {
          'mms_id' => mms
        })
      end

      # Delete obsolete MMS IDs from subcollection
      subcollection_pid = current_collections[generate_collection_title_from_course(course)]
      next if subcollection_pid.blank?
      subcollection_bibs = AlmaApiService.request('GET', '/almaws/v1/bibs/collections/' + subcollection_pid + '/bibs', {'limit' => 100})
      subcollection_bibs.dig('bibs', 'bib')&.each do |bib|
        if !mms_ids.include?(bib)
          AlmaApiService.request('DELETE', '/almaws/v1/collections/' + subcollection_pid + '/bibs/' + bib['mms_id'])
        end
      end
    end
  end

  def generate_collection_title_from_course(course)
    return course['code'] + ' - ' + course['name']
  end

end
