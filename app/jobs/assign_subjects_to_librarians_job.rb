class AssignSubjectsToLibrariansJob < ApplicationJob
  queue_as :default

  def perform
    SubjectArea.assign_subjects_to_librarians
  end

end
