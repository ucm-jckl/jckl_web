class FetchHoursJob < ApplicationJob
  queue_as :default

  def perform
    LibraryHour.fetch_hours
  end
end
