class BuildBestSellersCollectionJob < ApplicationJob
  queue_as :default

  def perform
    parent_collection_pid = '8195199390005571'

    mms_ids = []

    is_finished = false
    resumption_token = nil
    i = 0
    while i == 0 && is_finished == false do
      response = AlmaApiService.request('GET', '/almaws/v1/analytics/reports', {
        'path' => '/shared/University of Central Missouri 01UCMO_INST/Reports/Website Feed Reports - do not edit/Best Sellers',
        'limit' => 1000,
        'token' => resumption_token
      });
      xml = Nokogiri::XML(response.dig('anies', 0))

      xml.css('ResultXml rowset Row')&.each do |row|
        mms_ids << row.css('Column1').text
      end

      resumption_token = xml.css('ResumptionToken').text

      if(xml.css('IsFinished').text != 'false')
         is_finished = true
      end
      i = i+1
    end

    # Add them to the collection using the API
    mms_ids.each do |mms|
      puts "adding mms #{mms}"
      AlmaApiService.request('POST', '/almaws/v1/bibs/collections/'+ parent_collection_pid +'/bibs', {}, {
        'mms_id' => mms
      })
    end
  end

end
