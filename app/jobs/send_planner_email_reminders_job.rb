class SendPlannerEmailRemindersJob < ApplicationJob
  queue_as :default

  def perform
    Employee.all.each do |e|
      e.send_email_reminders
      sleep(5)
    end
  end
end
