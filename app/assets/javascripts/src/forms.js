JCKL = (typeof JCKL === 'undefined') ? {} : JCKL;
JCKL.FORMS = (function(){

    var forms = {

        init: function(){
            this.apply_datetimepicker();
            this.clear_form();
            this.toggle_checkboxes();

            return true;
        },

        //Apply the jQuery AnyTime plugin to datetime fields
        apply_datetimepicker: function(){

          // destroy existing pickers
          if( $('.anytime-datetime, .anytime-date, .anytime-time').length > 0 ){
            $('.anytime-datetime, .anytime-date, .anytime-time').AnyTime_noPicker();
          }

          // initialize pickers
          $( ".anytime-datetime" ).AnyTime_picker( {
            askEra: false,
            askSecond: false,
            format: "%Y-%m-%d %h:%i %p",
            latest: new Date( new Date().getTime() + ( 1000 * 60 * 60 * 24 * 365 * 2 ) )
          } ).removeAttr( "readonly" );

          $( ".anytime-date" ).AnyTime_picker( {
            askEra: false,
            askSecond: false,
            format: "%Y-%m-%d",
            latest: new Date( new Date().getTime() + ( 1000 * 60 * 60 * 24 * 365 * 2 ) )
          } ).removeAttr( "readonly" );

          $( ".anytime-time" ).AnyTime_picker( {
            askEra: false,
            askSecond: false,
            format: "%h:%i %p",
            latest: new Date( new Date().getTime() + ( 1000 * 60 * 60 * 24 * 365 * 2 ) )
          } ).removeAttr( "readonly" );
        },

        //Clear a form
        clear_form: function(){
            $( ".form-reset" ).click( function(){
                var form = $( this ).parents( "form" );
                form.find( "input, select" ).val("");
            });
        },

        //Toggle checkboxes
        toggle_checkboxes: function(){
          $('.toggle-check-all').click(function(){
            DEBUGME = $(this);
            $(this).parents('form').find(':checkbox').prop('checked', true);
          });

          $('.toggle-uncheck-all').click(function(){
            $(this).parents('form').find(':checkbox').prop('checked', false);
          });
        }

    };


    return forms;
}());
