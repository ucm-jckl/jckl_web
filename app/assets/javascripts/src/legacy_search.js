JCKL = (typeof JCKL === 'undefined') ? {} : JCKL;
JCKL.LEGACY_SEARCH = (function(){

    var legacy_search = {

        url_templates: {
          jckl: "https://ucmo.primo.exlibrisgroup.com/discovery/search?query=any,contains,{QUERY}&tab=collections&search_scope=collections&vid=01UCMO_INST:jckl_custom&offset=0",
          worldcat: "https://ucmo.primo.exlibrisgroup.com/discovery/search?query=any,contains,{QUERY}&tab=worldcat&search_scope=worldcat&vid=01UCMO_INST:jckl_custom&offset=0",
          jckl_plus_worldcat: "https://ucmo.primo.exlibrisgroup.com/discovery/search?query=any,contains,{QUERY}&tab=collectionsplusworldcat&search_scope=collectionsplusworldcat&vid=01UCMO_INST:jckl_custom&offset=0",
          jckl_physical_only: "https://ucmo.primo.exlibrisgroup.com/discovery/search?query=any,contains,{QUERY}&tab=physical&search_scope=physical&vid=01UCMO_INST:jckl_custom&offset=0",
          website: "https://ucmo.primo.exlibrisgroup.com/discovery/search?query=any,contains,{QUERY}&tab=website&search_scope=website&vid=01UCMO_INST:jckl_custom&offset=0"
        },

        init: function(){
          this.give_search_box_focus();
          this.handle_search_box_dropdown();
          this.toggle_search_options();

          this.do_search();

          return true;
        },


        give_search_box_focus: function() {
          //Add a special CSS class to the search box form when it has focus.
          //Also gives the search box focus on page load.

          //$(".search-text-field").focus(); //don't start with search box focused because it causes accessibility issues for screen readers
          $( ".search-wrapper" ).addClass( "search-has-focus" );

          $( ".search-wrapper" ).focusin( function() {
            $( this ).addClass( "search-has-focus" );
          } );

          $( ".search-wrapper" ).focusout( function() {
            $( this ).removeClass( "search-has-focus" );
          } );
      },


      //Change the contents of the search dropdown button based on the clicked dropdown item.
      handle_search_box_dropdown: function() {
         var this_class = this;

         //set the click event
         $( ".search-type-list a" ).click( function(e) {
           e.preventDefault();
           //First check if it is a plain link. Those should just behave like normal links.
           if ( $( "a", this ).length == 0 ) {
             this_class.select_search_box_dropdown_item( $( this ) );
           }
         } );

         //populate initial values based on the first item in the list
         this.select_search_box_dropdown_item( $( ".search-type-list a:first-child" ) );
      },


      //make the contents and value of the button equal to those of the dropdown item clicked
      select_search_box_dropdown_item: function( selectedItem ) {
        var clickedValue = selectedItem.attr( "data-value" );
        var clickedText = selectedItem.text();
        selectedItem.parents( ".search-type-list" ).siblings( ".search-type-button" ).val( clickedValue ).text( clickedText );

        //make the search field placeholder text equal to that specified on the dropdown item
        var clickedPlaceholder = selectedItem.attr( "data-placeholder" );
        selectedItem.parents( ".search-wrapper" ).children( ".search-text-field" ).attr( "placeholder", clickedPlaceholder );
      },


      //Create an onclick event to show/hide search options on small screens.
      toggle_search_options: function() {
        $( ".search-options h3" ).click( function() {
          $( this ).toggleClass( "open" );
          $( ".search-options .menu-block-wrapper" ).slideToggle();
        } );
      },


      //Execute a search and open up a results page
      do_search: function() {
        var this_class = this;

        $( "#jckl-search-box" ).submit( function( e ) {
          e.preventDefault();

          var search_terms = encodeURIComponent($( ".search-text-field" ).val());
          var search_type = $( ".search-type-button" ).val();
          window.location.href = this_class.url_templates[search_type].replace('{QUERY}',search_terms);
        } );

        //Fix for iOS form not submitting when Enter key is pressed
        var is_ios = navigator.userAgent.toLowerCase().match( /(iphone|ipod|ipad)/ );
        if ( is_ios ) {
          $( ".search-wrapper .search-text-field" ).keypress( function( e ) {
            //if Enter key was pressed, trigger a Click on the submit button
            if ( e.which == 13 ) {
              $( ".search-wrapper .search-submit-button" ).trigger( "click" );
            }
          } );
        }
      },


    };


    return legacy_search;
}());
