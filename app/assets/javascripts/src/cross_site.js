//Helper functions for enabling cross-site communication between various web platforms
JCKL = (typeof JCKL === 'undefined') ? {} : JCKL;
JCKL.CROSS_SITE = ( function() {

  var cross_site = {

    init: function() {
      //First check that the current domain is not library.ucmo.edu
      if ( JCKL.UTILITIES.local_hosts().indexOf( window.location.host ) == -1 ) {

        this.insert_header();
        this.insert_footer();
        this.insert_search();

      }
      return true;
    },

    //Determine whether to use HTTPS or HTTP with a special HTTP proxy (because IE requires same protocol for CORS)
    base_url: function(){
      var url = window.location.protocol + "//library.ucmo.edu/elements/";
      if ( window.location.protocol.toLowerCase() == "http:" ) {
        url = window.location.protocol + "//library.ucmo.edu/http/elements/";
      }
      return url;
    },


    //Insert the header
    insert_header: function(callback) {
      $( "#jckl-header" ).empty().load( this.base_url() + "header > *", function(){
        typeof callback === 'function' && callback();
      });
    },

    //Insert the search bar and then re-initialize the search bar behavior for the newly added DOM elements
    insert_search: function(callback) {
      $( "#jckl-search" ).load( this.base_url() + "search > *", function() {
        JCKL.SEARCH.init();
        typeof callback === 'function' && callback();
      });
    },

    //Insert the footer
    insert_footer: function(callback) {
      $( "#jckl-footer" ).empty().load( this.base_url() + "footer > *", function(){
        typeof callback === 'function' && callback();
      });
    },

    //Insert the library hours block
    insert_hours_block: function(callback) {
      $( "#jckl-sidebar .library-hours-block" ).load( this.base_url() + "hours .library-hours-block > *", function(){
        typeof callback === 'function' && callback();
      });
    },

    //Insert the help block
    insert_help_block: function(callback) {
      $( "#jckl-sidebar .help-block" ).load( this.base_url() + "help .help-block > *", function(){
        typeof callback === 'function' && callback();
      });
    },

    //Insert the computers block
    insert_computers_block: function(callback) {
      $( "#jckl-sidebar .computers-block" ).load( this.base_url() + "rooms_computers .computers-block > *", function() {
        JCKL.CHAT.init();
        typeof callback === 'function' && callback();
      });
    }

  };

  return cross_site;
}() );
