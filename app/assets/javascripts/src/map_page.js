JCKL = (typeof JCKL === 'undefined') ? {} : JCKL;
JCKL.MAP_PAGE = (function(){

    var map_page = {

        init: function(){
          if( $('.controller_map.action_index').length > 0 ){
            var self = this;

            JCKL.MAP.init(function(){
              var floor_height = JCKL.MAP.get_svg_height(false)
              JCKL.MAP.set_viewport_height( floor_height );

              var active_floor_number = JCKL.MAP.map_container.data('activeFloor') || 1;
              JCKL.MAP.go_to_floor(active_floor_number);

              self.handle_room_search();
              self.handle_toggle_floor();

              self.highlight_initial_element();
              self.set_computer_availability();
            });
          }

          return true;
        },


        highlight_initial_element: function(){
          var highlighted_element_id = JCKL.MAP.map_container.data('highlightedElementId');
          if(highlighted_element_id){
            highlighted_element = JCKL.MAP.viewport.select('#' + highlighted_element_id);
            if(highlighted_element){
              this.highlight_and_go_to_element(highlighted_element);
            }
          }
        },


        highlight_and_go_to_element: function(element){

          var floor_number = JCKL.MAP.find_element_parent_floor(element).node.id.replace('floor-','');
          $('.floor-toggle[data-floor=' + floor_number + ']').addClass('active');
          JCKL.MAP.go_to_floor(floor_number);
          JCKL.MAP.highlight_element(element, true);

          $('.floor-toggle').removeClass('active');
          $('.floor-toggle[data-floor=' + floor_number + ']').addClass('active');
        },


        set_computer_availability: function(){
          if( JCKL.COMPUTER_AVAILABILITY ){
            for(var i = 0; i < JCKL.COMPUTER_AVAILABILITY.length; i++ ){
              var computer_id = 'computers-' + JCKL.COMPUTER_AVAILABILITY[i]['map_tag'];
              var computer_element = JCKL.MAP.viewport.select('#' + computer_id);

              if( computer_element ){
                var availability = JCKL.COMPUTER_AVAILABILITY[i]['available?'];
                if(availability){
                  computer_element.addClass('available')
                }else{
                  computer_element.addClass('unavailable')
                }
              }
            }
          }
        },


        handle_room_search: function(){
          var self = this;
          $('.room-search').submit(function(e){
            e.preventDefault();
            var search_value = $(this).find('.room-search-number').val();
            var room_number = search_value.replace( /[^\d]/g, "" );
            if ( search_value.indexOf( "media" ) != -1 ) {
              var room_id = "room-media" + room_number;
            } else {
              var room_id = "room-" + room_number;
            }
            var room_element = JCKL.MAP.viewport.select("#" + room_id);
            if(room_element){
              $('.room-not-found').hide();
              $('.floor-toggle').removeClass('active');
              self.highlight_and_go_to_element(room_element);
            }else{
              $('.room-not-found').show();
              $('.room-not-found-value').text(search_value);
            }
          });
        },


        handle_toggle_floor: function(){
          $('.floor-toggle').click(function(e){
            e.preventDefault();
            var floor_number = $(this).data('floor');
            JCKL.MAP.go_to_floor(floor_number);

            $('.floor-toggle').removeClass('active');
            $('.floor-toggle[data-floor=' + floor_number + ']').addClass('active');
          });
        }

    };


    return map_page;
}());
