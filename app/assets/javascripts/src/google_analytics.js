// Custom event tracking for Google Analytics
JCKL.GOOGLE_ANALYTICS = ( function() {

  /*
   * Information on Google Analytics tracking events here: https://developers.google.com/analytics/devguides/collection/gajs/eventTrackerGuide
   * ga(
   *      'send',
   *      'event', //This has to be in every event
   *      'Category',
   *      'Action',
   *      'Label',
   *      'Value'
   *  );
   */


  var stats = {

    init: function() {
      this.record_header_events();
      this.record_footer_events();
      this.record_search_events();
      this.record_chat_events();
      this.record_new_books_events();
      this.record_news_events();
      this.record_front_page_three_column_events();
      this.record_old_homepage_toggle_events();

      return true;
    },

    record_header_events: function() {
      //record when the UCM links dropdown menu is expanded
      $( "#jckl-header .ucm-links-standalone" ).on( "show.bs.dropdown", function() {
        ga(
          'send',
          'event',
          'Header Region',
          'Dropdown Menu Expand',
          'UCM Links Menu',
          1
        );
      } );

      //record when one of the main navigation dropdowns is expanded
      $( "#jckl-header .navbar-nav > .dropdown" ).on( "show.bs.dropdown", function() {
        var event_label = $( ".dropdown-toggle", this ).text();
        ga(
          'send',
          'event',
          'Header Region',
          'Dropdown Menu Expand',
          event_label,
          1
        );
      } );

      //record when a link in one of the dropdowns is clicked
      $( "#jckl-header .dropdown-menu a" ).click( function() {
        var event_label = $( this ).parents( ".dropdown" ).find( ".dropdown-toggle" ).text() + " - " + $( this ).text();
        ga(
          'send',
          'event',
          'Header Region',
          'Dropdown Menu Link Click',
          event_label,
          1
        );
      } );

      //record when the logo is clicked
      $( "#jckl-header .navbar-brand" ).click( function() {
        ga(
          'send',
          'event',
          'Header Region',
          'Logo Home Link Click',
          '',
          1
        );
      } );
    },

    record_footer_events: function() {
      //record when a link in the footer is clicked
      $( "#jckl-footer .footer-columns a" ).click( function() {
        var event_label = $( this ).parents( ".footer-column" ).find( "h3" ).text() + " - " + $( this ).text();
        ga(
          'send',
          'event',
          'Footer Region',
          'Footer Link Click',
          event_label,
          1
        );
      } );
    },

    record_search_events: function() {
      //record when a search is submitted
      $( "#jckl-search-box" ).submit( function() {
        var search_terms = $( ".search-text-field" ).val();
        var search_type = $( ".search-type-button" ).text();
        ga(
          'send',
          'event',
          'Search Region',
          search_type + ' Submit',
          search_terms,
          1
        );
      } );

      //record when someone opens the search type dropdown
      $( "#jckl-search-box .dropdown-button-wrapper" ).on( "show.bs.dropdown", function() {
        ga(
          'send',
          'event',
          'Search Region',
          'Search Type Menu Expand',
          '',
          1
        );
      } );

      //record when someone changes the search type using the dropdown
      $( "#jckl-search-box .search-type-list a" ).click( function() {
        var event_label = $( this ).text();
        ga(
          'send',
          'event',
          'Search Region',
          'Search Type Change',
          event_label,
          1
        );
      } );

      //record when someone clicks a link in the Search Options area
      $( "#jckl-search .search-options a" ).click( function() {
        var event_label = $( this ).text();
        ga(
          'send',
          'event',
          'Search Region',
          'Search Option Link Click',
          event_label,
          1
        );
      } );
    },


    record_chat_events: function() {
      //record when the big chat button on the home page is clicked
      $( ".chat-button, .hero-button button, .hero-button a" ).click( function() {
        var event_label = $( this ).text();
        ga(
          'send',
          'event',
          'Chat Links',
          'Home Page Giant Chat Link Click',
          event_label,
          1
        );
      } );

    },


    record_new_books_events: function() {
      //record when new book scroll arrows are clicked on the home page
      $( ".index-new-books .owl-prev, .index-new-books .owl-next" ).click( function() {
        var event_label = $( this ).attr( "class" );
        ga(
          'send',
          'event',
          'New Books',
          'New Books Homepage Scroll Arrow Click',
          event_label,
          1
        );
      } );

      //record when new books scroll dots are clicked on the home page
      $( ".index-new-books .owl-page" ).click( function() {
        ga(
          'send',
          'event',
          'New Books',
          'New Books Homepage Scroll Dot Click',
          '',
          1
        );
      } );

      //record when the View More New Arrivals link is clicked on the home page
      $( ".index-new-books .new-books-view-more" ).click( function() {
        ga(
          'send',
          'event',
          'New Books',
          'New Books Homepage View More Click',
          '',
          1
        );
      } );

      //record when a specific book image link is clicked
      $( ".new-book a img" ).click( function() {
        var event_label = $( this ).parent( "a" ).attr( "href" );
        ga(
          'send',
          'event',
          'New Books',
          'New Books Image Link Click',
          event_label,
          1
        );
      } );

      //record when a specific book popover appears
      $( ".new-book" ).on( "show.bs.popover", function() {
        var event_label = $( this ).find( "a" ).attr( "href" );
        ga(
          'send',
          'event',
          'New Books',
          'New Books Image Popover',
          event_label,
          1
        );
      } );

      //record when a specific book title link is clicked
      $( ".new-book .new-book-title a" ).click( function() {
        var event_label = $( this ).attr( "href" );
        ga(
          'send',
          'event',
          'New Books',
          'New Books Title Link Click',
          event_label,
          1
        );
      } );

      //record when a specific book call number link is clicked
      $( ".new-book .new-book-call-number a" ).click( function() {
        var event_label = $( this ).attr( "href" );
        ga(
          'send',
          'event',
          'New Books',
          'New Books Call Number Link Click',
          event_label,
          1
        );
      } );

      //record when a specific book show more/less button is clicked
      $( ".new-book .more-link" ).click( function() {
        var event_label = $( this ).text() + " - " + $( this ).parents( ".new-book" ).find( ".new-book-title a" ).attr( "href" );
        ga(
          'send',
          'event',
          'New Books',
          'New Books Show More/Less Click',
          event_label,
          1
        );
      } );

      //record when the new books page subject dropdown is expanded
      $( ".new-books-subjects" ).on( "show.bs.dropdown", function() {
        ga(
          'send',
          'event',
          'New Books',
          'New Books Subject Dropdown Expand',
          '',
          1
        );
      } );

      //record when the new books page subject dropdown is expanded
      $( ".new-books-call-number-ranges" ).on( "show.bs.dropdown", function() {
        ga(
          'send',
          'event',
          'New Books',
          'New Books Call Number Range Dropdown Expand',
          '',
          1
        );
      } );

      //record when the new books page timeframe dropdown is expanded
      $( ".new-books-timeframe" ).on( "show.bs.dropdown", function() {
        ga(
          'send',
          'event',
          'New Books',
          'New Books Timeframe Dropdown Expand',
          '',
          1
        );
      } );

      //record when a new books page subject is clicked
      $( ".new-books-subjects .dropdown-menu a" ).click( function() {
        var event_label = $( this ).text();
        ga(
          'send',
          'event',
          'New Books',
          'New Books Subject Click',
          event_label,
          1
        );
      } );

      //record when a new books page subject is clicked
      $( ".new-books-call-number-ranges .dropdown-menu a" ).click( function() {
        var event_label = $( this ).text();
        ga(
          'send',
          'event',
          'New Books',
          'New Books Call Number Range Click',
          event_label,
          1
        );
      } );

      //record when a new books page timeframe is clicked
      $( ".new-books-timeframe .dropdown-menu a" ).click( function() {
        var event_label = $( this ).text();
        ga(
          'send',
          'event',
          'New Books',
          'New Books Timeframe Click',
          event_label,
          1
        );
      } );


      //record when a pagination button is clicked
      $( ".new-books-pagination a" ).not( ".disabled" ).click( function() {
        var event_label = $( this ).text();
        ga(
          'send',
          'event',
          'New Books',
          'New Books Pagination Click',
          event_label,
          1
        );
      } );
    },

    record_news_events: function() {
      //record when a news scroll arrow is clicked
      $( ".news-slider .owl-prev, .news-slider .owl-next" ).click( function() {
        var event_label = $( this ).attr( "class" );
        ga(
          'send',
          'event',
          'News',
          'News Homepage Scroll Arrow Click',
          event_label,
          1
        );
      } );

      //record when a news scroll dot is clicked
      $( ".news-slider .owl-page" ).click( function() {
        ga(
          'send',
          'event',
          'News',
          'News Homepage Scroll Dot Click',
          '',
          1
        );
      } );

      //record when a story image link is clicked (LEGACY)
      $( ".news-slider .news-image a" ).click( function() {
        var event_label = $( this ).attr( "href" );
        ga(
          'send',
          'event',
          'News',
          'News Homepage Story Image Click',
          event_label,
          1
        );
      } );

      //record when a story image Read More link is clicked (LEGACY)
      $( ".news-slider .news-story-read-more" ).click( function() {
        var event_label = $( this ).attr( "href" );
        ga(
          'send',
          'event',
          'News',
          'News Homepage Story Read More Click',
          event_label,
          1
        );
      } );

      //record when news story is clicked
      $( ".index-links .news-block .news-item" ).click( function() {
        var event_label = $( this ).attr( "href" );
        ga(
          'send',
          'event',
          'News',
          'News Homepage Story Read More Click',
          event_label,
          1
        );
      } );

      //record when a social media icon is clicked
      $( ".social-media-icon" ).click( function() {
        var event_label = $( this ).attr( "href" );
        ga(
          'send',
          'event',
          'News',
          'Homepage Social Media Icon Click',
          event_label,
          1
        );
      } );
    },

    record_front_page_three_column_events: function() {
      //record when a link is clicked
      $( ".front-page-row-1 a, .index-links a" ).click( function() {

        if ( $( this ).is( ".badge" ) ) {
          var event_label = $( this ).siblings( "a" ).text() + " (badge)";
        } else {
          var event_label = $( this ).text();
        }

        ga(
          'send',
          'event',
          'Homepage Three Columns Region',
          'Link Click',
          event_label,
          1
        );
      } );
    },


    record_old_homepage_toggle_events: function(){
      $('.legacy-homepage-toggle a').click(function(){
        var event_label = $(this).text();

        ga(
          'send',
          'event',
          'Home Page',
          'Legacy Version Toggle',
          event_label,
          1
        );
      })
    }

  };


  return stats;
}() );
