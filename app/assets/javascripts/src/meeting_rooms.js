JCKL = (typeof JCKL === 'undefined') ? {} : JCKL;
JCKL.MEETING_ROOMS = (function(){

    var meeting_rooms = {

        init: function(){
          if($('.controller_meeting_rooms.action_index').length == 1){
            this.show_calendars();
          }
          return true;
        },

        show_calendars: function(){
          $('.room-calendar-link').click(function(){
            var room_id = $(this).parents('.room').attr('id');
            var calendar_url = $(this).data('calendarUrl');
            var calendar_embed_url = $(this).data('calendarEmbedUrl');
            var modal = $( $(this).data('target') );
            modal.find('iframe').attr({
              'src': calendar_embed_url,
              'id': room_id + '-calendar',
            });
            modal.find('.google-calendar-link a').attr('href', calendar_url);
          })
        }

    };


    return meeting_rooms;
}());
