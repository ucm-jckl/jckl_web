//Plugins that don't fit anywhere else
JCKL = (typeof JCKL === 'undefined') ? {} : JCKL;
JCKL.PLUGINS = (function(){

    var plugins = {

      BOOTSTRAP_TABLE_ICONS: {
        refresh: 'fa-refresh',
        toggle: 'fa-list',
        columns: 'fa-columns',
        detailOpen: 'fa-plus-square',
        detailClose: 'fa-minus-square',
        export: 'fa-download'
      },


      init: function(){
        this.apply_readmore();
        return true;
      },

      apply_readmore: function(){
        $( ".readmore" ).readmore();
      },


    };


    return plugins;
}());
