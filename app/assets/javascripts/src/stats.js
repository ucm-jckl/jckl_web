JCKL = (typeof JCKL === 'undefined') ? {} : JCKL;
JCKL.STATS = (function(){

    var stats = {

      COMPUTER_ACTIVE_TIME_PERIOD: {
        name: 'Average',
        value: 'average',
      },


      STUDY_ROOM_HOURS_USED_CHART_OPTIONS: {
        chart: {
          type: 'line',
          panning: true,
          zoomType: 'xy',
          height: 500
        },
        title: {
          text: 'Hours Used',
        },
        legend: {
          enabled: true,
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle',
          maxHeight: 400,
        },
        xAxis: {
          type: 'category',
          crosshair: {
            enabled: true,
            width: 3
          },
          labels: {
            rotation: -45,
          }
        },
        yAxis: {
          title: {
            text: 'Hours Used'
          },
          min: 0
        },
        plotOptions: {
          line: {
            turboThreshold: 0
          }
        },
        credits: {
          enabled: false
        }
      },

      STUDY_ROOM_PERCENT_USED_CHART_OPTIONS: {
        chart: {
          type: 'line',
          panning: true,
          zoomType: 'xy',
          height: 500
        },
        title: {
          text: 'Percent of Available Hours Used',
        },
        legend: {
          enabled: true,
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle',
          maxHeight: 400,
        },
        xAxis: {
          type: 'category',
          crosshair: {
            enabled: true,
            width: 3
          },
          labels: {
            rotation: -45,
          }
        },
        yAxis: {
          title: {
            text: '% Used'
          },
          min: 0
        },
        tooltip: {
          valueDecimals: 2,
          valueSuffix: '%'
        },
        plotOptions: {
          turboThreshold: 0
        },
        credits: {
          enabled: false
        }
      },


      chart_data: {},


      init: function(){
        if( $('.controller_stats .chart-map').length > 0 ){
          this.location_data = JCKL.LOCATIONS;
          this.map_data = JCKL.STATS_DATA;

          var self = this;

          JCKL.MAP.init(function(){
            var floor_height = JCKL.MAP.get_svg_height(true)
            JCKL.MAP.set_viewport_height( floor_height );

            self.recolor_map();
            self.adjust_time_nav();

            self.handle_change_time_period_dropdown();
            self.handle_previous_time_period_click();
            self.handle_next_time_period_click();

          });
        }

        if( $('.controller_stats.action_studyrooms').length > 0 ){
          this.load_chart('hours-used-chart', STATS_DATA.DEFAULT_STUDY_ROOMS_HOURS_USED, JCKL.STATS.STUDY_ROOM_HOURS_USED_CHART_OPTIONS, 'rooms');
          this.load_data_table('hours-used-chart', STATS_DATA.DEFAULT_STUDY_ROOMS_HOURS_USED );
          this.load_chart('percent-used-chart', STATS_DATA.DEFAULT_STUDY_ROOMS_PERCENT_USED, JCKL.STATS.STUDY_ROOM_PERCENT_USED_CHART_OPTIONS, 'all');
          this.load_data_table('percent-used-chart', STATS_DATA.DEFAULT_STUDY_ROOMS_PERCENT_USED );
          this.handle_chart_toggle();
        }

        return true;
      },


      // START COMPUTER STATS SECTION


      recolor_map: function(){
        var active_data = this.get_active_data(this.COMPUTER_ACTIVE_TIME_PERIOD);
        var color_scale = this.generate_color_scale();
        JCKL.MAP.viewport.selectAll('.location').forEach(function(element, i){
          element.removeClass('active');
        });

        for(var location in active_data){
          // Find location element on map_data
          var location_system_record = $.grep(this.location_data, function(l){
            return location.indexOf(l.display_name) != -1;
          })[0]

          if(location_system_record){
            var location_system_name = location_system_record.location_name;
            var location_element = JCKL.MAP.viewport.select('#' + location_system_name);

            // Change color attribute
            if(location_element){
              location_element.addClass('active');
              var color = color_scale(active_data[location]);
              location_element.attr({
                fill: color.hex()
              });
            }
          }
        }
      },


      get_active_data: function(COMPUTER_ACTIVE_TIME_PERIOD){
        var active_data = {};
        for( var i = 0; i < this.map_data.series_data.length; i++ ){
          var location = this.map_data.series_data[i];

          if(COMPUTER_ACTIVE_TIME_PERIOD.value == 'average'){
            var sum = 0;
            for( var j = 0; j < location.data.length; j++ ){
              sum += location.data[j].y
            }

            active_data[location.name] = sum / location.data.length;
          }else{
            var data_point = $.grep(location.data, function(data_point){
              return data_point.x == COMPUTER_ACTIVE_TIME_PERIOD.value;
            })[0];
            if( data_point ){
              active_data[location.name] = data_point.y;
            }
          }
        }

        return active_data;
      },


      generate_color_scale: function(){
        var max_range = 0;
        for(var i = 0; i < this.map_data.series_data.length; i++){
          for(var j = 0; j < this.map_data.series_data[i].data.length; j++){
            if(!this.map_data.series_data[i].data[j].y){
              var data_point = this.map_data.series_data[i].data[j];
            }else{
              var data_point = this.map_data.series_data[i].data[j].y;
            }
            if(data_point > max_range){
              max_range = data_point;
            }
          }
        }


        var color1 = "#222";
        var color2 = "#55f";
        var color3 = "red";
        color_scale = chroma.scale( [ color1, color2, color3 ] ).domain( [ 0, max_range ] )
        $('.map-color-scale').css('background', 'linear-gradient(to right, '+ color1 +', '+ color2 +', '+ color3 +')');
        $('.map-color-scale-start').empty().append(0);
        $('.map-color-scale-midpoint').empty().append(max_range / 2);
        $('.map-color-scale-end').empty().append(max_range);

        return color_scale;
      },


      adjust_time_nav: function(){

        //Determine whether to show/hide next/prev
        if(this.COMPUTER_ACTIVE_TIME_PERIOD.value == 'average'){
          //Don't show either
          $('.map-data-pagination .previous-time-period, .map-data-pagination .next-time-period').css('visibility', 'hidden');
        }else{
          // Determine label values
          var time_axis_values = this.map_data.series_data[0].data.map(function(data_point){
            return data_point.x;
          });

          var active_time_index = time_axis_values.indexOf(Number(this.COMPUTER_ACTIVE_TIME_PERIOD.value));
          var previous_label = '<<  ' + $.trim($('.map-time-period option:eq('+ (active_time_index + 1 - 1) +')').text());
          var next_label =  $.trim($('.map-time-period option:eq('+ (active_time_index + 1 + 1) +')').text()) + '  >>';
          $('.map-data-pagination .previous-time-period').empty().append(previous_label);
          $('.map-data-pagination .next-time-period').empty().append(next_label);

          // Show/hide labels
          $('.map-data-pagination .previous-time-period, .map-data-pagination .next-time-period').css('visibility', 'visible');
          if(active_time_index == 0){
            //Hide previous
            $('.map-data-pagination .previous-time-period').css('visibility', 'hidden');
          }
          if(active_time_index == this.COMPUTER_ACTIVE_TIME_PERIOD.length - 1){
            //Hide next
            $('.map-data-pagination .next-time-period').css('visibility', 'hidden');
          }
        }
      },


      handle_change_time_period_dropdown: function(){
        var self = this;
        $('.map-time-period').change(function(){
          self.COMPUTER_ACTIVE_TIME_PERIOD = {
            name: $.trim($(this).find('option:selected').text()),
            value: $(this).val()
          };

          $('.map-time-period').val($(this).val());

          self.recolor_map();
          self.adjust_time_nav();
        });
      },


      handle_previous_time_period_click: function(){
        $('.map-data-pagination .previous-time-period').click(function(e){
          e.preventDefault();
          var current_index = $('.map-time-period option').index($('.map-time-period option:selected'));
          var previous_value = $('.map-time-period option:eq('+ (current_index - 1) +')').val();
          $('.map-time-period').val(previous_value).trigger('change');
        })
      },


      handle_next_time_period_click: function(){
        $('.map-data-pagination .next-time-period').click(function(e){
          e.preventDefault();
          var current_index = $('.map-time-period option').index($('.map-time-period option:selected'));
          var next_value = $('.map-time-period option:eq('+ (current_index + 1) +')').val();
          $('.map-time-period').val(next_value).trigger('change');
        })
      },

      // END COMPUTER STATS SECTION




      handle_chart_toggle: function(){
        var self = this;
        $('.grouping-toggle .btn, .time-period-toggle .btn').click(function(){
          $(this).addClass('active');
          $(this).siblings().removeClass('active');

          if( $(this).parents('.time-period-toggle').length > 0 ){
            var time_period = $(this).data('time-period');
            var grouping = $(this).parents('.chart-area').find('.grouping-toggle .btn.active').data('grouping');

          }else if( $(this).parents('.grouping-toggle').length > 0 ){
            var time_period = $(this).parents('.chart-area').find('.time-period-toggle .btn.active').data('time-period');
            var grouping = $(this).data('grouping');
          }

          var chart_id = $(this).parents('.chart-area').find('.chart').attr('id');
          var default_options_string = $(this).parents('.chart-area').find('.chart').data('default-options');
          var default_options = JCKL.STATS[default_options_string];
          var url = [location.protocol, '//', location.host, location.pathname].join('');
          var params = {
            chart: chart_id,
            interval: time_period,
            start_time: $('#original-start-time').val(),
            end_time: $('#original-end-time').val()
          }

          if( self.chart_data[Object.values(params).join('')] === undefined ){
            $('#' + chart_id).highcharts().showLoading('<i class="fa fa-spinner fa-spin fa-fw"></i> Loading...');
            $.getJSON(url, params).done(function(d){
              var data = d;
              self.chart_data[Object.values(params).join('')] = data;
              self.load_chart(chart_id, data, default_options, grouping);
              self.load_data_table(chart_id, data);
              $('#' + chart_id).highcharts().hideLoading();
            });
          }else{
            var data = self.chart_data[Object.values(params).join('')];
            self.load_chart(chart_id, data, default_options, grouping);
            self.load_data_table(chart_id, data);
          }
        });
      },


      load_chart: function(chart_id, data, default_options, grouping){
        var options = default_options;
        options.series = data;

        for(var i = 0; i < options.series.length; i++){
          if( (grouping == 'all' && options.series[i].name != 'All') || ( grouping != 'all' && options.series[i].name == 'All' ) ){
            options.series[i].visible = false;
          }else{
            options.series[i].visible = true;
          }
        }

        options.xAxis.categories = (function(){
          var categories = [];
          for(var i = 0; i < data.length; i++){
            for(var j = 0; j < data[i]['data'].length; j++){
              categories.push(data[i]['data'][j][0])
            }
          }
          return _.uniq(categories);
        })();

        options.xAxis.labels.step = (function(){
          if(options.xAxis.categories.length > 200000){
            return 10000;
          }else if(options.xAxis.categories.length > 20000){
            return 1000;
          }else if(options.xAxis.categories.length > 2000){
            return 100;
          }else if(options.xAxis.categories.length > 200){
            return 10;
          }else if( options.xAxis.categories.length > 100 ){
            return 5;
          }else if(options.xAxis.categories.length > 60){
            return 3;
          }else if(options.xAxis.categories.length > 30){
            return 2;
          }else{
            return 1;
          }
        })();

        $('#' + chart_id).highcharts(options);
      },


      load_data_table: function(chart_id, data ){
        $('#' + chart_id).parents('.chart-area').find('.chart-data-table .datatable').bootstrapTable('destroy');
        $('#' + chart_id).parents('.chart-area').find('.chart-data-table .datatable').bootstrapTable({
          data: (function(){
            var rows = [];
            for(var i = 0; i < data.length; i++){
              var series = data[i];
              for(var j = 0; j < series['data'].length; j++){
                var x = series['data'][j][0];
                var y = series['data'][j][1];
                rows.push({
                  'c1': x,
                  'c2': series.name,
                  'c3': y
                });
              }
            }
            return rows;
          })()
        });
      }


    };

    return stats;
}());
