JCKL = (typeof JCKL === 'undefined') ? {} : JCKL;
JCKL.INDEX = (function(){

    var index = {

        init: function(){
          if($('.controller_index.action_index').length == 1){
            this.setup_news_slider();
            this.setup_new_books_slider();
            this.open_chat_popout();
          }
          return true;
        },


        //Initialize plugins for the news slider
        setup_news_slider: function(){
          $('.news-slider').owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            autoplayHoverPause: true,
            nav: true,
            navText: ['<i class="fa fa-chevron-left fa-2x" aria-hidden="true"></i>','<i class="fa fa-chevron-right fa-2x" aria-hidden="true"></i>'],
            dots: false,
            slideBy: 'page'
          })
        },


        //Initialize plugins for the new books slider
        setup_new_books_slider: function(){
          $('.new-books-slider').owlCarousel({
            responsive: {
              0: {
                items: 2
              },
              768: {
                items: 3
              },
              960: {
                items: 5
              },
              1200: {
                items: 6
              }
            },
            loop: true,
            autoplay: true,
            autoplayHoverPause: true,
            slideBy: 'page',
            nav: true,
            navText: ['<i class="fa fa-chevron-circle-left fa-4x" aria-hidden="true"></i>','<i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true"></i>'],
            dots: true,
          });

          $('.new-books-slider img').popover({
            html: true,
            trigger: "hover",
            placement: "top",
            container: ".new-books-slider",
            delay: {
              show: 350,
              hide: 400
            }
          });
        },


        // Open chat popout on button click by triggering a click to hidden LibAnswers button
        open_chat_popout: function(){
          $('.index-links .chat-button').on('click keydown', function(e){
            if(e.type == 'click' || (e.type == 'keydown' && e.keyCode == 13)){
              e.preventDefault();
              $('.libchat-widget-button button').trigger('click');
            }
          })
        },

    };


    return index;
}());
