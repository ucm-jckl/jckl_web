JCKL = (typeof JCKL === 'undefined') ? {} : JCKL;
JCKL.ABSENCES = (function(){

    var absences = {

        init: function(callback){
          if( $('.controller_absences').length > 0 ){
            this.initialize_datepicker();
            this.clear_calendar();
            this.remove_date_subform();
            this.toggle_all_day();
          }

          return true;
        },


        initialize_datepicker: function(){

          $('#absence-dates').datepicker({
            multidate: true
          });

          $('#absence-dates').on('changeDate', function() {
            var dates =  $('#absence-dates').datepicker('getDates');
            var formatted_dates = [];
            for(var i = 0; i < dates.length; i++){
              formatted_dates.push( moment(dates[i]).format( "YYYY-MM-DD (ddd)" ));
            }
            $('#absence_absence_dates_list').val( formatted_dates.sort().join('\n') )

            var new_url = $('#change-absence-dates').attr('href').replace(/\?.*/i, '') + "?absence_dates=" + encodeURIComponent(formatted_dates.join('|'));
            $('#change-absence-dates').attr('href', new_url);
          });

          if( $('#selected-dates').val() != '' && $('#selected-dates').val() != undefined ){
            var dates_strings = $('#selected-dates').val().split('|');
            var dates = [];
            for(var i = 0; i < dates_strings.length; i++){
              dates.push(moment(dates_strings[i]).toDate());
            }
            $('#absence-dates').datepicker('setDates', dates);
          }

        },


        clear_calendar: function(){
          $('#clear-absence-dates').click(function(){
            $('#absence-dates').datepicker('update', '');
            $('#absence_absence_dates_list').val('')
            var new_url = $('#change-absence-dates').attr('href').replace(/\?.*/i, '') + "?absence_dates=";
            $('#change-absence-dates').attr('href', new_url);
          });
        },


        remove_date_subform: function(){
          $('.remove-date').click(function(){
            $(this).parents('.list-group-item').slideUp("slow", function(){
              $(this).find('.destroy-record').val(1);
            })
          });
        },


        toggle_all_day: function(){
          $('.out-all-day').change(function(){
            if( this.checked  ){
              $(this).parents('.absence-date').find('.start-time, .end-time').val('');
            }
          });

          $('.start-time, .end-time').change(function(){
            if( $(this).val() != '' ){
              $(this).parents('.absence-date').find('.out-all-day').attr('checked', false);
            }
          });
        }

    };


    return absences;
}());
