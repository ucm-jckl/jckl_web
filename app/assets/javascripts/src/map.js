JCKL = (typeof JCKL === 'undefined') ? {} : JCKL;
JCKL.MAP = (function(){

    var map = {

        init: function(callback){
          this.load_map(callback);

          return true;
        },


        load_map: function(callback){
          var self = this;
          self.map_container = $('.jckl-svg-map');
          self.svg_root = Snap('.jckl-svg-map');
          self.viewport = Snap().attr("class", "viewport");

          Snap.load(self.map_container.data('mapUrl'), function(svg){
            self.viewport.append(svg.selectAll('.floor'));
            self.pan_zoom_controls = self.initialize_pan_zoom_controls(self.viewport);

            callback();
          });

          self.svg_root.append(self.viewport);
        },


        set_viewport_height: function(height){
          this.map_container.find('.viewport').css('height', height);
        },


        get_svg_height: function(show_all_floors) {
          var tallest_floor = 0;
          var total_height = 0;

          this.viewport.selectAll( '.floor' ).forEach( function( element, i ) {
            var height = element.getBBox().height;
            total_height += height;
            if ( height > tallest_floor ) {
              tallest_floor = height;
            }
          } );

          if(show_all_floors){
            return total_height * 1.25;
          }else{
            return tallest_floor * 1.25;
          }

          return height;
        },


        initialize_pan_zoom_controls: function(viewport){
          var self = this;
          return svgPanZoom( '.' + viewport.node.getAttribute('class'), {
            maxZoom: 6,
            minZoom: 0.3,
            fit: false,
            center: false,
            contain: false,
            beforePan: function(oldPan, newPan){
              var stopHorizontal = false;
              var stopVertical = false;
              var gutterWidth = 100;
              var gutterHeight = 100;
              this.updateBBox();
              var sizes = this.getSizes();
              var leftLimit = -((sizes.viewBox.x + sizes.viewBox.width) * sizes.realZoom) + gutterWidth;
              var rightLimit = sizes.width - gutterWidth - (sizes.viewBox.x * sizes.realZoom);
              var topLimit = -((sizes.viewBox.y + sizes.viewBox.height) * sizes.realZoom) + gutterHeight;
              var bottomLimit = sizes.height - gutterHeight - (sizes.viewBox.y * sizes.realZoom);

              customPan = {}
              customPan.x = Math.max(leftLimit, Math.min(rightLimit, newPan.x))
              customPan.y = Math.max(topLimit, Math.min(bottomLimit, newPan.y))

              return customPan;
            }
          });
        },


        go_to_floor: function(floor_number) {
          var floor_element = this.viewport.select('#floor-' + floor_number)
          if(floor_element){
            //hide other floors
            this.viewport.selectAll( ".floor" ).forEach( function( element, i ) {
              element.removeClass( "active" );
              element.addClass( "hidden" );
            });

            //show this floor
            floor_element.addClass("active");
            floor_element.removeClass( "hidden" );
          }else{
            //remove active class from floors
            this.viewport.selectAll( ".floor" ).forEach( function( element, i ) {
              element.removeClass( "active" );
            });
          }

          this.pan_zoom_controls.reset();
          this.pan_zoom_controls.resize();
          this.pan_zoom_controls.fit();
          this.pan_zoom_controls.fit(); //fit has to be called twice for some reason
          this.pan_zoom_controls.center();
        },


        highlight_element: function( element, go_to_floor ) {
          this.viewport.selectAll( ".animating" ).forEach( function( element, i ) {
            element.removeClass( "animating" );
          } );
          element.addClass( "animating" );
          this.fade_in_out( element, 20, 0 );

          if(go_to_floor){
            var floor_element = this.find_element_parent_floor(element);
            var floor_number = floor_element.node.id.replace('floor-','');
            this.go_to_floor(floor_number);
          }

          return element;
        },


        fade_in_out: function( element, repeat, i ) {
          var self = this;
          var animation_duration = 750;
          element.animate( {
              opacity: 1
            },
            animation_duration,
            mina.linear,
            function() {
              element.animate( {
                  opacity: 0
                },
                animation_duration,
                mina.linear,
                function() {
                  i++;
                  if ( i < repeat && element.hasClass( "animating" ) ) {
                    self.fade_in_out( element, repeat, i );
                  } else {
                    element.removeClass( "animating" );
                  }
                }
              );
            }
          );
        },


        find_element_parent_floor: function( element ) {
          var floor = null;
          var current_element = element;
          while ( floor === null && current_element ) {
            var parent = current_element.parent();
            if ( parent.hasClass( "floor" ) ) {
              floor = parent;
            }
            current_element = parent;
          }

          return floor;
        },

    };


    return map;
}());
