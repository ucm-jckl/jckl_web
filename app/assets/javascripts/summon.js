
//= require src/utilities
//= require src/cross_site

JCKL = (typeof JCKL === 'undefined') ? {} : JCKL;
JCKL.SUMMON = ( function() {

  var summon = {

    init: function() {
      //this.load_custom_css();
      //this.insert_header();
      return true;
    },

    // Load a custom css file
    load_custom_css: function(){
         var css = $( document.createElement( "link" ) );
         css.attr({
             href: "https://library.ucmo.edu/css/summon.css",
             rel: "stylesheet",
             type: "text/css",
             media: "all"
         });
         $( "head" ).append( css );
     },

     // Insert JCKL header
     insert_header: function(){
       $('.header').prepend('<div id="jckl-header"></div>');
       JCKL.CROSS_SITE.insert_header(function(){
         $('#jckl-header').removeClass('header');
         $('.header > #jckl-header').replaceWith(function() {
           return $('#jckl-header', this);
          });
       });
     }
  };


  return summon
}() );



$(document).ready(function(){
  JCKL.SUMMON.init();
})
