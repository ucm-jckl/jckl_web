//= require src/cross_site
//= require src/utilities

JCKL = (typeof JCKL === 'undefined') ? {} : JCKL;
JCKL.LIBAPPS = ( function() {

  var libapps = {

    init: function() {

      this.identify_edit_mode();
      this.identify_profile_box();
      this.add_best_practices_link();
      this.fix_subpage_dropdowns();
      this.modify_email_link();
      this.modify_profile_box_buttons();
      this.shorten_profile_box_subject_names();
      setTimeout( function() {
        JCKL.LIBAPPS.modify_profile_box_buttons()
      }, 2000 );
      this.fix_single_page_guide_names();
      this.change_libcal_unavailable_language();
      if($( ".libguides-az-page").length > 0){
        this.record_database_page_events();
      }
      return true;
    },

    //add a special class to the body tag on Edit Mode pages
    identify_edit_mode: function() {
      if ( $( ".s-lib-cmd-bar" ).length > 0 ) {
        $( "body" ).addClass( "libguides-edit-mode" );
      }
    },


    //add a special class to profile boxes
    identify_profile_box: function(){
      if($('.s-lib-profile-container').length > 0){
        $('.s-lib-profile-container').each(function(i){
          $(this).parents('.s-lib-box').addClass('s-lib-profile-box');
        })
      }
    },

    //add best practices guide link to edit mode pages
    add_best_practices_link: function() {
      if ( $( ".libguides-edit-mode" ).length > 0 ) {

        var li = $( document.createElement( "li" ) );

        var a = $( document.createElement( "a" ) );
        a.attr( {
          href: "http://guides.library.ucmo.edu/best-practices",
          target: "_blank"
        } );

        var i = $( document.createElement( "i" ) );
        i.addClass( "fa fa-star-o fa-lg" );

        $( "#s-lg-admin-command-bar .nav:first-child" ).append( li );
        li.append( a );
        a.append( i ).append( "JCKL Best Practices" );

      }
    },

    //Add page navigation box listing all other boxes
    add_page_nav: function() {
      if ( $( ".libguides-page-navigation" ).length > 0 ) {

        var links = [];
        $( ".content-column .s-lib-box" ).each( function() {
          var a = document.createElement( "a" );
          a.href = "#" + $( this ).attr( "id" );
          var heading = $( ".s-lib-box-title", this ).clone();
          $( a ).append( heading.contents() );
          $( a ).children().remove();
          var li = document.createElement( "li" );
          $( li ).append( a );
          links.push( li );
        } );

        for ( var i = 0; i < links.length; i++ ) {
          $( ".libguides-page-navigation ul" ).append( links[ i ] );
        }
      }
    },

    //Fix dropdown nav menus for subpages on enhanced tab layout
    fix_subpage_dropdowns: function() {
      if ( $( ".libguides-template-enhanced-tabs" ).length > 0 ) {

        $( ".s-lg-tab-top-link" ).addClass( "dropdown-toggle" )
          .attr( "data-toggle", "dropdown" );
        $( ".s-lg-tab-drop" ).remove();

      }


    },

    //Modify the Email Me profile link so that it points to the library contact form instead of mailto
    modify_email_link: function() {
      if ( $( ".s-lib-profile-email" ).length > 0 ) {
        if ( $( ".s-lib-profile-email a" ).attr( "href" ) !== undefined ) {
          //get the librarian's email address minus the @ucmo.edu part
          var email_address = $( ".s-lib-profile-email a" ).attr( "href" ).replace( "mailto:", "" ).replace( "@ucmo.edu", "" );

          //build a contact page URL and make the Email Me link point to it
          var url = "https://library.ucmo.edu/contact/employee/" + email_address;
          $( ".s-lib-profile-email a" ).attr( "href", url );
        }
      }
    },

    //Change the CSS of the profile box widget buttons so they are all similar
    modify_profile_box_buttons: function() {
      if ( $( '.s-lib-profile-container' ).length > 0 ) {
        $( '.s-lib-profile-email a, .s-lib-profile-widget-la button, .s-lib-profile-widget-lc button' )
          .removeClass( 'label label-info' )
          .addClass( 'btn btn-default' )
          .css( {
            'background': '#ffffff',
            'color': 'white',
            'font-size': '14px',
            'padding': '8px 20px',
            'cursor': 'pointer',
            'width': '100%',
            'border': '1px solid #cccccc',
            'border-radius': '4px',
            'font-weight': 'normal',
            'display': 'inline-block',
            'vertical-align': 'middle',
            'white-space': 'nowrap',
            'text-align': 'center'
          } );

          $( '.s-lib-profile-email a' ).css('background-color', '#16a085');
          $( '.s-lib-profile-widget-la button' ).css('background-color', '#2980b9');
          $( '.s-lib-profile-widget-lc button' ).css('background-color', '#34495e');

      }
    },


    // Shorten subject names in profile boxes by removing the trailing degree names
    shorten_profile_box_subject_names: function(){
      $('.s-lib-profile-subjects a').each(function(i){
        console.log($(this).text());
        var subject = $(this).text().trim();
        var shortened_subject = subject.trim().replace(/\(.*?\)$/,'').trim();
        $(this).empty().append(shortened_subject);
      });
    },

    //Remove page names from html title and breadcrumb for single page guides
    fix_single_page_guide_names: function() {
      if ( $( ".libguides-template-single-page" ).length > 0 ) {
        //breadcrumb
        $( "#s-lib-bc-page" ).remove();
        var guide = $( "#s-lib-bc-guide" );
        guide.addClass( "active" );
        var guide_title = guide.find( "a" ).contents()[ 0 ];
        guide.empty().append( guide_title );

        //html title
        var title_parts = $( "title" ).text().split( " - " );
        var new_title = title_parts[ 1 ] + " - " + title_parts[ 2 ].replace( "Guides at ", "" );
        $( "title" ).empty().append( new_title );
      }
    },


    //Remove the term "padding" from "unavailable/padding" in LibCal room booking page
    change_libcal_unavailable_language: function(){
      if( $('#s-lc-public-main #eq-time-grid-legend .label-eq-unavailable').length > 0 ){
        $('#s-lc-public-main #eq-time-grid-legend .label-eq-unavailable').parent('span').contents()[1].data = ' Unavailable';
      }
    },


    record_database_page_events: function() {
      window.springSpace.public.Public.prototype.loadAzListOriginal = window.springSpace.public.Public.prototype.loadAzList;
      window.springSpace.public.Public.prototype.loadAzList = function(config){
        window.springSpace.public.Public.prototype.loadAzListOriginal(config);
        setTimeout(function(){
          //record when a letter in the Browse By Letter list is clicked
          $( ".libguides-az-page .s-lg-az-header" ).unbind().click( function() {
            var event_label = $( this ).text();
            ga(
              'send',
              'event',
              'Database Page',
              'Database Letter Click',
              event_label,
              1
            );
          } );

          //record when a subject is selected
          $( ".libguides-az-page #s-lg-sel-subjects" ).unbind().change( function() {
            var event_label = $( this ).find('option:selected').text().replace(/ \([0-9]{1,3}\)/,'');
            ga(
              'send',
              'event',
              'Database Page',
              'Subject Click',
              event_label,
              1
            );
          } );

          //record when the clear filter button is clicked
          $( ".libguides-az-page #s-lg-az-reset" ).unbind().click( function() {
            ga(
              'send',
              'event',
              'Database Page',
              'Clear Filters Click',
              '',
              1
            );
          } );

          //record when a database title link is clicked
          $( ".libguides-az-page .s-lg-az-result-title a" ).unbind().click( function() {
            var event_label = $( this ).text();
            ga(
              'send',
              'event',
              'Database Page',
              'Database Title Click',
              event_label,
              1
            );
          } );

        }, 1000)
      }


      //record when text in the database search field is changed
      $( ".libguides-az-page #s-lg-az-search" ).parents('form').submit( function() {
        var event_label = $( this ).find('#s-lg-az-search').val();
        ga(
          'send',
          'event',
          'Database Page',
          'Database Search',
          event_label,
          1
        );
      } );

    },

  };


  return libapps;
}() );


$( document ).ready( function() {
  JCKL.CROSS_SITE.init();
  JCKL.LIBAPPS.init();
} );
