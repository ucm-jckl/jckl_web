// Libraries and plugins
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require popper.js
//= require anytime.5.1.2

//= require src/utilities
//= require src/cross_site

$(document).ready(function(){
  JCKL.CROSS_SITE.init();
})
