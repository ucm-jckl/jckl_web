// Libraries and plugins
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require popper.js
//= require bootstrap
//= require anytime.5.1.2

//= require src/utilities
//= require src/cross_site

JCKL = (typeof JCKL === 'undefined') ? {} : JCKL;
JCKL.SERSOL = ( function() {

  var sersol = {

    init: function() {

        this.change_page_title();
        this.load_header_and_footer_angular();
        this.set_fixed_page_container();

      return true;
    },

    // Change the page title so it doesn't mention books
    change_page_title: function(){
      var scope = angular.element($(".search-page-header")).scope();
      scope.$apply(function(){
        scope.titleSearchCtrl.titleSearchPageHeader.value = 'Find Journals at JCKL';
      });
      $(".search-page-header").show();
    },


    // Load the header and footer HTML via Angular app used by SerSol
    load_header_and_footer_angular: function(){
      $.get(JCKL.CROSS_SITE.base_url() + 'header', function( data ){
        var scope = angular.element($('#header')).scope();
        scope.$apply(function(){
          scope.brandingCtrl.brandingHeader.value = data.toString();
        });
      });

      $.get(JCKL.CROSS_SITE.base_url() + 'footer', function( data ){
        var scope = angular.element($('#footer')).scope();
        scope.$apply(function(){
          scope.brandingCtrl.brandingFooter.value = data.toString();
        });
      });
    },


    // Set the main container as .container instead of .container-fluid
    set_fixed_page_container: function() {
      $( "body .container-fluid" ).addClass('container').removeClass('container-fluid');
    },


  };

  return sersol;
}() );

$(document).ready(function(){
  $('body').append("<script> \
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){  \
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), \
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) \
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); \
      ga('create', 'UA-19173945-5', 'auto'); \
      ga('send', 'pageview'); \
  </script>");

  if( typeof(angular) != 'undefined' ){
    JCKL.SERSOL.init();

  }else{
    // Handle non-angular link resolver loading page elements and GA event tracking
    JCKL.CROSS_SITE.init();
    if( $('.looking-for-label, .apaformat, .custom-links-rectangle').length > 0 ){
      setTimeout(function(){
        // Link Resolver page loads http://wd8cd4tk5m.search.serialssolutions.com/?url_ver=Z39.88-2003&ctx_ver=Z39.88-2003&ctx_enc=info:ofi/enc:UTF-8&rft_id=info:doi/&rft_val_fmt=info:ofi/fmt:kev:mtx:journal&rft.genre=article&rft.aulast=Egorov&rft.aufirst=S.&rft.issn=20737564&rft.volume=&rft.issue=60&rft.date=2017&rft.spage=322&rft.epage=331&rft.pages=322-331&rft.artnum=&rft.title=Dialog+so+Vremenem&rft.atitle=Heretics+and+authority%3a+Bulgarian+bogomils%27+social+%22Tactics%22+%28X%3Csup%3Eth%3C%2fsup%3E+century%29&rfr_id=info:sid/Elsevier:Scopus
        var citation = $('.main-container h2').text().trim();
        ga(
          'send',
          'event',
          'Link Resolver',
          'Link Resolver - Page Load',
          citation,
          1
        );

        // Link Resolver link clicks
        $('#mobile-main-page a, #mobile-action-page a').click(function(){
          var link = $(this).text().trim() + ' ('+$(this).attr('href')+')';
          ga(
            'send',
            'event',
            'Link Resolver',
            'Link Resolver - Link Click',
            link,
            1
          );
        })
      }, 1000);
    }
  }
})
