// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.

// Libraries and plugins
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require turbolinks
//= require popper.js
//= require bootstrap
//= require highcharts/highcharts
//= require highcharts/highcharts-more
//= require highcharts/modules/exporting
//= require snap.svg.js
//= require svg-pan-zoom
//= require anytime.5.1.2
//= require readmore
//= require owl.carousel
//= require chroma-js
//= require bootstrap-datepicker
//= require bootstrap-table
//= require bootstrap-table/extensions/export/bootstrap-table-export
//= require tableexport.jquery.plugin
//= require momentjs
//= require lodash
//= require blazy



// Application code
//= require_tree ./src

$( document ).on( 'turbolinks:load', function() {
  jQuery = $;
  JCKL.INDEX.init();
  JCKL.LEGACY_SEARCH.init();
  JCKL.MEETING_ROOMS.init();
  JCKL.MAP_PAGE.init();
  JCKL.ABSENCES.init();
  JCKL.STATS.init();
  JCKL.CLF.init();
  JCKL.GOOGLE_ANALYTICS.init();
  JCKL.FORMS.init();
  JCKL.PLUGINS.init();
} );
