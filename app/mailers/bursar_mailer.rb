class BursarMailer < ActionMailer::Base
  def error_notification(action_type, error_messages)
    recipients = ['welker@ucmo.edu'] # , 'fiegenbaum@ucmo.edu', 'agueros@ucmo.edu', 'horne-popp@ucmo.edu']
    @error_messages = error_messages
    @action_type = action_type

    mail(
      to: recipients,
      reply_to: recipients,
      subject: "#{@action_type} Errors (#{Time.now.strftime('%Y-%m-%d')})"
    )
  end
end
