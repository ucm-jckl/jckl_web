class ClfOrderMailer < ActionMailer::Base
  def send_email(order)
    recipients = [order.contact_email, 'clfbooks@ucmo.edu', 'marnholtz@ucmo.edu']

    @order = order

    mail(
      to: recipients,
      reply_to: recipients,
      subject: "Children's Literature Festival Online Order: #{order.contact_name}, #{order.school_name}",
      from: order.contact_email
    )
  end
end
