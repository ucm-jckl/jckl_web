class AbsencesMailer < ApplicationMailer
  def send_email(absence)
    @absence = absence

    # Set recipients
    recipients = []
    recipients += ALL_ABSENCES_EMAIL
    recipients << @absence.employee.email


    if @absence.employee.id == 15 || @absence.employee.id == 46
      recipients << "horne-popp@ucmo.edu"
    end

    if @absence.is_new
      recipients << @absence.employee&.supervisor&.email
      recipients << @absence.submitter&.email
    elsif @absence.is_edited && @absence.times_have_been_altered
      recipients << @absence.employee&.supervisor&.email
      recipients << @absence.editor&.email
    elsif @absence.is_approved
      # No one extra
      nil
    elsif @absence.is_rejected
      # No one extra
      nil
    elsif @absence.is_deleted
      recipients << @absence.employee&.supervisor&.email
    end

    # Set subject line
    @subject_prefix = nil
    if @absence.is_new
      @subject_prefix = "New Absence Request Submitted"
    elsif @absence.is_edited
      @subject_prefix = "Absence Request Edited"
    elsif @absence.is_approved
      @subject_prefix = "Absence Request Approved"
    elsif @absence.is_rejected
      @subject_prefix = "Absence Request Rejected"
    elsif @absence.is_deleted
      @subject_prefix = "Absence Request Deleted"
    end
    subject = "#{@subject_prefix}: #{@absence.employee.first_last} - #{@absence.date_range}"

    # Send email
    mail(
      to: recipients,
      reply_to: recipients,
      subject: subject
    )
  end
end
