class NewBookRequestsMailer < ApplicationMailer
  def send_email(user, request)
    @request = request
    @user = user

    recipients = BOOK_REQUESTS_EMAIL
    recipients += [@user.email]
    recipients += [@request.subject_area&.subject_librarian&.email]
    recipients += @request.fund_code&.librarians&.pluck(:email) || []

    subject = "Online Purchase Request: #{@request.title}"

    mail(
      to: recipients,
      reply_to: recipients,
      subject: subject
    )
  end
end
