class DvdMailer < ApplicationMailer

  def send_email( dvd )

    recipients = SYSTEM_EMAIL
    subject = "New DVD: #{dvd.title} (#{dvd.id})"

    @dvd = dvd

    mail(
      to: recipients,
      reply_to: recipients,
      subject: subject
    )

  end

end
