class EzproxyMailer < ApplicationMailer

  def send_email( host, link, referrer, ip )

    recipients = EZPROXY_EMAIL
    subject = "EZproxy host error: #{host}"

    @host = host
    @link = link
    @referrer = referrer
    @ip = ip

    mail(
      to: recipients,
      reply_to: recipients,
      subject: subject
    )

  end

end
