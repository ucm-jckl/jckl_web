class PlannerEmailRemindersMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.planner_email_reminders_mailer.send.subject
  #
  def send_email(employee, event, timeframe)
    @event = event
    mail(
      to: employee.email,
      reply_to: employee.email,
      subject: "#{timeframe}: #{event.summary}"
    )
  end
end
