class ApplicationMailer < ActionMailer::Base
  default from: 'library@ucmo.edu'
  layout 'mailer'
  @include_unmonitored_note = true

  SYSTEM_EMAIL = ['welker@ucmo.edu']
  EZPROXY_EMAIL = ['jklein@ucmo.edu']
  ALL_ABSENCES_EMAIL = ['gieselman@ucmo.edu', 'anthes@ucmo.edu']
  ADMIN_OFFICE_EMAIL = ['gieselman@ucmo.edu', 'anthes@ucmo.edu', 'staines@ucmo.edu', 'deanstudent-jckl@ucmo.edu']
  REFERENCE_EMAIL = ['reference-jckl@ucmo.edu', 'ask@ucmo.libanswers.com']
  ROOM_BOOKINGS_EMAIL = ['gieselman@ucmo.edu', 'roomsjckl@ucmo.edu', 'deanstudent-jckl@ucmo.edu']
  BOOK_REQUESTS_EMAIL = ['jklein@ucmo.edu', 'skahan@ucmo.edu']
end
