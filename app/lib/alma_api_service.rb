class AlmaApiService

  def self.request(method, endpoint, query_params={}, body={})
    # Build URL
    url = 'https://api-na.hosted.exlibrisgroup.com' + endpoint
    query_params['apikey'] = Rails.application.secrets.alma_api_key
    query_string = [];
    query_params.each do |key, value|
      query_string << (key.to_s + '=' + value.to_s)
    end
    full_url = url + '?' + query_string.join('&')

    # Do the query
    if method == 'GET'
      response = HTTParty.get(full_url, {
        headers: {
          'Accept' => 'application/json',
          'Content-Type' => 'application/json'
        }
      });
    elsif method == 'POST'
      response = HTTParty.post(full_url, {
        headers: {
          'Accept' => 'application/json',
          'Content-Type' => 'application/json'
        },
        body: body.to_json
      });
    elsif method == 'DELETE'
      response = HTTParty.delete(full_url, {
        headers: {
          'Accept' => 'application/json',
          'Content-Type' => 'application/json'
        },
        body: body.to_json
      });
    else
      response = {}
    end

    return response
  end

end
