class GoogleApiService
  require 'googleauth'
  require 'google/apis'
  require 'google/apis/calendar_v3'
  require 'google/apis/sheets_v4'

  APPLICATION_NAME = 'JCKL Web Google API Service'.freeze

  def get_gcal_events(calendar_id, start_time=nil, end_time=nil, q=nil)
    response = gcal_client.list_events(calendar_id,
                                       max_results: 1000,
                                       single_events: true,
                                       order_by: 'startTime',
                                       time_min: start_time&.iso8601 || Time.now.iso8601,
                                       time_max: end_time&.iso8601,
                                       q: q)
    response.items
  end


  def add_gcal_event(calendar_id, summary, date, start_time=nil, end_time=nil, description=nil, transparency=nil, color_id=nil)
    event = Google::Apis::CalendarV3::Event.new()
    event&.summary = summary

    if transparency
      event.transparency = transparency
    end

    if color_id
      event.color_id = color_id
    end

    if description
      event.description = description
    end

    if start_time.present? && end_time.present?
      event.start = {
        time_zone: 'America/Chicago',
        date_time: Chronic.parse("#{date} #{start_time.strftime("%l:%M %p")}").iso8601
      }

      event.end = {
        time_zone: 'America/Chicago',
        date_time: Chronic.parse("#{date} #{end_time.strftime("%l:%M %p")}").iso8601
      }

    else
      event.start = {
        time_zone: 'America/Chicago',
        date: date.iso8601
      }

      event.end = {
        time_zone: 'America/Chicago',
        date: date.iso8601
      }
    end

    gcal_client.insert_event(calendar_id, event)
  end


  def edit_gcal_event(calendar_id, event_id, summary, description, date, start_time=nil, end_time=nil, cancelled=false)
    event = gcal_client.get_event(calendar_id, event_id)
    event&.summary = summary
    event&.description = description
    event&.status = if cancelled then 'cancelled' else 'confirmed' end

    if start_time.present? && end_time.present?
      event.start = {
        time_zone: 'America/Chicago',
        date_time: Chronic.parse("#{date} #{start_time.strftime("%l:%M %p")}").iso8601
      }

      event.end = {
        time_zone: 'America/Chicago',
        date_time: Chronic.parse("#{date} #{end_time.strftime("%l:%M %p")}").iso8601
      }

    else

      event.start = {
        time_zone: 'America/Chicago',
        date: date.iso8601
      }

      event.end = {
        time_zone: 'America/Chicago',
        date: date.iso8601
      }
    end

    result = gcal_client.update_event(calendar_id, event_id, event)
  end


  def delete_gcal_event(calendar_id, event_id)
    gcal_client.delete_event(calendar_id, event_id)
  end


  def clear_google_sheet(sheet_id)

    # Clear spreadsheet and delete all rows and columns
    sheet = sheets_client.get_spreadsheet(sheet_id)
    col_count = sheet.sheets[0].properties.grid_properties.column_count
    row_count = sheet.sheets[0].properties.grid_properties.row_count

    delete_requests = []
    if col_count > 1
      delete_requests << {
        delete_dimension: {
          range: {
            sheet_id: 0,
            dimension: 'COLUMNS',
            start_index: 1,
            end_index: col_count
          }
        }
      }
    end
    if row_count > 1
      delete_requests << {
        delete_dimension: {
          range: {
            sheet_id: 0,
            dimension: 'ROWS',
            start_index: 1,
            end_index: row_count
          }
        }
      }
    end

    if delete_requests.present?
      delete = sheets_client.batch_update_spreadsheet(sheet_id, {requests: delete_requests}, {})
    end

    clear = sheets_client.clear_values(sheet_id, 'Sheet1', Google::Apis::SheetsV4::ClearValuesRequest.new)

    return clear

  end


  def append_to_google_sheet(sheet_id, data)
    # Restrict data table to 2,000,000 cell limit
    cell_limit = 2000000
    columns = data[0].length
    rows = data.length

    if columns * rows > cell_limit
      rows_to_remove = (columns * rows - cell_limit ) / columns
      data.pop(rows_to_remove)
    end

    # Replace nil values with empty strings
    data.map!{|row| row.map!{|v| v || ''}}

    # Update spreadsheet with new data in chunks of 100000 rows
    data.each_slice(100000) do |chunk|
      request_body = Google::Apis::SheetsV4::ValueRange.new
      request_body.values = chunk

      append = sheets_client.append_spreadsheet_value(sheet_id, 'Sheet1', request_body, value_input_option: 'RAW')
    end

  end


  private


  # Set up authorization
  def authorize
    if @authorization.blank?
      file = Tempfile.new('google_application_credentials')
      begin
        # Set up a temp .json file containing credentials since Googleauth requires it in a file rather than secrets.yml
        file.write(Rails.application.secrets.google_application_credentials.to_json.to_s)
        ENV['GOOGLE_APPLICATION_CREDENTIALS'] = file.path
        file.close
        # Create authorization
        scopes = ['https://www.googleapis.com/auth/cloud-platform', 'https://www.googleapis.com/auth/devstorage.read_only', Google::Apis::CalendarV3::AUTH_CALENDAR, 'https://www.googleapis.com/auth/spreadsheets']
        @authorization = Google::Auth.get_application_default(scopes)
      ensure
        file.close
        file.unlink
      end
    end
    @authorization
  end

  # Create/get the Google Calendar API Service
  def gcal_client
    if @gcal_client.blank?
      @gcal_client = Google::Apis::CalendarV3::CalendarService.new
      @gcal_client.client_options.application_name = APPLICATION_NAME
      @gcal_client.authorization = authorize
    end
    @gcal_client
  end

  # Create/get the Google Sheets API Service
  def sheets_client
    if @sheets_client.blank?
      Google::Apis::ClientOptions.default.open_timeout_sec = 1200
      Google::Apis::RequestOptions.default.retries = 50
      @sheets_client = Google::Apis::SheetsV4::SheetsService.new
      @sheets_client.client_options.application_name = APPLICATION_NAME
      @sheets_client.authorization = authorize
    end
    @sheets_client
  end
end
