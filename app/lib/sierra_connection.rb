class SierraConnection
  # Class variables
  HOST      = 'quest-db.searchmobius.org'.freeze
  PORT      = 1032
  SSLMODE   = 'require'.freeze
  DBNAME    = 'iii'.freeze
  USER      = 'ucmscripts'.freeze
  PASSWORD  = Rails.application.secrets.sierra_password

  # Instance variables
  @connection

  def initialize
    connect
  end

  # Create the connection
  def connect
    @connection = PG.connect(
      host: HOST,
      port: PORT,
      sslmode: SSLMODE,
      dbname: DBNAME,
      user: USER,
      password: PASSWORD
    )
  end

  # Close the connection
  def disconnect
    @connection.close
  end

  # Execute a SELECT query
  def select(query)
    result = @connection.exec(query)
    result
  end
end
