class LibcalApiService

  API_BASE_URL = 'https://api2.libcal.com/1.1'

  def initialize
    authorize
  end

  def authorize
    response = HTTParty.post("#{API_BASE_URL}/oauth/token",
                  body: {
                    'client_id' => Rails.application.secrets.libcal_client_id,
                    'client_secret' => Rails.application.secrets.libcal_client_secret,
                    'grant_type' => 'client_credentials'
                  })
    @access_token = JSON.parse(response.parsed_response)['access_token']
  end

  def get_bookings(space_id)

    bookings = HTTParty.get("#{API_BASE_URL}/space/bookings?eid=#{space_id}&limit=100",
                headers: {
                  'Authorization' => "Bearer #{@access_token}"
                }).parsed_response
  end

end
