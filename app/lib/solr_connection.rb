module SolrConnection
  URL_BASE = 'http://localhost:8983/solr/'.freeze

  def self.connect(core_name)
    solr = RSolr.connect url: "#{URL_BASE}/#{core_name}"
  end
end
