class SierraApiService
  API_BASE_URL = 'https://avalon.searchmobius.org/iii/sierra-api/v4'.freeze

  PICKUP_LOCATIONS = {
    'ckb' => 'UCM Warrensburg, Circulation Desk',
    'cky' => 'UCM Lee\'s Summit'
  }

  def initialize
    authorize
  end

  def authorize
    response = HTTParty.post("#{API_BASE_URL}/token", headers: {
                               'Content-Type' => 'application/json',
                               'Authorization' => "Basic #{Rails.application.secrets.sierra_api_auth_header}"
                             })
    @access_token = response.parsed_response['access_token']
  end

  def get_patron_by_barcode(barcode)
    response = HTTParty.get("#{API_BASE_URL}/patrons/find?barcode=#{barcode}&fields=id,names,barcodes,emails,patronType,patronCodes,homeLibraryCode,message,blockInfo,addresses,phones,moneyOwed",
                            headers: {
                              'Content-Type' => 'application/json',
                              'Authorization' => "Bearer '#{@access_token}'"
                            })

    patron = response.parsed_response
  end

  def get_checkouts_by_patron_id(patron_id)
    checkouts = HTTParty.get("#{API_BASE_URL}/patrons/#{patron_id}/checkouts?fields=id,item,dueDate,callNumber,barcode,numberOfRenewals,outDate",
        headers: {
          'Content-Type' => 'application/json',
          'Authorization' => "Bearer '#{@access_token}'"
        }).parsed_response['entries']

    if checkouts.present?
      item_ids = checkouts&.map{|c| c['item'].gsub("#{API_BASE_URL}/items/", '')}
      items = HTTParty.get("#{API_BASE_URL}/items?id=#{item_ids.join(',')}",
          headers: {
            'Content-Type' => 'application/json',
            'Authorization' => "Bearer '#{@access_token}'"
          }).parsed_response['entries']

      bib_ids = items&.map{|i| i['bibIds']}&.flatten
      bibs = HTTParty.get("#{API_BASE_URL}/bibs?id=#{bib_ids&.join(',')}",
          headers: {
            'Content-Type' => 'application/json',
            'Authorization' => "Bearer '#{@access_token}'"
          }).parsed_response['entries']


      checkout_data = []
      checkouts&.each do |checkout|
        item_id = checkout['item'].gsub("#{API_BASE_URL}/items/", '')
        item = items&.select{|i| i['id'] == item_id}&.first

        bib_id = item&.dig('bibIds', 0)
        bib = bibs&.select{|b| b['id'] == bib_id}&.first

        checkout_data << {
          checkout_id: checkout['id'],
          title: bib&.dig('title'),
          author: bib&.dig('author'),
          call_number: item&.dig('callNumber')&.gsub('|f', ''),
          due_date: checkout['dueDate'],
          number_of_renewals: checkout['numberOfRenewals'],
          checkout_date: Chronic.parse(checkout['outDate']).strftime('%Y-%m-%d'),
          catalog_url: "http://avalon.searchmobius.org/record=b#{bib&.dig('id')}",
          is_inn_reach: checkout['barcode'].blank? || checkout['callNumber'].blank?,
          api_data: {
            checkout: checkout,
            item: item,
            bib: bib
          }
        }
      end

      checkout_data.sort_by{|checkout| checkout[:due_date]}
    else
      []
    end
  end

  def renew_checkout(checkout_url)
    renewal_response = HTTParty.post("#{checkout_url}/renewal", headers: {
         'Content-Type' => 'application/json',
         'Authorization' => "Bearer '#{@access_token}'"
       }).parsed_response
    return renewal_response
  end

  def get_holds_by_patron_id(patron_id)
    holds = HTTParty.get("#{API_BASE_URL}/patrons/#{patron_id}/holds?fields=record,frozen,placed,notNeededAfterDate,notWantedBeforeDate,location,pickupLocation,status,recordType,note",
        headers: {
          'Content-Type' => 'application/json',
          'Authorization' => "Bearer '#{@access_token}'"
        }).parsed_response['entries']

    if holds.present?

      # Build the master initial holds array
      holds_data = []
      holds.each do |h|
        h['bibId'] = nil
        h['itemId'] = nil
        h['callNumber'] = nil
        h['title'] = nil
        h['author'] = nil
        holds_data << h
      end

      # Add record  numbers to master array
      holds.select{|h| h['recordType'] == 'j'} # NOTE: Sierra doesn't have an API endpoint for volume records at this time
      holds.select{|h| h['recordType'] == 'i'}.each do |h|
        h['itemId'] = h['record'].gsub("#{API_BASE_URL}/items/", '')
      end
      holds.select{|h| h['recordType'] == 'b'}.each do |h|
        h['bibId'] = h['record'].gsub("#{API_BASE_URL}/bibs/", '')
      end


      # Get item record data for item-level holds and add it to the master holds array
      item_ids = holds_data.map{|h| h['itemId']}.compact
      items = HTTParty.get("#{API_BASE_URL}/items?id=#{item_ids.join(',')}",
          headers: {
            'Content-Type' => 'application/json',
            'Authorization' => "Bearer '#{@access_token}'"
          }).parsed_response['entries']
      items&.each do |i|
        #Find the matching hold record and add item data
        matching_hold = holds_data.select{|h| h['itemId'] == i['id']}.first
        matching_hold['bibId'] = i['bibIds'].first
        matching_hold['callNumber'] = i['callNumber']
      end


      # Get bib record data for item-level holds and add it to the master holds array
      bib_ids = holds_data.map{|h| h['bibId']}.compact
      bibs = HTTParty.get("#{API_BASE_URL}/bibs?id=#{bib_ids.join(',')}",
          headers: {
            'Content-Type' => 'application/json',
            'Authorization' => "Bearer '#{@access_token}'"
          }).parsed_response['entries']
      bibs&.each do |b|
        #Find the matching hold record and add bib data
        matching_hold = holds_data.select{|h| h['bibId'] == b['id']}.first
        matching_hold['title'] = b['title']
        matching_hold['author'] = b['author']
        matching_hold['catalogUrl'] = "http://avalon.searchmobius.org/record=b#{b['id']}"
      end

      # Get item and bib record data for INN-Reach items sepaprately because they require individual API calls to a different endpoint (lame Sierra API)
      holds_data.each do |h|
        next unless h['itemId']&.include?('@')
        bib = HTTParty.get("#{API_BASE_URL}/bibs/#{h['itemId']}",
            headers: {
              'Content-Type' => 'application/json',
              'Authorization' => "Bearer '#{@access_token}'"
            })

        h['title'] = bib['title']
        h['author'] = bib['author']

      end
      holds_data.sort_by{|h| h['placed']}
    else
      []
    end
  end


  def cancel_hold(hold_url)
    HTTParty.delete("#{hold_url}", headers: {
             'Content-Type' => 'application/json',
             'Authorization' => "Bearer '#{@access_token}'"
           }).parsed_response
  end


  def change_hold_location(hold_url, hold_location_code)
    HTTParty.put("#{hold_url}", headers: {
             'Content-Type' => 'application/json',
             'Authorization' => "Bearer '#{@access_token}'"
           }, body: {
             'pickupLocation' => hold_location_code
           }.to_json).parsed_response
  end

  def get_fines_by_patron_id(patron_id)
    fines = HTTParty.get("#{API_BASE_URL}/patrons/#{patron_id}/fines",
        headers: {
          'Content-Type' => 'application/json',
          'Authorization' => "Bearer '#{@access_token}'"
        }).parsed_response['entries']

    if fines.present?
      item_ids = fines.map{|f| f['item']&.gsub("#{API_BASE_URL}/items/", '')}&.compact
      items = HTTParty.get("#{API_BASE_URL}/items?id=#{item_ids.join(',')}",
          headers: {
            'Content-Type' => 'application/json',
            'Authorization' => "Bearer '#{@access_token}'"
          }).parsed_response['entries']

      bib_ids = items&.map{|i| i['bibIds']}&.flatten
      bibs = HTTParty.get("#{API_BASE_URL}/bibs?id=#{bib_ids&.join(',')}",
          headers: {
            'Content-Type' => 'application/json',
            'Authorization' => "Bearer '#{@access_token}'"
          })&.parsed_response['entries']


      fines_data = []
      fines.each do |fine|
        skip unless fine.present?

        item = {}
        bib = {}

        if fine['item']
          item_id = fine['item'].gsub("#{API_BASE_URL}/items/", '')
          item = items.select{|i| i['id'] == item_id}.first

          bib_id = item&.dig('bibIds', 0)
          bib = bibs&.select{|b| b['id'] == bib_id}&.first
        end

        fines_data << {
          fine_id: fine['id'],
          title: if bib.present? then bib['title'] else '' end,
          author: if bib.present? then bib['author'] else '' end,
          call_number: if item.present? then item['callNumber']&.gsub('|f', '') else '' end,
          assessed_date: fine['assessedDate'],
          description: fine['description'],
          fine_total: (fine['itemCharge']&.to_f + fine['processingFee']&.to_f + fine['billingFee']&.to_f),
        }
      end

      fines_data.sort_by{|fine| fine[:assessed_date]}
    else
      []
    end
  end

end
