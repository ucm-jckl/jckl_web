module WebCrawler
  def self.crawl
    results = []

    Spidr.start_at(
      'https://library.ucmo.edu',
      hosts: ['library.ucmo.edu', 'guides.library.ucmo.edu'],
      ignore_links: [/dvds\/browse/, /\/roomschedules\/book/, /\/map\?/, /\.css/, /\.js/]
    ) do |spider|
      spider.every_html_page do |page|
        metadata = {}
        metadata[:url] = page.url.to_s
        metadata[:id] = metadata[:url]
        metadata[:title] = page.title&.strip&.tr("\r\n", '')&.squeeze(' ')
        metadata[:headings] = page.doc&.css('.content-body h1, .content-body h2, .content-body h3, .content-body h4, #s-lg-guide-name, .s-lib-box h1, .s-lib-box h2, .s-lib-box-h3, .s-lib-box h4')&.inner_html&.strip&.squeeze(' ')
        metadata[:paragraphs] = page.doc&.css('.content-body p, .s-lib-box p')&.inner_html&.strip&.squeeze(' ')
        metadata[:description] = page.doc&.at_css('meta[name=description], meta[name="DC.Description"]')&.get_attribute('content')

        results << metadata
        pp metadata
      end
    end

    solr = SolrConnection.connect('jckl_website_search')
    solr.delete_by_query('*:*')
    solr.add(results)
    solr.commit
  end
end
