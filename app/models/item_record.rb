class ItemRecord < ApplicationRecord

  # Fetch item records from Sierra
  def self.fetch_data
    sierra_connection = SierraConnection.new

    puts 'Querying item records'
    item_query = "select i.id as item_record_id, bil.bib_record_id, i.location_code, irp.call_number, i.barcode,
      i.itype_code_num, i.item_status_code, i.price, i.checkout_total, i.renewal_total, i.internal_use_count,
      i.year_to_date_checkout_total, i.last_year_to_date_checkout_total, i.inventory_gmt, i.last_checkout_gmt,
      i.record_creation_date_gmt as item_record_creation_date_gmt
      from sierra_view.item_view i
      left join sierra_view.item_record_property irp on irp.item_record_id=i.id
      left join sierra_view.bib_record_item_record_link bil on i.id=bil.item_record_id
      where i.location_code like 'ck%'"
    item_records = sierra_connection.select(item_query).to_a

    puts 'Querying bib records'
    bib_query = "select b.id, b.record_num, brp.best_title_norm, brp.best_author_norm, brp.publish_year,
      brp.material_code, b.record_creation_date_gmt
      from sierra_view.bib_view b
      left join sierra_view.bib_record_property brp on brp.bib_record_id=b.id
      left join sierra_view.bib_record_location brl on brl.bib_record_id=b.id
      where brp.bib_level_code in ('m','s')
      and brp.material_code='a'
      and brl.location_code like 'ck%' and brl.location_code not like 'ckw%'"
    bib_records = {}
    sierra_connection.select(bib_query).to_a.each do |b|
      bib_records[b['id']] = b
    end

    puts 'Querying ISN records'
    isn_query = "select m020a.content, b.record_num
      from sierra_view.bib_view b
      join sierra_view.subfield_view m020a on b.record_num = m020a.record_num
      where b.record_num in (#{bib_records.values.map{|b| b['record_num']}.join(',')})
      and m020a.marc_tag = '020'
      and m020a.tag = 'a'
      and m020a.record_type_code = 'b'"
    isn_records = {}
    sierra_connection.select(isn_query).to_a.each do |r|
      isn_records[r['record_num']] ||= r['content']
      isn_records[r['record_num']] += r['content']
    end
    sierra_connection.disconnect


    full_records = []
    item_records.uniq{|i| i['item_record_id']}.each_with_index do |i, index|
      puts "Combining records #{index+1} of #{item_records.length}"
      bib_record = bib_records[i['bib_record_id']]
      next unless bib_record.present?
      full_record = i
      full_record['bib_record_num'] = bib_record['record_num']
      full_record['best_title_norm'] = bib_record['best_title_norm']
      full_record['best_author_norm'] = bib_record['best_author_norm']
      full_record['publish_year'] = bib_record['publish_year']
      full_record['material_code'] = bib_record['material_code']
      full_record['bib_record_creation_date_gmt'] = bib_record['record_creation_date_gmt']
      full_record['isns'] = isn_records[bib_record['record_num']]
      full_records << full_record
    end

    # Write results to database and drop existing db records
    ItemRecord.delete_all
    full_records.each_slice(10000).with_index do |chunk, i|
      puts "Writing records batch #{i+1} of #{full_records.each_slice(10000).to_a.length} to database"
      item_records = []
      chunk.each do |r|
        item_records << ItemRecord.new(r)
      end
      ItemRecord.import(item_records)
    end

  end


  # Format data in a table for display in HTML or CSV
  def self.to_table
    table = []
    header = ['Item Record ID', 'Bib Record ID', 'Bib Record Number', 'Title', 'Author', 'Publication Year', 'ISNS',
    'Plain Call Number', 'Clean Call Number', 'Sortable Call Number', 'Call Number Class', 'Call Number Class Number',
    'Location Code', 'Barcode', 'Material Code', 'IType Code', 'Item Status Code',  'Item Price',
    'Checkout Total', 'Renewal Total', 'Internal Use Total', 'YTD Checkout Total', 'Last YTD Checkout Total',
    'Last Checkout Date', 'Last Inventory Date', 'Item Record Creation Date', 'Bib Record Creation Date']
    table << header
    ItemRecord.find_each.with_index do |r, i|
      puts "Parsing record #{i}"
      table << [
        r.item_record_id, r.bib_record_id, r.bib_record_num, r.best_title_norm, r.best_author_norm,
        r.publish_year, r.isns, r.call_number, Book.stripped_call_number(r.call_number),
        Book.normalized_call_number(r.call_number), Book.call_number_class(r.call_number), Book.call_number_class_number(r.call_number),
        r.location_code, r.barcode, r.material_code, r.itype_code_num,
        r.item_status_code, r.price,  r.checkout_total, r.renewal_total, r.internal_use_count,
        r.year_to_date_checkout_total, r.last_year_to_date_checkout_total, r.last_checkout_gmt, r.inventory_gmt,
        r.item_record_creation_date_gmt, r.bib_record_creation_date_gmt]
    end

    return table
  end


  def self.to_google_drive

  end


  # Turn into table format and send to Google Sheets in batches of 50,000 and 250,000 per sheet.
  # To avoid limitations of Google Sheets and Tableau, each row is a single cell delimited delimited by ||| and ~~~
  def self.write_to_google_sheets

    # Clear Google Sheets
    s = GoogleApiService.new
    GOOGLE_SHEET_IDS.each_with_index do |id, i|
      puts "===============================\n=== CLEARING #{i+1} (#{id})\n==============================="
      s.clear_google_sheet(id)
    end

    current_sheet = 0
    uses_for_this_sheet = 0
    ItemRecord.find_in_batches(batch_size: 10000).with_index do |chunk, i|

      table = []
      header = [[
        'Item Record ID', 'Bib Record Number', 'Title', 'Author', 'Publication Year', 'ISNS', '~~~',
        'Plain Call Number', 'Clean Call Number', 'Sortable Call Number', 'Call Number Class Number', '~~~',
        'Location Code', 'Barcode', 'Material Code', 'IType Code', 'Item Status Code',  'Item Price', '~~~',
        'Checkout Total', 'Renewal Total', 'Internal Use Total', 'YTD Checkout Total', 'Last YTD Checkout Total', '~~~',
        'Last Checkout Date', 'Last Inventory Date', 'Item Record Creation Date', 'Bib Record Creation Date'
      ].join('|||')]
      table << header if uses_for_this_sheet == 0
      chunk.each do |r|
        table << [[
          r.item_record_id, r.bib_record_num, r.best_title_norm, r.best_author_norm,
          r.publish_year, r.isns, '~~~', r.call_number, Book.stripped_call_number(r.call_number),
          Book.normalized_call_number(r.call_number), Book.call_number_class_number(r.call_number), '~~~',
          r.location_code, r.barcode, r.material_code, r.itype_code_num,
          r.item_status_code, r.price, '~~~', r.checkout_total, r.renewal_total, r.internal_use_count,
          r.year_to_date_checkout_total, r.last_year_to_date_checkout_total,'~~~', r.last_checkout_gmt, r.inventory_gmt,
          r.item_record_creation_date_gmt, r.bib_record_creation_date_gmt
        ].join('|||')]
      end

      begin
        puts "===============================\n=== Writing batch #{i+1} (#{GOOGLE_SHEET_IDS[current_sheet]})\n==============================="
        s.append_to_google_sheet(GOOGLE_SHEET_IDS[current_sheet], table)
        uses_for_this_sheet += 1
        if uses_for_this_sheet >= 8
          current_sheet += 1
          uses_for_this_sheet = 0
        end

      rescue Google::Apis::ClientError => e
        current_sheet += 1
        puts "===============================\n=== Writing batch #{i+1} (#{GOOGLE_SHEET_IDS[current_sheet]})\n==============================="
        s.append_to_google_sheet(GOOGLE_SHEET_IDS[current_sheet], header + table)
        uses_for_this_sheet = 1
      rescue Google::Apis::ServerError => e
        current_sheet += 1
        puts "===============================\n=== Writing batch #{i+1} (#{GOOGLE_SHEET_IDS[current_sheet]})\n==============================="
        s.append_to_google_sheet(GOOGLE_SHEET_IDS[current_sheet], header + table)
        uses_for_this_sheet = 1
      end
    end
  end

end
