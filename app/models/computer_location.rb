class ComputerLocation < ApplicationRecord
  belongs_to :computer
  belongs_to :location
end
