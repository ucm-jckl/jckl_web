class StudyRoom < ApplicationRecord
  has_many :study_room_bookings, dependent: :nullify
  belongs_to :location, optional: true
end
