class Office < ApplicationRecord
  belongs_to :department, optional: true
  belongs_to :primary_employee, class_name: 'Employee', foreign_key: 'primary_employee_id'
end
