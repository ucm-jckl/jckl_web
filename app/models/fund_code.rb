class FundCode < ApplicationRecord
  has_many :librarians, through: :fund_code_librarian_links, class_name: 'Employee'
  has_many :fund_code_librarian_links

  accepts_nested_attributes_for :fund_code_librarian_links, allow_destroy: true
  accepts_nested_attributes_for :librarians, allow_destroy: true


  def code_and_program
    return self.code + ' - ' + self.program
  end
end
