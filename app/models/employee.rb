class Employee < ApplicationRecord
  belongs_to :department
  belongs_to :supervisor, class_name: 'Employee', foreign_key: 'supervisor_id'
  has_many :headed_departments, class_name: 'Department', foreign_key: 'department_head_id', dependent: :nullify
  has_many :subordinates, class_name: 'Employee', foreign_key: 'supervisor_id', dependent: :nullify
  has_many :offices, foreign_key: 'primary_employee_id', dependent: :nullify
  has_many :absences, foreign_key: 'employee_id', dependent: :destroy
  has_many :fund_codes, through: :fund_code_librarian_links
  has_many :fund_code_librarian_links, foreign_key: :librarian_id

  def first_last
    "#{first_name} #{last_name}"
  end

  def last_first
    "#{last_name}, #{first_name}"
  end

  def title_and_rank
    if rank.blank?
      title
    else
      "#{title} - #{rank}"
    end
  end

  def url_slug
    email.gsub(/@.*?$/,'').downcase
  end

  def self.find_by_url_slug(slug)
    return nil if slug.blank?
    Employee.find_by("email like ?", "#{slug}%")
  end

  def library_administrator?
    department&.department_name == 'administration'
  end

  def can_view_absences_of?(employee)
    if employee.id == id || employee.supervisor&.id == id || library_administrator?
      return true
    else
      return false
    end
  end

  def can_approve_absences_of?(employee)
    if employee.supervisor&.id == id || library_administrator?
      return true
    else
      return false
    end
  end

  def leave_time_used_between(start_date, end_date, absence_type, string = true)
    absence_type_id = AbsenceType.find_by(absence_type_name: absence_type)&.id
    minutes_used = AbsenceDate.includes(:absence).where(
      "employee_id = ? and absence_type_id = ? \
      and absence_date >= ? and absence_date <= ? and approved=1", id, absence_type_id, start_date, end_date
    ).sum(:duration_minutes)

    if string
      return ChronicDuration.output(minutes_used * 60,
                                    limit_to_hours: true,
                                    format: :long) || '0 hours'
    else
      return minutes_used
    end
  end

  def send_email_reminders
    begin
      api_service = GoogleApiService.new
      events = api_service.get_gcal_events(self.email, Time.now - 100.days, Time.now + 8.days, nil)
      events.each do |e|
        next unless e.description&.match(/\[PLANNER ID: [\w]*?\]/).present? && !e.summary&.include?("[FINISHED]")

        if e.start.date_time.to_date == Time.now.to_date + 7.days
          timeframe = 'DUE IN 7 DAYS'
        elsif e.start.date_time.to_date == Time.now.to_date
          timeframe = 'DUE TODAY'
        elsif e.start.date_time.to_date <= Time.now.to_date - 1.day
          next if Time.now.saturday? || Time.now.sunday?
          timeframe = 'OVERDUE'
        else
          next
        end

        PlannerEmailRemindersMailer.send_email(self, e, timeframe).deliver_now
      end
    rescue Google::Apis::ClientError => e
      puts e
    end
  end
end
