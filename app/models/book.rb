# This is an abstract class to facilitate sharing methods and data between the New Books, DVDs, and CLF Books models

class Book < ApplicationRecord
  self.abstract_class = true

  IMAGE_NOT_AVAILABLE_URL   = 'images/image-not-available.png'.freeze

  LIBRARY_CATALOG_BASE_URL  = 'https://avalon.searchmobius.org'.freeze
  LIBRARY_CATALOG_CLUSTER   = 'S2'.freeze
  LOCATION_CODE_LIKE = 'ck%'.freeze

  SYNDETICS_BASE_URL        = 'http://www.syndetics.com'.freeze
  SYNDETICS_CLIENT_ID       = 'univcentmo'.freeze
  GOOGLE_BOOKS_BASE_URL_1     = /^http:\/\/bks[0-9]\.books\.google\.com/
  GOOGLE_BOOKS_BASE_URL_2     = /^http:\/\/books\.google\.com/
  GOOGLE_BOOKS_API_BASE_URL = 'https://www.googleapis.com/books/v1/volumes?q=isbn:'
  GOOGLE_API_KEY = Rails.application.secrets.google_api_key

  RSS_CHANNEL_TITLE = 'New Arrivals'.freeze
  RSS_CHANNEL_AUTHOR = 'James C. Kirkpatrick Library'.freeze
  RSS_CHANNEL_ABOUT_URL = 'https://library.ucmo.edu'



  def title=(val)
    write_attribute(:title, val.gsub('[electronic resource]', '').squish)
  end

  def author=(val)
    write_attribute(:author, val.gsub('by ', '').squish)
  end

  def call_number=(val)
    write_attribute(:call_number, val.squish)
  end

  def quest_url
    LIBRARY_CATALOG_BASE_URL + "/record=#{self.record_number&.to_s}~#{LIBRARY_CATALOG_CLUSTER}"
  end

  def plain_call_number
    call_number&.sub('je', '').sub('ref', '').sub('curr', '').sub('big books', '')
  end

  def call_number_without_marc_tags
    call_number&.gsub(/\|[a-zA-Z]/i, '')
  end

  # get the absolute url of a cover image or a filler image
  def full_image_url(force_https = false)
    # if there is no URL, use the local "image not available" image
    url = ''
    if image_url.blank?
      url = ActionController::Base.helpers.asset_path('image-not-available.png', host: "//#{Rails.application.config.host}")

    # if the image URL is from Syndetics and this is the production/staging server, replace Syndetics with the URL of the local reverse proxy
    elsif image_url.start_with?(SYNDETICS_BASE_URL) && !Rails.env.development? && !Rails.env.test?
      url = image_url.sub(SYNDETICS_BASE_URL, "https://#{Rails.application.config.host}/syndetics")

    # if the image URL is from Google Books and this is the production server, replace Google Books with the URL of the local reverse proxy
    elsif image_url =~ GOOGLE_BOOKS_BASE_URL_1
      google_books_subdomain_number = image_url.sub('http://bks', '').sub(/\.books\.google\.com\/.*/, '')
      url = image_url.sub(GOOGLE_BOOKS_BASE_URL_1, "//#{Rails.application.config.host}/google-books#{google_books_subdomain_number}")
    elsif image_url =~ GOOGLE_BOOKS_BASE_URL_2
      url = image_url

    # otherwise, return the regular image URL (Syndetics in dev environment and Google Books in all environments)
    else
      url = image_url
    end

    if force_https && !Rails.env.development? && !Rails.env.test?
      url.sub!(/^(http:\/\/|\/\/)/, 'https://')
    end
    return url
  end

  # Get metadata from the SQL result record set and mash it up with metadata from Syndetics or Google Books
  def fetch_metadata(isbn)
    metadata = scrape_syndetics(isbn)
    metadata_is_missing = [:image, :summary, :info_page].any? { |key| metadata[key].blank? }
    metadata = query_google_books_api(isbn, metadata) if metadata_is_missing

    self.image_url ||= metadata[:image]
    self.summary ||= metadata[:summary]
    self.info_page_url ||= metadata[:info_page]
    save
  end

  # Screen scrape Syndetics for metadata
  def scrape_syndetics(isn, metadata = {})

    book_url  = SYNDETICS_BASE_URL + '?isbn=' + isn + '/summary.html&client=' + SYNDETICS_CLIENT_ID
    av_url    = SYNDETICS_BASE_URL + '?isbn=' + isn + '/avsummary.html&client=' + SYNDETICS_CLIENT_ID

    # Open the book and AV summary pages.
    require 'resolv-replace'
    begin
      book_page = Nokogiri::HTML(HTTParty.get(book_url))
      av_page = Nokogiri::HTML(HTTParty.get(av_url))
    rescue Net::OpenTimeout
      return {}
    end

    # Search the book and AV pages for DOM elements.
    if metadata[:title].blank?
      if book_page.at_css('.header_title').present?
        metadata[:info_page] = book_url
      elsif book_page.at_css('.header_title_r1').present?
        metadata[:info_page] = book_url
      elsif av_page.at_css('.header_title').present?
        metadata[:info_page] = av_url
      elsif av_page.at_css('.header_title_r1').present?
        metadata[:info_page] = book_url
      end
      end

    if metadata[:summary].blank?
      if book_page.at_css('.element_body').present?
        book_page.at_css('.element_body').children.each do |child|
          metadata[:summary] = child.text.strip if child.text?
        end
      elsif av_page.at_css('.element_body').present?
        av_page.at_css('.element_body').children.each do |child|
          metadata[:summary] = child.text.strip if child.text?
        end
      end
    end

    if metadata[:author].blank?
      if book_page.at_css('.author_heading').present?
        metadata[:author] = book_page.at_css('.author_heading').text.strip
      elsif av_page.at_css('.author_heading').present?
        metadata[:author] = av_page.at_css('.author_heading').text.strip
      end
    end

    if metadata[:image].blank?
      image_url = nil
      if book_page.at_css('.book_cover img').present?
        image_url = SYNDETICS_BASE_URL + '/' + book_page.at_css('.book_cover img')[:src]
      elsif av_page.at_css('.book_cover img').present?
        image_url = SYNDETICS_BASE_URL + '/' + av_page.at_css('.book_cover img')[:src]
      end

      if image_url
        image_dimensions = FastImage.size(image_url)
        if image_dimensions.present?
          if image_dimensions[0] > 1 && image_dimensions[1] > 1
            metadata[:image] = image_url
          end
        end
      end
    end

    metadata
  end

  # Query Google Books API for metadata
  def query_google_books_api(isn, metadata = {})

    # Fetch data from Google Books API
    google_books_url = GOOGLE_BOOKS_API_BASE_URL + isn + '&key=' + GOOGLE_API_KEY
    json_data = HTTParty.get(google_books_url).body
    data = JSON.parse(json_data)

    # Loop through the resulting data and try to fill our missing metadata
    if data['items'].present?
      data['items'].each do |item|
        if metadata[:info_page].blank? && item.key?('volumeInfo')
          metadata[:info_page] = item['volumeInfo']['infoLink']
        end

        if metadata[:author].blank? && item.key?('volumeInfo')
          if item['volumeInfo'].key?('authors')
            metadata[:author] = item['volumeInfo']['authors'].join(', ')
          end
        end

        if metadata[:summary].blank? && item.key?('volumeInfo')
          if item['volumeInfo'].key?('description')
            metadata[:summary] = item['volumeInfo']['description']
          end
        end

        next unless metadata[:image].blank? && item.key?('volumeInfo')
        next unless item['volumeInfo'].key?('imageLinks')
        if item['volumeInfo']['imageLinks'].key?('thumbnail')
          metadata[:image] = item['volumeInfo']['imageLinks']['thumbnail']
        end
      end
    end

    metadata
  end






  def self.stripped_call_number(call_number)
    stripped_call_number = call_number
      &.gsub(/^\|\w/i, '')&.gsub(/\|\w/i, ' ')
      &.gsub(/^JE/i, '')
      &.gsub(/^Ref/i, '')
      &.gsub(/^Big Books/i, '')
      &.gsub(/^Curr/i, '')
      &.gsub('.', ' ')
      &.strip&.squeeze(' ')
    if stripped_call_number&.match(/[0-9]{5,}/)
      long_number = stripped_call_number&.match(/[0-9]{5,}/).to_s
      decimalized_number = long_number.insert(4, '.')
      stripped_call_number.gsub!(long_number, decimalized_number)
    end
    if stripped_call_number&.match(/^[a-zA-Z]{1,}[0-9]/i)
      s = stripped_call_number&.match(/^[a-zA-Z]{1,}/i).to_s
      stripped_call_number.sub!(s, s + ' ')
    end

    return stripped_call_number
  end

  def self.normalized_call_number(call_number)
    normalized_call_number = Lcsort.normalize( stripped_call_number(call_number)  || '')
    if normalized_call_number&.match(/^[a-zA-Z]{1,}0/i)
      normalized_call_number.sub!(/0{1,}/, '.')
    end
    if call_number&.match(/^(\|f)?JE/i)
      normalized_call_number = stripped_call_number(call_number)&.gsub(/^(\|f)?JE/i,'')&.strip&.gsub(' ','.')
    end
    return normalized_call_number
  end

  def self.call_number_class(call_number)
    cn_class = normalized_call_number(call_number)&.gsub('.', ' ')&.split(' ')&.first
    if call_number&.match(/^(\|f)?JE/i)
      if cn_class&.match(/[0-9]{3}$/i) && !cn_class&.match(/[a-zA-Z]/i)
        cn_class.gsub!(/[0-9]{2}$/i, '00')
      end
      cn_class = 'JE ' + (cn_class || '')
    end
    return cn_class
  end

  def self.call_number_class_number(call_number)
    cn_classnum = normalized_call_number(call_number)&.gsub(/\.0{1,}/, '.')&.gsub('.', ' ')&.split(' ')
      &.select{|x| x.present?}
      &.first(2)&.join(' ')
      &.gsub(/[a-zA-Z]{1,}$/i, '')

    if call_number&.match(/^(\|f)?JE/i)
      if cn_classnum.match(/^[0-9]{3}/i) && !cn_classnum.match(/[a-zA-Z]/i)
        cn_classnum.gsub!(/\s\w{1,}$/i, '')
      end
      cn_classnum = 'JE ' + cn_classnum
    end
    return cn_classnum
  end



end
