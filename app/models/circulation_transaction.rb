class CirculationTransaction < ApplicationRecord

  GOOGLE_SHEET_ID = '1b9duicNAKl1IWFlogCDeUjG_jI0kH1U1P7JJEFKgJiA'

  def self.write_to_google_sheet
    sql = 'select transaction_gmt, op_code, best_title_norm, call_number, item_location_code, itype_code, itype_name, ptype_name, patron_home_library_code '
    sql += 'from circulation_transactions '
    sql += 'order by transaction_gmt desc'

    data = ActiveRecord::Base.connection.execute(sql)

    table = [['Transaction Time', 'Transaction Type', 'Title', 'Call Number', 'Clean Call Number', 'Sortable Call Number', 'Call Number Class', 'Call Number Class Number', 'Location Code', 'Item Type', 'Patron Type', 'Patron Home Library Code']]
    table += data.to_a

    # Go through each row, split call number into parts, and replace opcode letter with descriptive string
    table.each_with_index do |row, i|
      next if i == 0
      stripped_call_number = Book.stripped_call_number(row[3])
      sortable_call_number = Book.normalized_call_number(row[3])
      call_number_class = Book.call_number_class(row[3])
      call_number_class_number = Book.call_number_class_number(row[3])

      if [9,16,18,19,20,21,24,52,53,57,62,64,68].include?(row[9])
        row.insert(4, nil, nil, nil, nil)
      else
        row.insert(4, stripped_call_number, sortable_call_number, call_number_class, call_number_class_number)
      end

      row.delete_at(9) # Delete ITYPE code

      if row[1] == 'o'
        row[1] = 'checkout'
      elsif row[1] == 'r'
        row[1] = 'renewal'
      elsif row[1] == 'u'
        row[1] = 'use count'
      end
    end

    s = GoogleApiService.new
    s.clear_google_sheet(GOOGLE_SHEET_ID)
    s.append_to_google_sheet(GOOGLE_SHEET_ID, table)

    return table
  end

  #Get circulation data from Sierra and save it as CirculationTransaction objects
  def self.fetch_transactions
    #Connect to the Sierra database
    sierra_connection = SierraConnection.new

    #Construct the query
    query = "SELECT c.id as transaction_id, transaction_gmt, c.application_name, c.source_code, c.op_code,
      c.item_record_id, c.bib_record_id, c.stat_group_code_num,
      brp.best_title_norm, brp.best_author_norm,
      c.item_location_code, irp.call_number, c.itype_code_num, itp.name as itype_name,
      p.ptype_code, ptp.name as ptype_name, p.checkout_total as patron_checkout_total,
      c.patron_home_library_code


      FROM sierra_view.circ_trans c

      left join sierra_view.bib_record_property brp on c.bib_record_id=brp.bib_record_id
      left join sierra_view.material_property_category_myuser mp on brp.material_code=mp.code

      left join sierra_view.item_record_property irp on c.item_record_id=irp.item_record_id
      left join sierra_view.itype_property_myuser itp on c.itype_code_num=itp.code

      left join sierra_view.patron_view p on c.patron_record_id=p.id
      left join sierra_view.ptype_property_myuser ptp on p.ptype_code=ptp.value

      where c.item_location_code like 'ck%'
      and c.op_code in ('o','r','u')"

    #Execute the query
    results = sierra_connection.select( query )
    sierra_connection.disconnect

    #Save the transaction records as new database objects
    i = 1
    existing_transactions = CirculationTransaction.all
    new_circs = []
    results.each do |record|
      puts "processing #{i} of #{results.ntuples}"
      i += 1
      #First make sure this circ isn't already saved

      if existing_transactions.to_a.select{|t| t.transaction_id == record['transaction_id']}.blank?
        circulation = CirculationTransaction.new
        circulation.transaction_id = record['transaction_id']
        circulation.transaction_gmt = record['transaction_gmt']
        circulation.application_name = record['application_name']
        circulation.source_code = record['source_code']
        circulation.op_code = record['op_code']
        circulation.item_record_id = record['item_record_id']
        circulation.bib_record_id = record['bib_record_id']
        circulation.stat_group_code_num = record['stat_group_code_num']
        circulation.best_title_norm = record['best_title_norm']
        circulation.best_author_norm = record['best_author']
        circulation.item_location_code = record['item_location_code']
        circulation.call_number = record['call_number']
        circulation.itype_code = record['itype_code_num']
        circulation.itype_name = record['itype_name']
        circulation.ptype_code = record['ptype_code']
        circulation.ptype_name = record['ptype_name']
        circulation.patron_home_library_code = record['patron_home_library_code']
        circulation.patron_checkout_total = record['patron_checkout_total']

        new_circs << circulation

      end


    end

    CirculationTransaction.import( new_circs )

  end

  # Look up un-normalized call numbers in Sierra SQL and save them to the DB over the normalized ones
  def self.fix_call_numbers

    sierra_connection = SierraConnection.new
    item_ids = CirculationTransaction.all.pluck(:item_record_id).uniq
    sql = "select i.item_record_id, i.call_number from sierra_view.item_record_property i where i.item_record_id in (#{item_ids.join(', ')})"
    results = sierra_connection.select( sql )
    sierra_connection.disconnect

    transactions = CirculationTransaction.where("call_number not like '%|a%'" ).to_a
    results.each_with_index do |row, i|
      puts "#{i} of #{results.values.length}"
      transactions.select{|t| t.item_record_id == row['item_record_id'].to_i}.each do |t|
        t.call_number = row['call_number']
        t.save
      end
    end
  end

end
