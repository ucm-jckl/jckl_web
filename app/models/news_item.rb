class NewsItem < ApplicationRecord
  has_attached_file :news_image, styles: { medium: '300x300>', thumb: '100x100>' }

  validates :title, :article, presence: true
  validates_with AttachmentPresenceValidator, attributes: :news_image
  validates_attachment_content_type :news_image, content_type: /\Aimage/

  before_save :set_friendly_url

  def set_friendly_url
    self.friendly_url = "#{(created_at || Time.now).strftime('%Y-%m-%d')}-#{URI.escape(title.tr(' ', '_').gsub(/[,.?!\/\\()\[\]]/, '')).downcase}"
  end

  def self.current_news
    NewsItem.where('expiration_date is null or expiration_date > ?', Time.zone.now).order('updated_at desc')
  end
end
