class Hour < ApplicationRecord
  def self.populate
    start_time = Time.now - 10.years
    end_time = Time.now + 5.years
    time_marker = start_time
    while time_marker <= end_time
      if Hour.find_by( hour: time_marker.beginning_of_hour ).blank?
        h = Hour.new
        h.hour = time_marker.beginning_of_hour
        h.save
      end
      time_marker += 1.hour
    end
  end
end
