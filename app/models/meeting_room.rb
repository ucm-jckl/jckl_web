class MeetingRoom < ApplicationRecord
  has_many :meeting_room_bookings, dependent: :nullify

  has_attached_file :photo,
                    styles: {
                      medium: '400x400'
                    }
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, attributes: :photo, less_than: 10.megabytes

  GOOGLE_CALENDAR_EMBED_BASE_URL = 'https://www.google.com/calendar/embed?showCurrentTime=0showSubscribeButton=0&showTz=0&showPrint=0&showTabs=0&showCalendars=0&wkst=1&bgcolor=%23e2e2e2&ctz=America%2FChicago'.freeze
  GOOGLE_CALENDAR_PAGE_BASE_URL = 'http://www.google.com/calendar/embed?mode=WEEK&ctz=America/Chicago'.freeze
  GOOGLE_CALENDAR_AVAILABILITY_BASE_URL = "https://www.googleapis.com/calendar/v3/freeBusy".freeze

  def google_calendar_week_embed
    "#{GOOGLE_CALENDAR_EMBED_BASE_URL}&src=#{google_calendar_id}&mode=WEEK"
  end

  def google_calendar_week_page
    "#{GOOGLE_CALENDAR_PAGE_BASE_URL}&src=#{google_calendar_id}&mode=WEEK"
  end

  def medium_photo_url
    self.photo&.url(:medium)
  end

  def bookings(start_time, end_time)
    api_service = GoogleApiService.new
    bookings = api_service.get_gcal_events(self.google_calendar_id, start_time, end_time)
  end

  def available?(start_time, end_time)
    bookings = self.bookings(start_time - 1.day, end_time + 1.day)
    concurrent_bookings = bookings.select do |b|
      (start_time <= b.start.date_time && end_time > b.start.date_time && end_time <= b.end.date_time) ||
      (start_time >= b.start.date_time && end_time <= b.end.date_time) ||
      (start_time >= b.start.date_time && start_time < b.end.date_time && end_time >= b.end.date_time)
    end
    concurrent_bookings.blank?
  end

end
