class ComputerSession < ApplicationRecord
  belongs_to :computer

  SESSION_TIMEOUT = 3.minutes
  GOOGLE_SHEET_ID = '1zx-TQyg61qsRl-e5Ao_nKvm_Y-l7pX2MZUZ_TfCp3DA'

  def self.write_to_google_sheet

  end


  def current?
    Time.zone.now - updated_at <= SESSION_TIMEOUT
  end


  def minutes_between(start_time, end_time)
    minutes = 0
    if self.created_at < start_time && self.updated_at > end_time
      minutes += (end_time - start_time) / 60
    elsif self.created_at < start_time && self.updated_at > start_time && self.updated_at < end_time
      minutes += (self.updated_at - start_time) / 60
    elsif self.created_at > start_time && self.updated_at < end_time
      minutes += ( self.updated_at - self.created_at ) / 60
    elsif self.created_at > start_time && self.created_at < end_time && self.updated_at > end_time
      minutes += ( end_time - self.created_at ) / 60
    end

    return minutes
  end


  def existed_between?(start_time, end_time)
    minutes = self.minutes_between(start_time, end_time)
    return !minutes.zero?
  end



end
