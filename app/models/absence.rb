class Absence < ApplicationRecord

  SICK_HOURS_BEFORE_DOCTOR_NOTE = 8 * 8
  FAMILY_HOURS_BEFORE_DOCTOR_NOTE = 12 * 8
  PERSONAL_HOURS_PER_YEAR = 8

  belongs_to :employee
  belongs_to :submitter, foreign_key: 'submitter_id', class_name: 'Employee'
  belongs_to :editor, foreign_key: 'editor_id', class_name: 'Employee', optional: true
  belongs_to :approver, foreign_key: 'approver_id', class_name: 'Employee', optional: true
  belongs_to :absence_type
  has_many :absence_dates, inverse_of: :absence, dependent: :destroy
  accepts_nested_attributes_for :absence_dates, reject_if: :all_blank, allow_destroy: true

  attr_accessor :absence_dates_list, :is_new, :is_edited, :is_deleted, :is_approved, :is_rejected, :is_note_received, :times_have_been_altered, :was_previously_approved, :was_previously_rejected

  validates :absence_type,  presence: true
  validates :background_information, presence: {message: 'An explanation is required when editing an absence.'}, if: :is_edited

  before_save :determine_if_note_needed, if: proc { |a| !Rails.env.test? }
  after_update :update_absence_dates, if: proc { |a| !Rails.env.test? }

  def absence_duration
    self.absence_dates.each{|d| d.set_absence_duration if d.duration_minutes.nil?}
    self.absence_dates&.pluck(:duration_minutes)&.reduce(:+) || 0
  end

  def absence_duration_string
    "#{(self.absence_duration || 0) / 60} hours, #{(self.absence_duration || 0) % 60} minutes"
  end

  def original_absence_duration
    self.absence_dates.map do |d|
      if d.original_data&.dig('end_time').present? && d.original_data&.dig('start_time').present?
        d.original_data&.dig('duration_minutes')
      else
        0
      end
    end.reduce(:+) || 0
  end

  def original_absence_duration_string
    "#{((self.original_absence_duration || 0) / 60).to_i} hours, #{((self.original_absence_duration || 0) % 60).to_i} minutes"
  end

  def first_date
    self.absence_dates.order(:absence_date).first
  end

  def last_date
    self.absence_dates.order(:absence_date).last
  end

  def date_range
    "#{self.first_date&.absence_date&.strftime("%Y-%m-%d")} to #{self.last_date&.absence_date&.strftime("%Y-%m-%d")}"
  end

  def has_absence_dates_between?(start_date, end_date)
    self.absence_dates.to_a.select{|d| d.absence_date >= start_date && d.absence_date <= end_date}.present?
  end

  def is_over_personal_leave_limit?
    if self.absence_type.absence_type_name == 'personal'
      previous_personal_leave_minutes = self.employee.leave_time_used_between(Chronic.parse('last july 1').to_date, Chronic.parse('last july 1').to_date + 1.year, 'personal', false)
      new_personal_leave_minutes = self.absence_duration

      if ( previous_personal_leave_minutes + new_personal_leave_minutes ) / 60 > PERSONAL_HOURS_PER_YEAR
        return true
      else
        return false
      end
    else
      return false
    end
  end

  def determine_if_note_needed
    past_minutes_used = employee&.leave_time_used_between(Chronic.parse('last july 1'), Time.now, absence_type.absence_type_name, false).to_i || 0
    new_minutes_used = self.absence_duration
    total_minutes_used = past_minutes_used + new_minutes_used

    if absence_type.absence_type_name == 'sickself' \
      && total_minutes_used > SICK_HOURS_BEFORE_DOCTOR_NOTE * 60

      self.note_required = true

    elsif absence_type.absence_type_name == 'sickfamily' \
      && total_minutes_used > FAMILY_HOURS_BEFORE_DOCTOR_NOTE * 60

      self.note_required = true

    else
      self.note_required = false
    end

  end


  def update_absence_dates
    if self.is_approved || self.is_rejected
      self.absence_dates.each{|d| d.update_google_calendar(self)}
    end
  end


end
