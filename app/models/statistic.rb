# This is an abstract class to facilitate sharing methods and data between the various statistics models

class Statistic < ApplicationRecord
  self.abstract_class = true

  def self.seconds_used_during_hour(range_start, range_end, time)
    if time.beginning_of_hour > range_end # Range is fully after the hour
      return 0
    elsif time.end_of_hour < range_start # Range is fully before the hour
      return 0
    elsif range_start <= time.beginning_of_hour && range_end >= time.end_of_hour # Range encompasses the whole hour
      return 60 * 60
    elsif range_start >= time.beginning_of_hour && range_end <= time.end_of_hour # Range is fully within the hour but less than a full hour
      return range_end - range_start
    elsif range_start <= time.beginning_of_hour && range_end <= time.end_of_hour # Range starts before the hour starts and ends before the hour ends
      return range_end - time.beginning_of_hour
    elsif range_start >= time.beginning_of_hour && range_end >= time.end_of_hour # Range starts after the hour starts and ends after the hour ends
      return time.end_of_hour - range_start
    else
      return nil
    end
  end


  def self.date_format(time_interval)
    case time_interval
    when 'hours'
      format = '%Y-%m-%d (%a) %H:00'
    when 'days'
      format = '%Y-%m-%d (%a)'
    when 'weeks'
      format = '%Y, Week %U'
    when 'months'
      format = '%Y-%m'
    when 'years'
      format = '%Y'
    when 'hour_averages'
      format = '%H:00'
    when 'weekday_averages'
      format = '%a'
    when 'weekday_hour_averages'
      format = '%a %H:00'
    end
    return format
  end


  def self.x_time_categories(start_time, end_time, interval)

    if interval == 'hours_avg'
      return [
        '00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00',
        '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00',
        '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'
      ]
    elsif interval == 'weekdays_avg'
      return %w(Sun Mon Tue Wed Thu Fri Sat)
    elsif interval == 'weekdays_hours_avg'
      return [
        'Sun 00:00', 'Sun 01:00', 'Sun 02:00', 'Sun 03:00', 'Sun 04:00', 'Sun 05:00',
        'Sun 06:00', 'Sun 07:00', 'Sun 08:00', 'Sun 09:00', 'Sun 10:00', 'Sun 11:00',
        'Sun 12:00', 'Sun 13:00', 'Sun 14:00', 'Sun 15:00', 'Sun 16:00', 'Sun 17:00',
        'Sun 18:00', 'Sun 19:00', 'Sun 20:00', 'Sun 21:00', 'Sun 22:00', 'Sun 23:00',

        'Mon 00:00', 'Mon 01:00', 'Mon 02:00', 'Mon 03:00', 'Mon 04:00', 'Mon 05:00',
        'Mon 06:00', 'Mon 07:00', 'Mon 08:00', 'Mon 09:00', 'Mon 10:00', 'Mon 11:00',
        'Mon 12:00', 'Mon 13:00', 'Mon 14:00', 'Mon 15:00', 'Mon 16:00', 'Mon 17:00',
        'Mon 18:00', 'Mon 19:00', 'Mon 20:00', 'Mon 21:00', 'Mon 22:00', 'Mon 23:00',

        'Tue 00:00', 'Tue 01:00', 'Tue 02:00', 'Tue 03:00', 'Tue 04:00', 'Tue 05:00',
        'Tue 06:00', 'Tue 07:00', 'Tue 08:00', 'Tue 09:00', 'Tue 10:00', 'Tue 11:00',
        'Tue 12:00', 'Tue 13:00', 'Tue 14:00', 'Tue 15:00', 'Tue 16:00', 'Tue 17:00',
        'Tue 18:00', 'Tue 19:00', 'Tue 20:00', 'Tue 21:00', 'Tue 22:00', 'Tue 23:00',

        'Wed 00:00', 'Wed 01:00', 'Wed 02:00', 'Wed 03:00', 'Wed 04:00', 'Wed 05:00',
        'Wed 06:00', 'Wed 07:00', 'Wed 08:00', 'Wed 09:00', 'Wed 10:00', 'Wed 11:00',
        'Wed 12:00', 'Wed 13:00', 'Wed 14:00', 'Wed 15:00', 'Wed 16:00', 'Wed 17:00',
        'Wed 18:00', 'Wed 19:00', 'Wed 20:00', 'Wed 21:00', 'Wed 22:00', 'Wed 23:00',

        'Thu 00:00', 'Thu 01:00', 'Thu 02:00', 'Thu 03:00', 'Thu 04:00', 'Thu 05:00',
        'Thu 06:00', 'Thu 07:00', 'Thu 08:00', 'Thu 09:00', 'Thu 10:00', 'Thu 11:00',
        'Thu 12:00', 'Thu 13:00', 'Thu 14:00', 'Thu 15:00', 'Thu 16:00', 'Thu 17:00',
        'Thu 18:00', 'Thu 19:00', 'Thu 20:00', 'Thu 21:00', 'Thu 22:00', 'Thu 23:00',

        'Fri 00:00', 'Fri 01:00', 'Fri 02:00', 'Fri 03:00', 'Fri 04:00', 'Fri 05:00',
        'Fri 06:00', 'Fri 07:00', 'Fri 08:00', 'Fri 09:00', 'Fri 10:00', 'Fri 11:00',
        'Fri 12:00', 'Fri 13:00', 'Fri 14:00', 'Fri 15:00', 'Fri 16:00', 'Fri 17:00',
        'Fri 18:00', 'Fri 19:00', 'Fri 20:00', 'Fri 21:00', 'Fri 22:00', 'Fri 23:00',

        'Sat 00:00', 'Sat 01:00', 'Sat 02:00', 'Sat 03:00', 'Sat 04:00', 'Sat 05:00',
        'Sat 06:00', 'Sat 07:00', 'Sat 08:00', 'Sat 09:00', 'Sat 10:00', 'Sat 11:00',
        'Sat 12:00', 'Sat 13:00', 'Sat 14:00', 'Sat 15:00', 'Sat 16:00', 'Sat 17:00',
        'Sat 18:00', 'Sat 19:00', 'Sat 20:00', 'Sat 21:00', 'Sat 22:00', 'Sat 23:00'
      ]
    else
      time_marker = start_time
      categories = []
      while time_marker <= end_time

        categories << time_marker.strftime( Statistic.date_format( interval ) )

        if interval == 'hours'
          time_marker += 1.hour
        elsif interval == 'days'
          time_marker += 1.day
        elsif interval == 'weeks'
          time_marker += 1.week
        elsif interval == 'months'
          time_marker += 1.month
        elsif interval == 'years'
          time_marker += 1.year
        end
      end
      categories << time_marker.strftime( Statistic.date_format( interval ) )

      return categories
    end
  end




  def self.year_and_week_to_date(week_string)
    year = week_string.split(',')[0].to_i
    week_number = week_string.split(',')[1].gsub(/[^0-9]/i, '').to_i
    (Chronic.parse("#{year}-01-01").beginning_of_year.beginning_of_week + week_number.weeks).beginning_of_week
  end

end
