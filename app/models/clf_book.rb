class ClfBook < Book
  has_many :clf_orders, through: :clf_book_order_links
  has_many :clf_book_order_links, foreign_key: 'book_id', dependent: :destroy

  def combined_authors
    authors = []
    authors.push( self.primary_author )
    authors.push( self.secondary_author )
    authors.push( self.illustrator.to_s + " (illustrator)" )
    authors.delete_if{ |item| item.blank? }
    return authors.join( ", " )
  end

  #After creating a record, get metadata from Syndetics/Google
  after_create { fetch_metadata( self.isbn ) }

end
