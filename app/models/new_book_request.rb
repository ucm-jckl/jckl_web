class NewBookRequest < ApplicationRecord
  belongs_to :subject_area, optional: true
  belongs_to :fund_code, optional: true

  validates :requestor_name, :requestor_email, :format, :title, :copies_ordered, presence: true
  validate :subject_area_or_fund_code

  private

  def subject_area_or_fund_code
    if subject_area.blank? && fund_code.blank?
      errors.add(:subject_area, " or fund code must be selected.")
      errors.add(:fund_code, " or subject area must be selected.")
    end
  end
end
