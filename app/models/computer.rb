class Computer < HighchartsDataSource
  belongs_to :computer_build, optional: true
  has_many :computer_locations
  has_many :computer_sessions
  has_many :computer_softwares, through: :computer_software_links
  has_many :computer_software_links, dependent: :destroy


  def last_session
    computer_sessions.order(:updated_at).last
  end


  def current_session
    if last_session.present?
      if last_session.current?
        return last_session
      else
        return ComputerSession.new(computer_id: id, ping_count: 0)
      end
    else
      return ComputerSession.new(computer_id: id, ping_count: 0)
    end
  end


  def available?
    current_session.new_record?
  end


  def current_location
    locations = self.computer_locations.to_a.select{|l| l.start_date <= Time.now && (l.end_date && l.end_date >= Time.now || l.end_date.blank?)}
    return locations[0]&.location
  end


  def minutes_used_between(start_time, end_time)
    minutes = 0
    self.computer_sessions.where('created_at <= ? and updated_at >= ?', start_time.beginning_of_day, end_time.end_of_day).each do |s|
      next unless s.created_at.present? && s.updated_at.present?
      if s.created_at < start_time && s.updated_at > end_time
        minutes += (end_time - start_time) / 60
      elsif s.created_at < start_time && s.updated_at > start_time && s.updated_at < end_time
        minutes += (s.updated_at - start_time) / 60
      elsif s.created_at > start_time && s.updated_at < end_time
        minutes += ( s.updated_at - s.created_at ) / 60
      elsif s.created_at > start_time && s.created_at < end_time && s.updated_at > end_time
        minutes += ( end_time - s.created_at ) / 60
      end
    end

    return minutes
  end


  def was_used_between?(start_time, end_time)
    minutes_used = self.minutes_used_between(start_time, end_time)
    return !minutes_used.zero?
  end


  def self.computers_used_per_hour_between(start_time, end_time)
    hours = {}
    time_marker = start_time
    while time_marker <= end_time
      hours[time_marker.beginning_of_hour.to_s] = 0
      time_marker += 1.hour
    end

    sessions = ComputerSession.where('created_at >= ? and updated_at <= ?', start_time.beginning_of_hour, end_time.end_of_hour)
    puts "#{sessions.length} sessions, #{hours.length} hours"

    sessions.each_with_index do |s, i|
      puts "session #{s.id} (#{(i / sessions.length ) * 100}%)"
      hours.each do |time, computers|
        hours[time] += 1 if s.existed_between?( Chronic.parse(time).beginning_of_hour, Chronic.parse(time).end_of_hour )
      end
    end

    # hours.each do |time, computers|
    #   a = b
    #   puts "building hours hash: #{time}"
    #   nearby_sessions = sessions.to_a.select{|s| }
    #   sessions_this_hour = sessions.to_a.select{|s| s.existed_between?( Chronic.parse(time).beginning_of_hour, Chronic.parse(time).end_of_hour ) }
    #   hours[time] = sessions_this_hour.length
    # end

    return hours
  end


  def self.to_highcharts(start_time, end_time, interval, grouping, aggregation)
    sql = nil

    # Get all headcounts grouped by floor
    # NOTE: Any location whose name includes "hcc" is grouped into an HCC "floor" as requested by
    # by Horne-Popp and Tessone on 2016-02-04
    if grouping == 'floor'
      sql = "select if(l.location_name like '%hcc%','HCC',CONCAT('Floor ', l.floor)) as series, DATE_FORMAT(CONVERT_TZ(cs.created_at, \'+0:00\', \'US/Central\'), '#{date_format(interval)}') as time_period, sum(timestampdiff(minute, created_at, updated_at)) as count from computer_sessions cs "

    # OR Get all headcounts grouped by location
    elsif grouping == 'location'
      sql = "select CONCAT('Floor ', l.floor, ': ', l.display_name) as series, DATE_FORMAT(CONVERT_TZ(cs.created_at, \'+0:00\', \'US/Central\'), '#{date_format(interval)}') as time_period, sum(timestampdiff(minute, created_at, updated_at)) as count from computer_sessions cs "

    # OR Get all headcounts for the whole building
    else
      sql = "select 'JCKL' as series, DATE_FORMAT(CONVERT_TZ(cs.created_at, \'+0:00\', \'US/Central\'), '#{date_format(interval)}') as time_period, sum(timestampdiff(minute, created_at, updated_at)) as count from computer_sessions cs "
    end


    sql += 'left join computers c on cs.computer_id = c.id '
    sql += 'left join computer_locations cl on c.id = cl.computer_id '
    sql += 'left join locations l on cl.location_id = l.id '
    sql += 'where (cs.created_at > cl.start_date and (cs.updated_at < cl.end_date or cl.end_date is null)) '
    sql += "and (cs.created_at >= '#{start_time}' and cs.updated_at <= '#{end_time}') "
    sql += 'group by series, time_period '
    sql += 'order by series, time_period asc'

    data = ActiveRecord::Base.connection.execute(sql)
    transform_to_highcharts_series(start_time, end_time, interval, data, aggregation)
  end
end
