class LibraryHour < ApplicationRecord
  # Set the # of hours after 12 AM that a new date for our operating hours starts
  DATE_CUTOVER_INTERVAL = 6.hours

  # Set LibCal API URLs
  LIBCAL_TODAY_URL = 'https://api3.libcal.com/api_hours_today.php?iid=3245&lid=1921&format=json'.freeze
  LIBCAL_WEEK_URL = 'https://api3.libcal.com/api_hours_grid.php?iid=3245&format=json&weeks=52'.freeze

  # Determine whether a given time is part of the previous day's overnight hours
  def self.is_overnight(time)
    time < (time.beginning_of_day + DATE_CUTOVER_INTERVAL)
  end

  # Get the date record for today
  def self.hours_today
    if is_overnight(Time.zone.now)
      return LibraryHour.find_by(calendar_date: Time.zone.now.to_date - 1.day)
    else
      return LibraryHour.find_by(calendar_date: Time.zone.now.to_date)
    end
  end

  # Get the number of weeks a given calendar date is from now
  def weeks_from_now
    this_week_start = Time.zone.now.beginning_of_week
    if self.class.is_overnight(Time.zone.now)
      this_week_start = (Time.zone.now.to_date - 1.day).beginning_of_week
    end

    ((calendar_date.to_time - this_week_start) / (60 * 60 * 24 * 7)).floor
  end

  # Get hours from LibCal
  def self.fetch_hours
    raw_data = Net::HTTP.get_response(URI.parse(self::LIBCAL_WEEK_URL)).body
    json_data = JSON.parse(raw_data)

    json_data['locations'][0]['weeks'].each do |week|
      week.each do |weekday, date_data|
        hours_text = date_data['rendered']

        if date_data.dig('times', 'hours', 0, 'from') && date_data.dig('times', 'hours', 0, 'to')
          open_time = Chronic.parse(date_data['date'] + ' ' + date_data.dig('times', 'hours', 0, 'from'))
          close_time = Chronic.parse(date_data['date'] + ' ' + date_data.dig('times', 'hours', 0, 'to'))
        else
          open_time = nil
          close_time = nil
        end

        if close_time && open_time
          close_time += 1.day if close_time < open_time
        end

        hour = LibraryHour.find_by(calendar_date: Chronic.parse(date_data['date']).to_date)
        hour ||= LibraryHour.new
        hour.calendar_date = Chronic.parse(date_data['date']).to_date
        hour.open_time = open_time
        hour.close_time = close_time
        hour.hours_display_text = hours_text
        hour.save
      end
    end

  end



  # get raw weekly hours from LibCal
  def self.hours_weekly
    Rails.cache.fetch('hours_weekly', expires_in: 6.hours) do
      json_data = Net::HTTP.get_response(URI.parse(self::LIBCAL_WEEK_URL)).body
      data = JSON.parse(json_data)
      return data['locations'][0]['weeks']
    end
  end

  # get the number of minutes the library is open over a given time period
  def self.minutes_open_between(start_time, end_time, strftime_pattern=nil, strftime_match=nil)
    total_minutes = 0

    dates = []
    date_marker = start_time.to_date
    while date_marker <= end_time.to_date
      dates << date_marker
      date_marker += 1.day
    end

    hours_records = LibraryHour.where(calendar_date: dates)
    hours_records.each do |lh|
      next unless lh.open_time.present? && lh.close_time.present?
      minutes_on_date = (lh.close_time - lh.open_time) / 60.0
      if strftime_pattern.present?
        total_minutes += minutes_on_date if lh.open_time.strftime(strftime_pattern) == strftime_match
      else
        total_minutes += minutes_on_date
      end
    end

    return total_minutes
  end


  def self.minutes_open_on_date(date)
    return self.minutes_open_between(date.beginning_of_day, date.end_of_day)
  end

  def self.minutes_open_on_week(date)
    return self.minutes_open_between(date.beginning_of_week, date.end_of_week)
  end

  def self.minutes_open_on_month(date)
    return self.minutes_open_between(date.beginning_of_month, date.end_of_month)
  end

  def self.minutes_open_on_year(date)
    return self.minutes_open_between(date.beginning_of_year, date.end_of_year)
  end

end
