class AbsenceDate < ApplicationRecord
  DETAILED_GOOGLE_CALENDAR_ID = 'ucmo.edu_g3v7r53k657u2ajn6d2v2j0j94@group.calendar.google.com'.freeze
  CONDENSED_GOOGLE_CALENDAR_ID = 'ucmo.edu_l4upveu3lkkomokr2strtf9fc8@group.calendar.google.com'

  belongs_to :absence

  attr_accessor :leave_hours_used, :leave_minutes_used, :original_data

  validates :absence_date, presence: true
  validate :leave_time_must_be_set
  validate :start_time_must_be_before_end_time
  validate :times_or_all_day_required


  def leave_time_must_be_set
    if leave_hours_used.blank? && leave_minutes_used.blank?
      errors.add(:leave_hours_used, 'Either hours used or minutes used must be specified.')
    end
  end


  def start_time_must_be_before_end_time
    if start_time.present? && end_time.present?
      if start_time >= end_time
        errors.add(:end_time, 'End time must be later than start time.')
        errors.add(:start_time, 'Start time must be earlier than end time.')
      end
    end
  end


  def times_or_all_day_required
    unless (start_time.present? && end_time.present?) || ( is_all_day == true || is_all_day == 1)
      errors[:base] << 'Either times or "Out All Day" must be specified for each date absent.'
    end
  end


  before_save :set_absence_duration
  after_save :set_leave_hours_and_minutes
  after_find :set_leave_hours_and_minutes

  before_destroy :delete_from_google_calendar, if: proc { |a| !Rails.env.test? }
  before_create :add_to_google_calendar, if: proc { |a| !Rails.env.test? }
  before_update :update_google_calendar, if: proc { |a| !Rails.env.test? }


  def set_absence_duration
    self.duration_minutes = (leave_hours_used.to_i * 60) + leave_minutes_used.to_i
  end

  def set_leave_hours_and_minutes
    self.leave_hours_used = ((duration_minutes || 0) / 60) || 0
    self.leave_minutes_used = ((duration_minutes || 0) % 60) || 0
  end


  def add_to_google_calendar
    return if Rails.env.development?

    s = GoogleApiService.new

    # Add the detailed calendar event
    if self.is_all_day == true || self.is_all_day == 1
      detailed_event = s.add_gcal_event(DETAILED_GOOGLE_CALENDAR_ID, "#{self.absence.employee.last_first} Out #{if self.absence.approved.nil? then '(pending approval)' end}", self.absence_date, nil, nil)
    else
      detailed_event = s.add_gcal_event(DETAILED_GOOGLE_CALENDAR_ID, "#{self.absence.employee.last_first} Out #{if self.absence.approved.nil? then '(pending approval)' end}", self.absence_date, self.start_time, self.end_time)
    end
    self.google_calendar_event_id = detailed_event.id

    # Edit the condensed calendar
    self.update_condensed_calendar(self.absence, false)
  end


  def update_google_calendar(absence = self.absence)
    return if Rails.env.development?

    s = GoogleApiService.new

    # Edit the detailed calendar event
    if self.is_all_day == true || self.is_all_day == 1
      event = s.edit_gcal_event(DETAILED_GOOGLE_CALENDAR_ID, self.google_calendar_event_id, "#{absence.employee.last_first} Out #{if absence.approved.nil? then '(pending approval)' end}", nil, absence_date, nil, nil, if absence.is_rejected then true else false end)
    else
      event = s.edit_gcal_event(DETAILED_GOOGLE_CALENDAR_ID, self.google_calendar_event_id, "#{absence.employee.last_first} Out #{if absence.approved.nil? then '(pending approval)' end}", nil, absence_date, self.start_time, self.end_time, if absence.is_rejected then true else false end)
    end
    # Edit the condensed calendar
    self.update_condensed_calendar(absence, false)
  end


  def delete_from_google_calendar
    return if Rails.env.development?

    s = GoogleApiService.new

    # Delete the detailed calendar event
    begin
      s.delete_gcal_event(DETAILED_GOOGLE_CALENDAR_ID, google_calendar_event_id)
    rescue Google::Apis::ClientError => e
      if !e.message.downcase.include?('has been deleted') || !e.message.downcase.include?('Not Found') || !e.message.downcase.include?('Resource has been deleted') # Skip errors due to event already having been deleted
        # do nothing
      else
        raise e
      end
    end

    # Edit the condensed calendar
    self.update_condensed_calendar( self.absence, true)
  end


  def update_condensed_calendar(absence = self.absence, destroy = false)
    return if Rails.env.development?

    s = GoogleApiService.new

    # Get or create a new condensed event for the date
    condensed_event = s.get_gcal_events(CONDENSED_GOOGLE_CALENDAR_ID, self.absence_date.beginning_of_day, self.absence_date.end_of_day)[0]
    if condensed_event.blank?
      condensed_event = s.add_gcal_event(CONDENSED_GOOGLE_CALENDAR_ID, "Absences", self.absence_date)
    end

    # Remove this employee's previous absences from the description, add a new one, and sort them all by last name
    absences = condensed_event.description&.split("\n") || []
    absences.delete_if{|a| a.include?(absence.employee.last_first)}
    unless absence.is_rejected || self.destroyed? || destroy
      if self.is_all_day == true || self.is_all_day == 1
        absences << "#{absence.employee.last_first}: out all day #{if absence.approved.nil? then '(pending approval)' end}"
      else
        absences << "#{absence.employee.last_first}: out #{self.start_time.strftime("%l:%M %p")} - #{self.end_time.strftime("%l:%M %p")} #{if absence.approved.nil? then '(pending approval)' end}"
      end
    end
    absences.sort!
    event = s.edit_gcal_event(CONDENSED_GOOGLE_CALENDAR_ID, condensed_event.id, "Absences", absences.join("\n"), self.absence_date)
  end
end
