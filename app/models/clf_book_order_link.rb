class ClfBookOrderLink < ApplicationRecord
  belongs_to :clf_book, foreign_key: 'book_id'
  belongs_to :clf_order, foreign_key: 'order_id'
end
