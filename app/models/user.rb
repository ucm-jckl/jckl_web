class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :omniauthable,
         :rememberable, :trackable, :validatable,
         omniauth_providers: [:google_oauth2]

  has_many :user_roles, foreign_key: 'user_id', dependent: :destroy
  belongs_to :employee, optional: true

  def admin?
    check_role = UserRole.find_by(user_id: id, name: UserRole::ROLES[:admin])
    check_role.present?
  end

  def employee?
    employee.present?
  end

  def registered?
    !new_record?
  end

  def has_role?(role)
    check_role = UserRole.find_by(user_id: id, name: role)
    check_role.present? || admin?
  end

  def username
    if employee?
      employee.first_last
    else
      email
    end
  end

  # Find all users with a specific role
  def self.find_by_role(role)
    User.joins(:user_roles).where(user_roles: { name: UserRole::ROLES[role] })
  end

  # Set up Google authentication
  def self.from_omniauth(access_token, _signed_in_resource = nil)
    data = access_token.info
    user = User.where(provider: access_token.provider, uid: access_token.uid).first
    if user
      return user
    else
      registered_user = User.where(email: access_token.info.email).first
      if registered_user
        return registered_user
      else
        user = User.create(
          name: data['name'],
          provider: access_token.provider,
          email: data['email'],
          uid: access_token.uid,
          password: Devise.friendly_token[0, 20]
        )
      end
   end
  end

  # Whenever a user logs in, try to find a matching employee record and attach it to the user
  def find_employee
    employee = Employee.find_by email: email
    self[:employee_id] = employee&.id
  end
  before_save :find_employee
end
