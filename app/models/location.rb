class Location < ApplicationRecord
  has_many :computer_locations, dependent: :nullify
  has_many :headcount_entries, dependent: :nullify
  has_many :study_rooms, dependent: :nullify

  def is_hcc?
    return location_name.start_with?('hcc')
  end
end
