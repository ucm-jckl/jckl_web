class ClfOrder < ApplicationRecord
  has_many :clf_books, through: :clf_book_order_links
  has_many :clf_book_order_links, foreign_key: 'order_id', dependent: :destroy

  validates :session_id, presence: true
  validates :school_name, :contact_name, :contact_phone, presence: true, on: :form_submit
  validates :contact_email, presence: true, email: true, on: :form_submit

  @@tax_rate = 0.0885
  @@minimum_books = 10

  #Add a book to this order
  def add_book( book, quantity )
    #Look up an existing instance of this book for this order, or create a new one
    book_order = self.clf_book_order_links.find_by( :book_id => book.id, :order_id => self.id )
    if book_order.blank?
      book_order = ClfBookOrderLink.new
      book_order.order_id = self.id
      book_order.book_id = book.id
    end

    #Update the quantity and save the order for this book
    book_order.quantity = quantity

    return book_order.save
  end


  #Look up the book on this order and remove it
  def remove_book( book )
    book_order = self.clf_book_order_links.find_by( :book_id => book.id, :order_id => self.id )
    if book_order.blank?
      return false
    else
      return book_order.destroy.frozen?
    end
  end


  def tax
    if self.tax_exempt
      return 0
    else
      return subtotal * @@tax_rate
    end
  end


  def total
    return subtotal + tax
  end


  def subtotal
    subtotal = 0
    self.clf_book_order_links.each do |book_order|
      quantity = book_order.quantity
      price = book_order.clf_book.price
      subtotal += price * quantity
    end

    return subtotal
  end

  def tax_rate
    return @@tax_rate * 100
  end

  def book_count
    quantity = self.clf_book_order_links.sum( :quantity )
    quantity ||= 0
    return quantity
  end

  def minimum_books
    return @@minimum_books
  end

end
