class StudyRoomBooking < HighchartsDataSource

  belongs_to :study_room, optional: true

  GOOGLE_SHEET_ID = '1bTF2KVHduKR8jwyOpK4_OAeRj02L_cbO-rK1d2nBq00'


  def self.write_to_google_sheet
    sql = 'select CONVERT_TZ(b.booking_start, \'America/Chicago\', \'UTC\'),  CONCAT(\'Floor \', l.floor) as floor, r.display_name as room, b.booking_duration_minutes '
    sql += 'from study_room_bookings b '
    sql += 'left join study_rooms r on b.study_room_id=r.id '
    sql += 'left join locations l on r.location_id=l.id '
    sql += 'order by b.booking_start desc '

    data = ActiveRecord::Base.connection.execute(sql)

    table = [['Time', 'Floor', 'Room','Duration']]
    table += data.to_a

    s = GoogleApiService.new
    s.clear_google_sheet(GOOGLE_SHEET_ID)
    s.append_to_google_sheet(GOOGLE_SHEET_ID, table)

    return table
  end


  def self.hours_used_per_time_period(start_time, end_time, interval)

    x_categories = Statistic.x_time_categories(start_time, end_time, interval)

    bookings = StudyRoomBooking.includes(:study_room).where('booking_end >= ? and booking_start <= ?', start_time, end_time).order('study_rooms.display_name asc')
    chart_data = {}

    StudyRoom.all.each do |r|
      x_categories.each do |c|
        chart_data[ r.display_name ] ||= {}
        chart_data[ r.display_name ][ c ] = []
      end
    end

    bookings.each do |b|

      if interval == 'hours' && b.booking_start.present? && b.booking_end.present?
        time_marker = b.booking_start.beginning_of_hour
        while time_marker <= b.booking_end.beginning_of_hour + 1.hour
          seconds = Statistic.seconds_used_during_hour(b.booking_start, b.booking_end, time_marker)
          chart_data[ b.study_room.display_name ][ time_marker.strftime( Statistic.date_format(interval) ) ] ||= []
          chart_data[ b.study_room.display_name ][ time_marker.strftime( Statistic.date_format(interval) ) ] << seconds

          time_marker += 1.hour
        end

      else
        chart_data[ b.study_room.display_name ][ b.booking_start.strftime( Statistic.date_format(interval) ) ] ||= []
        chart_data[ b.study_room.display_name ][ b.booking_start.strftime( Statistic.date_format(interval) ) ] << b.booking_end - b.booking_start
      end

    end
    period_totals = {}
    chart_data.each do |room, times|
      times.each do |time, seconds_used|

        if ['hours_avg', 'weekdays_avg', 'weekdays_hours_avg'].include?(interval)
          period_totals[time] ||= 0
          this_period_total = (seconds_used.reduce(:+) || 0) / (60.0 * 60.0) / seconds_used.length
          if !this_period_total.nan?
            period_totals[time] += this_period_total
          end
          chart_data[room][time] = (seconds_used.reduce(:+) || 0) / (60.0 * 60.0) / seconds_used.length
          chart_data[room][time] = 0 if chart_data[room][time].nan?
        else
          period_totals[time] ||= 0
          period_totals[time] += (seconds_used.reduce(:+) || 0) / (60.0 * 60.0)
          chart_data[room][time] = (seconds_used.reduce(:+) || 0) / (60.0 * 60.0)
        end
      end
    end
    chart_data['All'] = period_totals

    highcharts_series_data = []
    chart_data.each do |room, times|
      series = {
        name: room,
        data: []
      }
      times.each do |x, y|
        series[:data] << [x, y]
      end
      highcharts_series_data << series
    end

    return highcharts_series_data
  end


  def self.percent_used_per_time_period(start_time, end_time, interval)
    hours_data = StudyRoomBooking.hours_used_per_time_period(start_time, end_time, interval)
    percent_data = hours_data

    minutes_open_per_time_period = {}
    hours_data[0][:data].each do |data|
      time_period = Chronic.parse(data[0])
      if interval == 'days'
        minutes_open = LibraryHour.minutes_open_on_date(time_period)
      elsif interval == 'weeks'
        year = data[0].downcase.split(', week')[0]
        week_number = data[0].downcase.split(', week')[1].to_i
        week_start_date = Chronic.parse("#{year}-01-01") + week_number.weeks
        minutes_open = LibraryHour.minutes_open_on_week(week_start_date)
      elsif interval == 'months'
        minutes_open = LibraryHour.minutes_open_on_month(time_period)
      elsif interval == 'years'
        minutes_open = LibraryHour.minutes_open_on_year(time_period)
      elsif interval == 'weekdays_avg'
        minutes_open = LibraryHour.minutes_open_between(start_time, end_time, Statistic.date_format(interval))
      end
      minutes_open_per_time_period[data[0]] = minutes_open
    end

    hours_data.each_with_index do |series, i|
      series[:data].each_with_index do |data, j|
        minutes_open = minutes_open_per_time_period[data[0]] || 0

        if series[:name] == 'All'
          minutes_open *= hours_data.length - 1 # the -1 is to account for the All series from the room count
        end

        minutes_used = data[1]
        percent_hours_used = (minutes_used.to_f / (minutes_open / 60.0) ) * 100
        percent_data[i][:data ][j][1] = percent_hours_used
      end
    end


    return percent_data
  end


  def self.hours_used_per_room(start_time, end_time)
    bookings = StudyRoomBooking.includes(:study_room).where('booking_end >= ? and booking_start <= ?', start_time, end_time).order('study_rooms.display_name asc')
    chart_data = {}
    bookings.each do |b|
      chart_data[b.study_room.display_name] ||= []
      chart_data[b.study_room.display_name] << (b.booking_end - b.booking_start)
    end
    chart_data.each do |k, minutes_array|
      chart_data[k] = (minutes_array.reduce(:+) || 0) / (60.0 * 60.0) # Convert seconds to hours
    end

    return chart_data
  end


  # Get bookings from LibCal and save them locally
  def self.get_bookings_from_libcal
    s = LibcalApiService.new
    libcal_bookings = []
    StudyRoom.all.each do |r|
      next if r.springshare_space_id.blank?
      libcal_bookings += s.get_bookings(r.springshare_space_id)
    end

    libcal_bookings.each do |lb|
      existing_booking = StudyRoomBooking.find_by(springshare_booking_id: lb['bookId'])
      if existing_booking.blank?
        new_booking = StudyRoomBooking.new(
          study_room: StudyRoom.find_by(springshare_space_id: lb['eid']),
          email: lb['email'],
          booking_start: lb['fromDate'],
          booking_end: lb['toDate'],
          booking_duration_minutes: (Chronic.parse(lb['toDate']) - Chronic.parse(lb['fromDate'])) / 60,
          springshare_booking_id: lb['bookId']
        )
        new_booking.save
      end
    end
  end



end
