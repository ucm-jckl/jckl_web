class SubjectArea < ApplicationRecord
  belongs_to :subject_librarian, class_name: 'Employee', foreign_key: 'subject_librarian_id', optional: true
  belongs_to :parent_subject, class_name: 'SubjectArea', foreign_key: 'parent_subject_id', optional: true
  has_many :child_subjects, class_name: 'SubjectArea', foreign_key: 'parent_subject_id', dependent: :nullify
  has_many :call_number_ranges, dependent: :nullify

  def self.subjects_with_call_number_ranges
    SubjectArea.includes(:call_number_ranges).where.not(call_number_ranges: { id: nil })
  end


  def new_books
    all_new_books = NewBook.all
    subject_new_books = NewBook.none
    call_number_ranges.each do |range|
      all_new_books.each do |book|
        subject_new_books << book if book.within_call_number_range?(range.id)
      end
    end
    subject_new_books.distinct
  end


  def self.assign_subjects_to_librarians
    old_scdps = {}
    SubjectArea.all.each do |subject|
      if subject.scdp_url.present?
        old_scdps[subject.springshare_id] = subject.scdp_url
      end
    end
    new_subjects = SubjectArea.get_subjects_from_springshare
    SubjectArea.delete_all
    new_subjects.each do |subject|
      librarian = Employee.find_by(email: subject[:librarian])
      next unless librarian.present?
      subject_area = SubjectArea.new
      subject_area.subject_name = subject[:name]
      subject_area.display_name = subject[:name]
      subject_area.subject_librarian_id = librarian.id
      subject_area.springshare_id = subject[:id]
      if old_scdps[subject[:id].to_i]
        subject_area.scdp_url = old_scdps[subject[:id].to_i]
      end

      subject_area.save
    end
  end


  def self.get_subjects_from_springshare
    subjects = []

    auth_response = HTTParty.post("https://lgapi-us.libapps.com/1.2/oauth/token", body: {
      client_id: Rails.application.secrets.libguides_client_id,
      client_secret: Rails.application.secrets.libguides_client_secret,
      grant_type: 'client_credentials'
    })&.parsed_response
    auth_token = JSON.parse(auth_response)&.dig('access_token')

    if auth_token.present?
      lg_accounts = HTTParty.get("https://lgapi-us.libapps.com/1.1/accounts?site_id=#{Rails.application.secrets.libguides_site_id}&key=#{Rails.application.secrets.libguides_site_key}&expand[]=subjects&expand[]=profile")&.parsed_response
      lg_accounts.each do |account|
        next unless account['subjects'].present?
        account['subjects'].each do |subject|
          subjects << {
            name: subject['name'],
            librarian: account['email'],
            id: subject['id']
          }
        end
      end
    end
    return subjects
  end
end
