class FundCodeLibrarianLink < ApplicationRecord
  belongs_to :librarian, class_name: 'Employee', foreign_key: 'librarian_id'
  belongs_to :fund_code
end
