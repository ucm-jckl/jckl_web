ActiveAdmin.register Location do
	permit_params :location_name, :display_name, :floor, :sort, :stat_form_start_date, :stat_form_end_date
	menu parent: 'Facilities'


end
