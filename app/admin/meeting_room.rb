ActiveAdmin.register MeetingRoom do
	permit_params :room_name, :display_name, :description, :google_calendar_id, :floor, :library_only, :faculty_staff_only, :classroom, :meetings, :seating, :desktops, :laptops, :projector, :podium, :whiteboard, :photo

	menu parent: 'Facilities'

	form do |f|
		f.inputs "Meeting Room Details" do
			f.input :room_name
			f.input :display_name
			f.input :description
			f.input :google_calendar_id
			f.input :floor
			f.input :library_only
			f.input :faculty_staff_only
			f.input :classroom
			f.input :meetings
			f.input :seating
			f.input :desktops
			f.input :laptops
			f.input :projector
			f.input :podium
			f.input :whiteboard

			f.input :photo, as: :file    
	  end
    f.actions
	end
end
