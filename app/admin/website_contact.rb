ActiveAdmin.register WebsiteContact do
	permit_params :contact_name, :contact_email, :topic, :recipients, :subject, :message
	menu parent: 'Website'


end
