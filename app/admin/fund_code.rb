ActiveAdmin.register FundCode do

  menu parent: 'Bibliographic'

  permit_params :code, :program, :librarians, :fund_code_librarian_links, librarian_ids: []

  form do |f|
		f.inputs "Fund Details" do
			f.input :code
			f.input :program
			f.input :librarians, collection: Employee.where(is_faculty: true).order('last_name, first_name').pluck("CONCAT(last_name, ', ', first_name)", :id), input_html: {class: 'chosen'}
	  end
    f.actions
	end

  show do
    attributes_table do
      row :code
      row :program
      row 'Librarians' do |fund_code|
        fund_code.librarians.map{|l| l.first_last}.join(", ")
      end
    end
  end

end
