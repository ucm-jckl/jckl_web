ActiveAdmin.register ComputerOperatingSystem do
	permit_params :os_family_name, :os_version_name
	menu parent: 'Facilities'
end
