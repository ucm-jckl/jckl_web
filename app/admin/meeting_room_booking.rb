ActiveAdmin.register MeetingRoomBooking do
	permit_params :requestor_name, :requestor_email, :contact_name, :contact_email, :contact_phone, :contact_dept, :contact_address, :meeting_room_id, :start_time, :end_time, :event_name, :setup_time, :takedown_time, :recurrence, :attendance, :notes

	menu parent: 'Facilities'


end
