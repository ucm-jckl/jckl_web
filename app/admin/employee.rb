ActiveAdmin.register Employee do
	permit_params :first_name, :last_name, :email, :phone, :office, :title, :rank, :is_faculty, :supervisor_id, :department_id, :image_name, :libguides_profile_url, :hide_from_public_views

	menu parent: 'Personnel'

	form do |f|
		f.inputs "Employee Details" do
			f.input :first_name
			f.input :last_name
			f.input :email
			f.input :phone
			f.input :office
			f.input :title
			f.input :rank
			f.input :is_faculty
			f.input :supervisor, collection: Employee.all.order('last_name, first_name').pluck("CONCAT(last_name, ', ', first_name)", :id)
			f.input :department
			f.input :image_name
			f.input :libguides_profile_url
			f.input :hide_from_public_views
	  end
    f.actions
	end

end
