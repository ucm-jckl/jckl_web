ActiveAdmin.register CallNumberRange do
	permit_params :range_start, :range_end, :range_name, :subject_area_id

	menu parent: 'Bibliographic'


end
