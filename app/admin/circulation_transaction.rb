ActiveAdmin.register CirculationTransaction do
	permit_params :transaction_id, :transaction_gmt, :application_name, :source_code, :op_code, :item_record_id, :bib_record_id, :stat_group_code_num, :best_title_norm, :best_author_norm, :item_location_code, :call_number, :itype_code, :itype_name, :ptype_code, :ptype_name, :patron_checkout_total, :patron_home_library_code

	menu parent: 'Statistics'


end
