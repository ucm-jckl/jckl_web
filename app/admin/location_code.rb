ActiveAdmin.register LocationCode do
	permit_params :location_code, :location_name

	menu parent: 'Bibliographic'

end
