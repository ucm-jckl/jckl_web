ActiveAdmin.register NavigationMenuItem do
	permit_params :url, :label, :element_class, :element_id, :position, :show_in_footer, :navigation_menu_item_id

	menu parent: 'Website'
	sortable tree: true

	index as: :sortable do
		para "Note: Only the first three levels of menu items will be displayed in the navigation menu. URLs in top-level menu items will not work."

		label do |item|
			label = item.label + " ..... URL: #{item.url}"
		end
		actions
	end

	form do |f|
    f.inputs "Navigation Menu Item Details" do
      f.input :label, :as => :string
      f.input :url, :as => :string, :label => "URL"
      f.input :element_class, :as => :string, :label => "HTML Class"
      f.input :element_id, :as => :string, :label => "HTML ID"
      f.input :show_in_footer, :as => :boolean
    end
    f.actions
  end

end
