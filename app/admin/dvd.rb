ActiveAdmin.register Dvd do
	permit_params :title, :subtitle, :title_sort, :copyright_date, :summary, :image_url, :imdb_id, :tmdb_id, :call_number, :volume, :location_code_id, :rating, :runtime, :language, :checkouts, :ytd_checkouts, :last_ytd_checkouts, :available, :cdate, :item_record_id, :bib_record_id, :item_record_num, :bib_record_num, :is_marc_data_harvested, :is_tmdb_data_harvested

	menu parent: 'Bibliographic'

	form do |f|
		f.inputs "DVD Details" do
      f.input :title
			f.input :subtitle
			f.input :title_sort
			f.input :copyright_date
			f.input :summary
			f.input :image_url
			f.input :imdb_id
			f.input :tmdb_id
			f.input :call_number
			f.input :volume
			f.input :location_code, collection: LocationCode.all.order(:location_code).pluck("CONCAT(location_code, ' - ', location_name)", :id)
			f.input :rating
			f.input :runtime
			f.input :language
			f.input :checkouts
			f.input :ytd_checkouts
			f.input :last_ytd_checkouts
			f.input :available
			f.input :cdate
			f.input :item_record_id
			f.input :bib_record_id
			f.input :item_record_num
			f.input :bib_record_num
			f.input :is_marc_data_harvested
			f.input :is_tmdb_data_harvested


	  end
    f.actions
	end

end
