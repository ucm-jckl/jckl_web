ActiveAdmin.register LibraryHour do
	permit_params :calendar_date, :open_time, :close_time, :hours_display_text, :total_calendar_date_minutes, :total_operating_date_minutes, :hour00minutes, :hour01minutes, :hour02minutes, :hour03minutes, :hour04minutes, :hour05minutes, :hour06minutes, :hour07minutes, :hour08minutes, :hour09minutes, :hour10minutes, :hour11minutes, :hour12minutes, :hour13minutes, :hour14minutes, :hour15minutes, :hour16minutes, :hour17minutes, :hour18minutes, :hour19minutes, :hour20minutes, :hour21minutes, :hour22minutes, :hour23minutes

	menu parent: 'Facilities'


end
