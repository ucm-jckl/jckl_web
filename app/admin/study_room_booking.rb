ActiveAdmin.register StudyRoomBooking do
	permit_params :study_room_id, :email, :booking_time, :booking_duration_minutes

	menu parent: 'Statistics'


end
