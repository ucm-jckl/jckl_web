ActiveAdmin.register Absence do

  permit_params :employee_id, :submitter_id, :start_date, :end_date, :absence_type_id, :duration_minutes, :schedule, :background_information, :professional_funding, :approved, :note_required, :note_received, :google_calendar_event_id
  menu parent: 'Personnel'

  form do |f|
    f.inputs "Absence Details" do
      f.input :employee, collection: Employee.all.order('last_name, first_name').pluck("CONCAT(last_name, ', ', first_name)", :id)
      f.input :submitter, collection: Employee.all.order('last_name, first_name').pluck("CONCAT(last_name, ', ', first_name)", :id)
      f.input :start_date
      f.input :end_date
      f.input :absence_type
      f.input :duration_minutes
      f.input :schedule
      f.input :background_information
      f.input :professional_funding
      f.input :approved
      f.input :note_required
      f.input :note_received
      f.input :google_calendar_event_id
    end
    f.actions
  end



end
