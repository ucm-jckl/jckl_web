ActiveAdmin.register Department do
	permit_params :department_name, :display_name, :department_head_id, :parent_department_id

	menu parent: 'Personnel'

	form do |f|
		f.inputs "Department Details" do
      f.input :department_name
      f.input :display_name
      f.input :department_head, collection: Employee.all.order('last_name, first_name').pluck("CONCAT(last_name, ', ', first_name)", :id)
			f.input :parent_department
	  end
    f.actions
	end

end
