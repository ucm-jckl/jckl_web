ActiveAdmin.register Computer do
	permit_params :map_tag, :ip_address, :computer_name, :computer_build_id, :active

	menu parent: 'Facilities'

end
