ActiveAdmin.register ClfBook do
	permit_params :title, :primary_author, :secondary_author, :illustrator, :isbn, :price, :festival_year, :summary, :image_url, :info_page_url

	menu parent: 'Bibliographic'


end
