ActiveAdmin.register AbsenceType do
	permit_params :absence_type_name, :display_name, :sort
	
	menu parent: 'Personnel'
end
