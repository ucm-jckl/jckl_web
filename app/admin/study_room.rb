ActiveAdmin.register StudyRoom do
	permit_params :room_name, :display_name, :location_id, :active

	menu parent: 'Facilities'


end
