ActiveAdmin.register NewBookRequest do
	permit_params :requestor_name, :requestor_email, :format, :title, :edition, :author, :isbn, :publisher, :year, :series, :volumes, :list_price, :copies_ordered, :recommender, :approved_by, :subject_area_id, :notes, :submitted_by

	menu parent: 'Bibliographic'


end
