ActiveAdmin.register User do

  permit_params :email, :n700, :name

  menu parent: 'Personnel'


  after_save do |user|
    UserRole.where(user_id: user.id).delete_all
    params[:user][:roles].each do |role|
      next unless role.present?
      UserRole.new(name: role, user_id: user.id).save
    end
  end

  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :last_sign_in_at
    column :sign_in_count
    column 'Roles' do |user|
      user.user_roles.pluck(:name).join(', ')
    end
    actions
  end

  form do |f|
    f.inputs "User Details" do
      f.input :name
      f.input :email
      f.input :n700, label: "700 #"
      f.input "roles", as: :check_boxes, collection: UserRole::ROLES.map{|k,v| [v, v, {checked: f.object.user_roles.pluck(:name).include?(v)}]}
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :email
      row :n700
      row :current_sign_in_at
      row :last_sign_in_at
      row :sign_in_count
      row 'Roles' do |user|
        user.user_roles.pluck(:name).join(', ')
      end
    end
  end

end
