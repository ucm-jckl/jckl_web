ActiveAdmin.register SubjectArea do
	permit_params :subject_name, :display_name, :subject_librarian_id, :parent_subject_id, :fund_code, :scdp_url, :scdp_name, :springshare_id

	menu parent: 'Bibliographic'

	index do
		id_column
		column :display_name
		column :fund_code
		column :springshare_id
		column :scdp_url
		column :scdp_name
		column 'Employee' do |s|
			s.subject_librarian&.last_first
		end
		actions
	end

	form do |f|
    f.inputs "News Item Details" do
      f.input :subject_name
      f.input :display_name
			f.input :scdp_name
			f.input :scdp_url
			f.input :springshare_id
      f.input :subject_librarian, collection: Employee.all.order('last_name, first_name').pluck("CONCAT(last_name, ', ', first_name)", :id)
      f.input :parent_subject
    end
    f.actions
  end

end
