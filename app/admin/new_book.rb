ActiveAdmin.register NewBook do
	permit_params :title, :author, :call_number, :isns, :year, :summary, :location_code_id, :image_url, :info_page_url, :record_number, :item_record_id, :bib_record_id, :cdate

	menu parent: 'Bibliographic'

	form do |f|
    f.inputs "New Book Details" do
      f.input :title
			f.input :author
			f.input :call_number
			f.input :isns
			f.input :year
			f.input :summary
			f.input :location_code, collection: LocationCode.all.order(:location_code).pluck("CONCAT(location_code, ' - ', location_name)", :id)
			f.input :image_url
			f.input :record_number
			f.input :item_record_id
			f.input :bib_record_id
			f.input :cdate
    end
    f.actions
  end

end
