ActiveAdmin.register Office do
	permit_params :display_name, :office_name, :department_id, :email, :primary_employee_id, :phone_number, :location, :webpage_url

	menu parent: 'Personnel'

	form do |f|
    f.inputs "Office Details" do
			f.input :display_name
			f.input :office_name
      f.input :department
			f.input :email
			f.input :primary_employee, collection: Employee.all.order('last_name, first_name').pluck("CONCAT(last_name, ', ', first_name)", :id)
			f.input :phone_number
			f.input :location
			f.input :webpage_url
    end
    f.actions
  end


end
