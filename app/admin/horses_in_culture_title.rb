ActiveAdmin.register HorsesInCultureTitle do
	permit_params :title, :publication_date, :publisher, :publication_place, :number_pages, :illustrations, :equestrian_discipline, :breed, :ethnicity, :notes, :subjects, :author

	menu parent: 'Bibliographic'


end
