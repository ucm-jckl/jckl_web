ActiveAdmin.register ComputerBuild do
	permit_params :build_name, :description, :computer_operating_system_id

	menu parent: 'Facilities'

	form do |f|
		f.inputs "Computer Build Details" do
			f.input :build_name
			f.input :description
      f.input :computer_operating_system, collection: ComputerOperatingSystem.all.pluck( "CONCAT(os_family_name, ', ', os_version_name )", :id)
    end
    f.actions
	end


end
