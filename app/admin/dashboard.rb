ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do

    para "This is the JCKL Website admin dashboard."

    h2 "Quick Links"

    ul do
      li do
        link_to "Special Messages", "/admin/special_messages"
      end
      li do
        link_to "Navigation Menu", "/admin/navigation_menu_items"
      end
      li do
        link_to "Employees", "/admin/employees"
      end

    end

  end # content
end
