feed_options = {
  :language => 'en-us',
  :root_url => newbooks_url(:html)
}

atom_feed feed_options do |feed|
  feed.title "JCKL New Arrivals - #{@active_subject&.display_name || @active_call_number_range&.range_summary || 'All'}"
  feed.author do |author|
    author.name "James C. Kirkpatrick Library"
  end
  feed.subtitle "Newly arrived books at the James C. Kirkpatrick Library"
  feed.updated Time.now

  @books.each do |book|
    entry_options = {
      :id => book.quest_url,
      :url => book.quest_url
    }
    feed.entry book, entry_options do |entry|
      entry.title book.title
      entry.updated book.cdate.to_datetime
      entry.author do |author|
        author.name book.author
      end
      entry.summary book.summary
      entry.published book.cdate.to_datetime
      entry << "<media:thumbnail  xmlns:media='http://search.yahoo.com/mrss/' url='#{html_escape(book.full_image_url( true ))}' />"
    end
  end
end
