# config valid only for current version of Capistrano
# lock '3.11.0'

set :application, 'jckl_web'
set :repo_url, 'git@bitbucket.org:ucm-jckl/jckl_web.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :branch, '2.0'

set :use_sudo, false

set :rbenv_ruby, '2.5.7'

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"

# Default value for linked_dirs is []
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system'

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 3

# Set file permissions for executables
set :file_permissions_paths, ["bin"]
set :file_permissions_chmod_mode, "0755"
before "deploy:published", "deploy:set_permissions:chmod"
