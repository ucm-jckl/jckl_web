Rails.application.routes.draw do

  # Home page and sitemap routes
  root to: redirect('https://ucmo.primo.exlibrisgroup.com/discovery/search?vid=01UCMO_INST:jckl_custom')
  get 'index', to: redirect('https://ucmo.primo.exlibrisgroup.com/discovery/search?vid=01UCMO_INST:jckl_custom')

  # Hours routes
  resources :library_hours, only: [:index], path: 'hours'

  # News routes REDIRECT
  resources :news_items, path: 'news', only: [:index, :show], param: :friendly_url

  # Database routes  get 'databases', to: redirect('http://guides.library.ucmo.edu/az.php')
  get 'databases', to: redirect('http://guides.library.ucmo.edu/az.php')

  # Employee routes
  get 'employees/absence', to: 'absences#new', as: 'legacy_absence' #redirect old absence form route
  get 'employees/index', to: redirect('https://guides.library.ucmo.edu/employees') # redirect old employee directory
  get 'employees', to: redirect('https://guides.library.ucmo.edu/employees') # redirect old employee directory

  get 'scdps', to: 'employees#scdps'
  resources :employees, except: :index, param: :name do
    collection do
      get 'portal'
      get 'student-workers', to: 'employees#student_workers'
    end

    resources :absences do
      patch 'approve', on: :member
      patch 'reject', on: :member
      patch 'receive_note', on: :member
    end
  end
  get 'absences', to: 'absences#by_date', as: 'absences_by_date'
  get 'absences/:date', to: 'absences#by_date'

  resources :absences, only: [:new, :create, :index]


  # Contact routes REDIRECT
  resources :website_contacts, path: 'contact', only: [:create] do
    collection do
      get 'general'
      get 'employee/:name', action: 'employee', as: 'employee'
      get 'office/:name', action: 'office', as: 'office'
      get 'info'
    end
  end
  get 'contact', to: 'website_contacts#general'
  get 'contact/chat', to: 'website_contacts#chat'
  get 'chat', to: 'website_contacts#chat'

  # Map routes REDIRECT
  get 'map', to: 'map#index', as: 'map'
  get 'map/index', to: 'map#index'

  # Room booking routes
  get 'room-bookings', to: redirect('https://ucmo.libcal.com/reserve/jckl-rooms'), as: 'meeting_rooms' #old url redirect
  get 'roomschedules', to: redirect('https://ucmo.libcal.com/reserve/jckl-rooms') # old url redirect


  # New books routes REDIRECT
  resources :new_books, path: 'new-books', only: [:index] do
    get 'mappings', on: :collection
  end
  get 'newbooks', to: 'new_books#index'

  # Purchase request routes REDIRECT
  resources :new_book_requests, path: 'purchase-request', only: [:new, :create]
  get 'purchase-request', to: 'new_book_requests#new'

  # DVD routes REDIRECT
  get 'dvds', to: 'dvds#index', as: 'dvds'
  get 'dvds/index', to: 'dvds#index'

  # Stats routes
  get 'stats', to: 'stats#dashboard'
  get 'stats/dashboard', to: 'stats#dashboard', as: 'stats_dashboard'
  get 'stats/headcount_form', to: 'stats#headcount_form', as: 'stats_headcount_form'
  post 'stats/headcount_form', to: 'stats#headcount_form'
  get 'stats/headcounts', to: 'stats#headcounts', as: 'stats_headcounts'
  get 'stats/studyrooms', to: 'stats#studyrooms', as: 'stats_studyrooms'
  get 'stats/computers', to: 'stats#computers', as: 'stats_computers'

  # CLF routes
  get 'clf', to: redirect('http://clf.ucmo.edu')
  get 'clf/index', to: redirect('http://clf.ucmo.edu')
  get 'clf/books', to: 'clf#books_feed'
  get 'clf/browse'
  post 'clf/add_to_cart', as: 'clf_add_to_cart'
  get 'clf/cart', as: 'clf_cart'
  get 'clf/order', as: 'clf_order'
  post 'clf/order'

  # Tools routes
  get 'tools/callnumber-parser', to: 'tools#callnumber_parser'
  post 'tools/callnumber-parser', to: 'tools#callnumber_parser'
  post 'tools/calendar_sync', to: 'tools#calendar_sync'

  # File upload routes
  resources :files

  # Horses in Culture routes
  get 'horses_in_culture/data', to: 'horses_in_culture#data', as: 'horses_in_culture_data'

  # Error handling
  match '/401', to: 'errors#unauthorized', via: :all, as: 'error_unauthorized'
  match '/404', to: 'errors#not_found', via: :all, as: 'error_not_found'
  match '/422', to: 'errors#unprocessable', via: :all, as: 'error_unprocessable'
  match '/500', to: 'errors#internal_server_error', via: :all, as: 'error_internal_server_error'
  match '/ezproxy-error', to:'errors#ezproxy', via: :post, as: 'error_ezproxy'

  # Page element routes
  get 'api/ucm_header', to: 'api#ucm_header'
  get 'api/ucm_footer', to: 'api#ucm_footer'

  # API routes MERGE WITH ELEMENTS
  get 'api/nav', to: 'api#nav'

  # Assets handling
  get 'css/:file', to: 'assets#css'
  get 'js/:file', to: 'assets#js'
  get 'img/:file', to: 'assets#img'

  # Devise and ActiveAdmin authentication
  devise_config = ActiveAdmin::Devise.config
  devise_config[:controllers][:omniauth_callbacks] = 'omniauth_callbacks'
  devise_config[:controllers][:registrations] = 'devise/registrations'
  devise_config[:controllers][:sessions] = 'sessions'
  devise_for :users, devise_config
  ActiveAdmin.routes(self)
  get 'omniauth_callbacks/all'
end
