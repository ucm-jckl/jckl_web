require 'test_helper'

class BookTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  CALL_NUMBERS = [
    {
      raw: '|aBS2409|b.B67 2013',
      stripped: 'BS 2409 B67 2013',
      class: 'BS',
      class_number: 'BS 2409'
    },
    {
      raw: '|aPN1993 5 U6 M666 1988',
      stripped: 'PN 1993 5 U6 M666 1988',
      class: 'PN',
      class_number: 'PN 1993'
    },
    {
      raw: '|aHC110.P6|bW53 2008',
      stripped: 'HC 110 P6 W53 2008',
      class: 'HC',
      class_number: 'HC 110'
    },
    {
      raw: '|aXNC2 85593 1997',
      stripped: 'XNC 2 85593 1997',
      class: 'XNC',
      class_number: 'XNC 2'
    },
    {
      raw: '|aXNW80 3872 1990',
      stripped: 'XNW 80 3872 1990',
      class: 'XNW',
      class_number: 'XNW 80'
    },
    {
      raw: '|aXNA8 554094 1997',
      stripped: 'XNA 8 554094 1997',
      class: 'XNA',
      class_number: 'XNA 8'
    },
    {
      raw: '|fJE|aR797hc 1999 CD',
      stripped: 'R 797hc 1999 CD',
      class: 'JE R',
      class_number: 'JE R 797'
    },
    {
      raw: '|fCURR|aTX165|b.F663 2009 gr.9-12',
      stripped: 'TX 165 F663 2009 gr 9-12',
      class: 'TX',
      class_number: 'TX 165'
    },
    {
      raw: '|fJE|aB2216wh 2015',
      stripped: 'B 2216wh 2015',
      class: 'JE B',
      class_number: 'JE B 2216'
    },
    {
      raw: '|fJE|aT467ku 1984',
      stripped: 'T 467ku 1984',
      class: 'JE T',
      class_number: 'JE T 467'
    },
    {
      raw: '|fJE|a973.7 Y7988tc 2013',
      stripped: '973 7 Y7988tc 2013',
      class: 'JE 900',
      class_number: 'JE 973'
    },
    {
      raw: '|fJE|a741.5973 B4121de 2014',
      stripped: '741 5973 B4121de 2014',
      class: 'JE 700',
      class_number: 'JE 741'
    },
  ]

  test "strip call number" do
    CALL_NUMBERS.each do |cn|
      stripped = Book.stripped_call_number(cn[:raw])
      cnclass = Book.call_number_class(cn[:raw])
      cnclassnum = Book.call_number_class_number(cn[:raw])

      assert_equal stripped, cn[:stripped]
      assert_equal cnclass, cn[:class]
      assert_equal cnclassnum, cn[:class_number]
    end
  end
end
