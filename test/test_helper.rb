ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'capybara/rails'
require 'capybara/poltergeist'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
end

class ActionDispatch::IntegrationTest
  # Add Devise helpers to integration test class
  include Devise::Test::IntegrationHelpers

  # Make the Capybara DSL available in all integration tests
  include Capybara::DSL

  include Rails.application.routes.url_helpers

  # Set up default driver and wait times
  # Use super wherever this method is redefined in your individual test classes
  def setup
    Capybara.current_driver = :poltergeist
    Capybara.default_max_wait_time = 5
  end

  # Reset sessions and driver between tests
  # Use super wherever this method is redefined in your individual test classes
  def teardown
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
end

Capybara.register_driver :poltergeist do |app|
  Capybara::Poltergeist::Driver.new(app, debug:     false,
                                         js_errors: true,
                                         timeout:   5_000,
                                         phantomjs_options: ['--load-images=no', '--ignore-ssl-errors=yes', '--ssl-protocol=any'])
end

Capybara.javascript_driver = :poltergeist
