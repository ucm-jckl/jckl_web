require 'test_helper'

class DvdMailerTest < ActionMailer::TestCase
  test 'send new dvd email' do
    dvd = Dvd.first
    mail = DvdMailer.send_email(dvd)

    assert_equal mail.subject, "New DVD: #{dvd.title} (#{dvd.id})"

    assert_emails 1 do
      mail.deliver_now
    end
  end
end
