# Preview all emails at http://localhost:3000/rails/mailers/planner_email_reminders_mailer
class PlannerEmailRemindersMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/planner_email_reminders_mailer/send
  def send
    PlannerEmailRemindersMailer.send
  end

end
