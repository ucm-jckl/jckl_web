require 'test_helper'
require 'devise'

class AbsencesControllerTest < ActionDispatch::IntegrationTest
  TEST_ABSENCE_REQUEST = {
    employee_id: 38,
    submitter_id: 38,
    absence_type_id: 1,
    absence_dates_attributes: {
      '1': {
        id: '',
        absence_date: Chronic.parse('july 30').to_date,
        start_time: Chronic.parse('8 am'),
        end_time: Chronic.parse('5 pm'),
        leave_minutes_used: 0,
        leave_hours_used: 8
      },
      '2': {
        id: '',
        absence_date: Chronic.parse('july 31').to_date,
        start_time: Chronic.parse('8 am'),
        end_time: Chronic.parse('5 pm'),
        leave_minutes_used: 0,
        leave_hours_used: 8
      }
    }
  }.freeze

  setup do
    @user = User.find(2)
    sign_in @user
    @employee = @user.employee
    @subordinate = @employee.subordinates.first
    @absence = Absence.find(1)
  end

  teardown do
    travel_back
  end

  test 'get by_date' do
    get absences_by_date_path
    assert_response :success
  end

  test 'get index' do
    get employee_absences_path(employee_name: @employee.url_slug)
    assert_response :success
  end

  test 'get subordinate\'s index page as supervisor' do
    get employee_absences_path(employee_name: @subordinate.url_slug)
    assert_response :success
  end

  test 'get show' do
    get employee_absence_path(employee_name: @employee.url_slug, id: @absence.id)
    assert_response :success
  end

  test 'get new' do
    get new_employee_absence_path(employee_name: @employee.url_slug)
    assert_response :success
  end

  test 'create' do
    post employee_absences_path(employee_name: @employee.url_slug),
         params: { absence: TEST_ABSENCE_REQUEST }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert @controller.action_name == 'index'
  end

  test 'get edit' do
    get edit_employee_absence_path(employee_name: @employee.url_slug, id: @absence.id)
    assert_response :success
  end

  test 'update' do
    patch employee_absence_path(employee_name: @employee.url_slug, id: @absence.id),
          params: { absence: TEST_ABSENCE_REQUEST }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert @controller.action_name == 'index'
  end

  test 'destroy' do
    delete employee_absence_path(employee_name: @subordinate.url_slug, id: @subordinate.absences.last.id)
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert @controller.action_name == 'index'
  end

  test 'approve' do
    patch approve_employee_absence_path(employee_name: @subordinate.url_slug, id: @subordinate.absences.last.id)
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert @controller.action_name == 'show'
    assert @subordinate.absences.last.approved
  end

  test 'reject' do
    patch reject_employee_absence_path(employee_name: @subordinate.url_slug, id: @subordinate.absences.last.id)
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert @controller.action_name == 'show'
    assert !@subordinate.absences.last.approved
  end

  test 'receive note' do
    patch receive_note_employee_absence_path(employee_name: @subordinate.url_slug, id: @subordinate.absences.last.id)
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert @controller.action_name == 'show'
    assert @subordinate.absences.last.note_received
  end

  test 'wrong user cannot do anything' do
    # user signed out gets redirected to login page
    sign_out(@user)

    get employee_absences_path(employee_name: @employee.url_slug)
    assert :redirect
    follow_redirect!
    assert @controller.login_page?

    get employee_absence_path(employee_name: @employee.url_slug, id: @absence.id)
    assert :redirect
    follow_redirect!
    assert @controller.login_page?

    get new_employee_absence_path(employee_name: @employee.url_slug)
    assert :redirect
    follow_redirect!
    assert @controller.login_page?

    get edit_employee_absence_path(employee_name: @employee.url_slug, id: @absence.id)
    assert :redirect
    follow_redirect!
    assert @controller.login_page?

    patch employee_absence_path(employee_name: @employee.url_slug, id: @absence.id),
          params: { absence: TEST_ABSENCE_REQUEST }
    assert :redirect
    follow_redirect!
    assert @controller.login_page?

    post employee_absences_path(employee_name: @employee.url_slug, id: @absence.id),
         params: { absence: TEST_ABSENCE_REQUEST }
    assert :redirect
    follow_redirect!
    assert @controller.login_page?

    # wrong user signed in gets unauthorized page
    sign_in User.find(30)

    get employee_absences_path(employee_name: @employee.url_slug)
    assert_response :redirect
    follow_redirect!
    assert_response 401

    get employee_absence_path(employee_name: @employee.url_slug, id: @absence.id)
    assert_response :redirect
    follow_redirect!
    assert_response 401

    get new_employee_absence_path(employee_name: @employee.url_slug)
    assert_response :redirect
    follow_redirect!
    assert_response 401

    get edit_employee_absence_path(employee_name: @employee.url_slug, id: @absence.id)
    assert_response :redirect
    follow_redirect!
    assert_response 401

    patch employee_absence_path(employee_name: @employee.url_slug, id: @absence.id),
          params: { absence: TEST_ABSENCE_REQUEST }
    assert_response :redirect
    follow_redirect!
    assert_response 401

    post employee_absences_path(employee_name: @employee.url_slug, id: @absence.id),
         params: { absence: TEST_ABSENCE_REQUEST }
    assert_response :redirect
    follow_redirect!
    assert_response 401

    delete employee_absence_path(employee_name: @employee.url_slug, id: @employee.absences.last.id)
    assert_response :redirect
    follow_redirect!
    assert_response 401

    patch approve_employee_absence_path(employee_name: @employee.url_slug, id: @employee.absences.last.id)
    assert_response :redirect
    follow_redirect!
    assert_response 401
  end
end
