require 'test_helper'

class AccountsControllerTest < ActionDispatch::IntegrationTest
  test 'get index' do
    get account_url
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert @controller.login_page?

    sign_in User.find(2)
    get account_url
    assert_response :success
  end

  test 'post update' do
    sign_in User.find(2)
    post account_update_url(user: { n700: '700601921' })
    assert :redirect

    post account_update_url(user: { n700: 'fakenumber' })
    assert :success
  end

  test 'post renew' do
    user = User.find(2)
    user.n700 = '700601921'
    sign_in user

    post account_renew_url(checkouts: {'https://avalon.searchmobius.org/iii/sierra-api/v3/patrons/checkouts/123456' => 1})
    assert :success
  end
end
