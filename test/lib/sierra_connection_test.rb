require 'test_helper'

class SierraConnectionTest < ActiveSupport::TestCase

  test 'query sierra' do
    sierra_connection = SierraConnection.new
    results = sierra_connection.select( 'select * from sierra_view.item_view limit 5' )
    sierra_connection.disconnect

    assert_not_empty results[0]['id']
  end

end
