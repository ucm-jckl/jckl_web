require 'test_helper'

class SierraApiServiceTest < ActiveSupport::TestCase

  setup do
    @api_service = SierraApiService.new
  end

  test 'get patron by barcode' do
    patron = @api_service.get_patron_by_barcode('700601921')
    assert_not_nil patron['id']
  end

  test 'get checkouts' do
    assert_nothing_raised do
      patron = @api_service.get_patron_by_barcode('700601921')
      checkouts = @api_service.get_checkouts_by_patron_id(patron['id'])
    end
  end

  test 'get holds' do
    assert_nothing_raised do
      patron = @api_service.get_patron_by_barcode('700601921')
      holds = @api_service.get_holds_by_patron_id(patron['id'])
    end
  end

  test 'get fines' do
    assert_nothing_raised do
      patron = @api_service.get_patron_by_barcode('700601921')
      fines = @api_service.get_fines_by_patron_id(patron['id'])
    end
  end

end
