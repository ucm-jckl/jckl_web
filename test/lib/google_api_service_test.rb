require 'test_helper'

class GoogleApiServiceTest < ActiveSupport::TestCase

  setup do
    @service = GoogleApiService.new
  end

  teardown do
    # Delete any test events created
    @test_event_ids&.each do |event_id|
      @service.delete_gcal_event(AbsenceDate::DETAILED_GOOGLE_CALENDAR_ID, event_id)
    end
  end

  test 'gcal api' do
    @test_event_ids = []

    # Add an event
    new_event = @service.add_gcal_event(AbsenceDate::DETAILED_GOOGLE_CALENDAR_ID, 'test event description', Time.now.to_date, Time.now, Time.now + 2.hours)
    assert_not_empty new_event.id
    @test_event_ids << new_event.id

    # Edit the event
    edited_event = @service.edit_gcal_event(AbsenceDate::DETAILED_GOOGLE_CALENDAR_ID, new_event.id, 'updated test event description', nil, Time.now.to_date, Time.now, Time.now + 3.hours, false)
    assert_equal edited_event.summary, 'updated test event description'

    # Retrieve the event
    events = @service.get_gcal_events(AbsenceDate::DETAILED_GOOGLE_CALENDAR_ID)
    assert_not_empty events.select{|e| e.id == new_event.id}

    # Delete the event and make sure it is gone
    @service.delete_gcal_event(AbsenceDate::DETAILED_GOOGLE_CALENDAR_ID, new_event.id)
    events = @service.get_gcal_events(AbsenceDate::DETAILED_GOOGLE_CALENDAR_ID)
    assert_empty events.select{|e| e.id == new_event.id}
    @test_event_ids.delete(new_event.id)
  end

end
