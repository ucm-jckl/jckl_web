require 'test_helper'

class AbsencesTest < ActionDispatch::IntegrationTest

  Capybara.register_driver :poltergeist do |app|
    Capybara::Poltergeist::Driver.new(app, {js_errors: false})
  end

  setup do
    @user = User.find(2)
    sign_in @user
    @employee = @user.employee
    @subordinate = @employee.subordinates.first
    @absence = Absence.find(1)
    travel_to(Chronic.parse('2016-09-30 8:00 AM'))
  end

  teardown do
    travel_back
  end

  test 'show absences on date' do
    visit(absences_by_date_path(date: '2017-07-11'))
    assert page.assert_selector('.absence')
  end

  test 'show correct view of employee\'s own hours on index page' do
    visit(employee_absences_path(employee_name: @employee.url_slug))

    assert page.assert_selector('.absence', count: 8)
    assert page.find('.absence-sick-leave-self-used').assert_text('32 hours')
    assert page.find('.absence-sick-leave-family-used').assert_text('16 hours')
    assert page.find('.absence-vacation-used').assert_text('48 hours')
  end

  test 'supervisor can view approve on index page and employee cannot' do
    visit(employee_absences_path(employee_name: @subordinate.url_slug))
    assert page.assert_selector('.absence-approvals', count: 1)
    assert page.assert_selector('.absence-approval-actions', count: 1)

    sign_out(@user)
    sign_in User.find_by(email: @subordinate.email)
    visit(employee_absences_path(employee_name: @subordinate.url_slug))
    assert page.has_selector?('.absence-approvals', count: 1)
    assert page.assert_no_selector('.absence-approval-actions', count: 1)
  end
end
