require 'test_helper'

class IndexTest < ActionDispatch::IntegrationTest
  Capybara.register_driver :poltergeist do |app|
    Capybara::Poltergeist::Driver.new(app, js_errors: false)
  end

  setup do
    Capybara.raise_server_errors = false #have to do this to ignore news image 404 errors
  end

  # View time-sensitive special messages
  test 'view special messages' do
    travel_to Chronic.parse('2014-09-13 12:00 PM') do
      visit(root_path)
      assert page.assert_selector('.special-message', count: 1)
      assert page.find('.special-message').assert_text('ILLiad downtime')
    end
  end

  test 'do searches' do
    # by default searches summon
    # visit(root_path)
    # fill_in('q', with: 'america')
    # click_button(id: 'search-submit')
    # assert page.assert_selector('.article-result')
  end

  test 'show hours' do
    travel_to Chronic.parse('2016-08-23 12:00 PM') do
      visit(root_path)
      assert page.find('.today-hours-display').assert_text('7:30 AM to Midnight')
    end

    # make sure page doesn't break when hours are nil
    travel_to Chronic.parse('2000-01-01 12:00 AM') do
      visit(root_path)
      assert_equal page.status_code, 200
    end
  end

  test 'show news' do
    travel_to Chronic.parse('2016-09-01 12:00 PM') do
      visit(root_path)
      NewsItem.current_news.first(2).each do |news_item|
        next unless news_item.friendly_url.present?
        assert page.assert_selector("#news-item-#{news_item.id}")
      end
    end
  end

  test 'show new books slideshow' do
    visit(root_path)
    assert page.assert_selector('.owl-item', count: 60)
    assert page.assert_selector('.owl-item.active')
  end
end
