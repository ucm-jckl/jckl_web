require 'test_helper'

class NavigationTest < ActionDispatch::IntegrationTest
  Capybara.register_driver :poltergeist do |app|
    Capybara::Poltergeist::Driver.new(app, {js_errors: false})
  end

  setup do
    Capybara.raise_server_errors = false #have to do this to ignore news image 404 errors
  end

  test 'show header and footer nav menu items' do
    visit(root_path)

    # make sure each nav menu item is present
    NavigationMenuItem.all.each do |item|
      assert page.assert_selector("#jckl-header .nav-menu-item-#{item.id}", visible: :all)
      assert page.assert_selector("#jckl-footer .nav-menu-item-#{item.id}") if item.show_in_footer && item.url.present?
    end

    # make sure dropdown menu works
    assert page.assert_no_selector('#jckl-header .navbar-nav .dropdown.show')
    page.first('#jckl-header .navbar-nav .dropdown-toggle').click
    assert page.assert_selector('#jckl-header .navbar-nav .dropdown.show')
  end
end
