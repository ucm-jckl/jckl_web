require 'test_helper'

class DvdsTest < ActionDispatch::IntegrationTest
  Capybara.register_driver :poltergeist do |app|
    Capybara::Poltergeist::Driver.new(app, {js_errors: false})
  end
  
  test 'index' do
    travel_to(Chronic.parse('2016-11-18 8:00 AM'))

    visit(dvds_path)
    assert page.assert_text('Showing 1 - 25 of 1872 DVDs.')
    assert page.assert_selector('.dvd', count: 25)

    visit(dvds_path(page: 5))
    assert page.assert_text('Showing 101 - 125 of 1872 DVDs.')
    assert page.assert_selector('.dvd', count: 25)

    visit(dvds_path(sort: 'title'))
    assert page.assert_text('Showing 1 - 25 of 1872 DVDs.')
    assert page.assert_selector('.dvd', count: 25)
    assert page.find('.dvd:first-child').assert_text('10 Cloverfield Lane')

    visit(dvds_path(genre: 'TV Movie'))
    assert page.assert_text('Showing 1 - 4 of 4 DVDs.')
    assert page.assert_selector('.dvd', count: 4)

    visit(dvds_path(title: 'happy'))
    assert page.assert_text('Showing 1 - 2 of 2 DVDs.')
    assert page.assert_selector('.dvd', count: 2)

    travel_back
  end
end
