require 'test_helper'

class AccountsTest < ActionDispatch::IntegrationTest
  Capybara.register_driver :poltergeist do |app|
    Capybara::Poltergeist::Driver.new(app, js_errors: false)
  end

  test 'index' do
    sign_in User.find(2)

    visit(account_path)
    page.fill_in(id: 'user_n700', with: '700601921')
    page.click_button('user_submit_action')

    assert (page.has_text?('You have no checkouts to display.') || page.has_selector?('.checkout'))
  end
end
