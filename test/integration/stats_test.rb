require 'test_helper'

class StatsTest < ActionDispatch::IntegrationTest
  Capybara.register_driver :poltergeist do |app|
    Capybara::Poltergeist::Driver.new(app, {js_errors: false, timeout: 300})
  end

  test 'headcounts' do
    visit(stats_headcounts_path)
    assert page.assert_selector('#headcounts-chart .highcharts-container svg')

    # NOTE: Using time range March 1 - 31 2015 because it contains a Daylight Savings change (March 8)

    visit(stats_headcounts_path(
            params: {
              start_time: 'March 1, 2015 12:00 AM',
              end_time: 'March 31, 2015 11:59 PM',
              interval: 'days',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('#headcounts-chart .highcharts-container svg')
    assert page.assert_selector('tr', text: '2015-03-01 (Sun) 179 431 570 246')
    assert page.assert_selector('tr', text: '2015-03-16 (Mon) 0 0 0 28')
    assert page.assert_selector('tr', text: '2015-03-20 (Fri) 32 38 14 24')
    assert page.assert_selector('tr', text: '2015-03-27 (Fri) 202 280 262 171')

    visit(stats_headcounts_path(
            params: {
              start_time: 'March 1, 2015 12:00 AM',
              end_time: 'March 31, 2015 11:59 PM',
              interval: 'hours',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: '2015-03-04 (Wed) 23:00 14 30 15 17')
    assert page.assert_selector('tr', text: '2015-03-05 (Thu) 00:00 9 21 8 3')
    assert page.assert_selector('tr', text: '2015-03-05 (Thu) 01:00 0 0 0 0')
    assert page.assert_selector('tr', text: '2015-03-12 (Thu) 23:00 9 30 10 12')
    assert page.assert_selector('tr', text: '2015-03-13 (Fri) 00:00 8 18 5 11')
    assert page.assert_selector('tr', text: '2015-03-13 (Fri) 01:00 0 0 0 0')

    visit(stats_headcounts_path(
            params: {
              start_time: 'March 1, 2015 12:00 AM',
              end_time: 'March 7, 2015 11:59 PM',
              interval: 'hours',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: '2015-03-02 (Mon) 07:00 0 0 0 0')
    assert page.assert_selector('tr', text: '2015-03-02 (Mon) 08:00 7 16 3 3')
    assert page.assert_selector('tr', text: '2015-03-03 (Tue) 23:00 12 44 38 12')
    assert page.assert_selector('tr', text: '2015-03-04 (Wed) 00:00 12 15 19 10')
    assert page.assert_selector('tr', text: '2015-03-04 (Wed) 01:00 0 0 0 0')
    assert page.assert_selector('tr', text: '2015-03-05 (Thu) 08:00 18 11 6 1')

    visit(stats_headcounts_path(
            params: {
              start_time: 'March 9, 2015 12:00 AM',
              end_time: 'March 16, 2015 11:59 PM',
              interval: 'hours',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: '2015-03-09 (Mon) 00:00 5 20 15 15')
    assert page.assert_selector('tr', text: '2015-03-09 (Mon) 01:00 0 0 0 0')
    assert page.assert_selector('tr', text: '2015-03-09 (Mon) 23:00 18 71 35 27')
    assert page.assert_selector('tr', text: '2015-03-10 (Tue) 00:00 3 27 19 15')
    assert page.assert_selector('tr', text: '2015-03-10 (Tue) 01:00 0 0 0 0')
    assert page.assert_selector('tr', text: '2015-03-10 (Tue) 08:00 3 15 4 2')

    visit(stats_headcounts_path(
            params: {
              start_time: 'March 1, 2015 12:00 AM',
              end_time: 'March 31, 2015 11:59 PM',
              interval: 'weeks',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: '2015, Week 09 (Mar 01) 2027 4101 3784 1639')
    assert page.assert_selector('tr', text: '2015, Week 11 (Mar 15) 88 124 54 138')
    assert page.assert_selector('tr', text: '2015, Week 13 (Mar 29) 769 1920 1862 880')

    visit(stats_headcounts_path(
            params: {
              start_time: 'March 1, 2015 12:00 AM',
              end_time: 'March 31, 2015 11:59 PM',
              interval: 'months',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: '2015-03 6516 13842 11991 5967')

    visit(stats_headcounts_path(
            params: {
              start_time: 'March 1, 2015 12:00 AM',
              end_time: 'March 31, 2015 11:59 PM',
              interval: 'years',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: '2015 6516 13842 11991 5967')

    visit(stats_headcounts_path(
            params: {
              start_time: 'March 1, 2015 12:00 AM',
              end_time: 'March 31, 2015 11:59 PM',
              interval: 'hour_averages',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: '00:00 3.81 11.03 5.47 6.19')
    assert page.assert_selector('tr', text: '01:00 0.0 0.0 0.0 0.0')
    assert page.assert_selector('tr', text: '21:00 11.42 38.32 27.65 11.03')
    assert page.assert_selector('tr', text: '22:00 8.45 30.35 23.84 13.03')
    assert page.assert_selector('tr', text: '23:00 6.23 21.23 13.77 10.71')

    visit(stats_headcounts_path(
            params: {
              start_time: 'March 1, 2015 12:00 AM',
              end_time: 'March 31, 2015 11:59 PM',
              interval: 'weekday_averages',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: 'Sun 128.2 326.0 391.2 165.2')
    assert page.assert_selector('tr', text: 'Tue 281.6 614.2 513.4 206.2')
    assert page.assert_selector('tr', text: 'Thu 285.0 504.75 439.5 243.0')
    assert page.assert_selector('tr', text: 'Sat 38.0 84.0 68.75 0.0')

    visit(stats_headcounts_path(
            params: {
              start_time: 'March 1, 2015 12:00 AM',
              end_time: 'March 31, 2015 11:59 PM',
              interval: 'weekday_hour_averages',
              grouping: 'floor'
            }
    ))

    assert page.assert_selector('tr', text: 'Mon 00:00 3.17 9.33 6.83 7.83')
    assert page.assert_selector('tr', text: 'Mon 08:00 4.4 7.2 3.4 2.6')
    assert page.assert_selector('tr', text: 'Mon 23:00 9.0 42.2 27.4 16.8')
    assert page.assert_selector('tr', text: 'Thu 00:00 7.5 17.75 6.0 6.25')
    assert page.assert_selector('tr', text: 'Thu 01:00 0.0 0.0 0.0 0.0')
    assert page.assert_selector('tr', text: 'Thu 23:00 8.25 20.0 11.75 15.0')

    visit(stats_headcounts_path(
            params: {
              start_time: 'March 1, 2015 12:00 AM',
              end_time: 'March 31, 2015 11:59 PM',
              interval: 'days',
              grouping: 'all'
            }
    ))

    assert page.assert_selector('tr', text: '2015-03-01 (Sun) 1426')
    assert page.assert_selector('tr', text: '2015-03-09 (Mon) 2603')
    assert page.assert_selector('tr', text: '2015-03-05 (Thu) 1916')
    assert page.assert_selector('tr', text: '2015-03-16 (Mon) 28')
    assert page.assert_selector('tr', text: '2015-03-25 (Wed) 2271')

    visit(stats_headcounts_path(
            params: {
              start_time: 'March 1, 2015 12:00 AM',
              end_time: 'March 31, 2015 11:59 PM',
              interval: 'days',
              grouping: 'location'
            }
    ))

    assert page.assert_selector('.jckl-svg-map .viewport')
    assert page.assert_selector('.map-color-scale', text: '0403.5807')
    assert page.assert_selector('tr', text: '2015-03-01 (Sun) 17 246 26 6 0 111 19 186 176 19 37 13 570')
    assert page.assert_selector('tr', text: '2015-03-03 (Tue) 68 244 120 8 0 158 82 379 412 56 82 8 777')
    assert page.assert_selector('tr', text: '2015-03-06 (Fri) 19 165 78 2 0 82 60 83 83 43 18 2 218')
    assert page.assert_selector('tr', text: '2015-03-10 (Tue) 82 273 129 15 0 140 56 354 398 56 71 20 777')
    assert page.assert_selector('tr', text: '2015-03-18 (Wed) 10 22 0 7 0 6 3 24 6 6 10 0 16')

  end



  test 'study rooms' do
    visit(stats_studyrooms_path(
            params: {
              start_time: 'August 1, 2015 12:00 AM',
              end_time: 'December 31, 2015 11:59 PM',
            }
    ))

    assert page.assert_selector('#hours-used-chart .highcharts-container')
    assert page.assert_selector('#percent-used-chart .highcharts-container')

    page.find('#hours-used-accordion h3 a').click
    page.find('#percent-used-accordion h3 a').click
    page.find('#room-comparison-accordion h3 a').click

    # Test hours
    page.all('#hours-data .time-period-toggle .btn')[0].click
    assert page.assert_selector('tr', text: '2015-08-16 (Sun) 08:00 All 1')
    assert page.assert_selector('tr', text: '2015-08-16 (Sun) 09:00 All 1')
    assert page.assert_selector('tr', text: '2015-08-16 (Sun) 10:00 All 0')
    assert page.assert_selector('tr', text: '2015-08-17 (Mon) 14:00 All 3')


    # Test days
    page.all('#hours-data .time-period-toggle .btn')[1].click
    assert page.assert_selector('tr', text: '2015-08-16 (Sun) All 2')
    assert page.assert_selector('tr', text: '2015-08-21 (Fri) All 53')

    # Test weeks
    page.all('#hours-data .time-period-toggle .btn')[2].click
    assert page.assert_selector('tr', text: '2015, Week 33 All 279')
    assert page.assert_selector('tr', text: '2015, Week 34 All 642')

    # Test months
    page.all('#hours-data .time-period-toggle .btn')[3].click
    assert page.assert_selector('tr', text: '2015-08 All 1283')
    assert page.assert_selector('tr', text: '2015-09 All 6299')

    # Test years
    page.all('#hours-data .time-period-toggle .btn')[4].click
    assert page.assert_selector('tr', text: '2015 All 22584')

    # Test hour averages
    page.all('#hours-data .time-period-toggle .btn')[5].click
    assert page.assert_selector('tr', text: '09:00 All 28')
    assert page.assert_selector('tr', text: '14:00 All 28')

    # Test weekday averages
    page.all('#hours-data .time-period-toggle .btn')[6].click
    assert page.assert_selector('tr', text: 'Sun All 11.3')
    assert page.assert_selector('tr', text: 'Fri All 6.24')

    # Test hour/weekday averages
    page.all('#hours-data .time-period-toggle .btn')[7].click
    assert page.assert_selector('tr', text: 'Mon 03:00 0.73')
    assert page.assert_selector('tr', text: 'Thu 17:00 0.45')

    # Test % days
    page.all('#percent-data .time-period-toggle .btn')[7].click
    assert page.assert_selector('tr', text: '2015-08-24 (Mon) All 4.0')
    assert page.assert_selector('tr', text: '2015-08-29 (Sat) All 2.0')

    # Test room comparison
    assert page.assert_selector('tr', text: 'Study Room 2218 870.0')
    assert page.assert_selector('tr', text: 'Study Room 3218 776.1')

  end


  test 'computers' do
    visit(stats_computers_path)
    assert page.assert_selector('#computers-chart .highcharts-container svg')

    visit(stats_computers_path(
            params: {
              start_time: 'September 1, 2016 12:00 AM',
              end_time: 'September 2, 2016 11:59 PM',
              interval: 'days',
              grouping: 'all',
            }
    ))
    assert page.assert_no_selector('.jckl-svg-map .viewport')
    assert page.assert_selector('tr', text: '2016-09-01 (Thu) 360')
    assert page.assert_selector('tr', text: '2016-09-02 (Fri) 0')

    visit(stats_computers_path(
            params: {
              start_time: 'September 1, 2016 12:00 AM',
              end_time: 'September 2, 2016 11:59 PM',
              interval: 'days',
              grouping: 'floor',
            }
    ))
    assert page.assert_no_selector('.jckl-svg-map .viewport')
    assert page.assert_selector('tr', text: '2016-09-01 (Thu) 360')
    assert page.assert_selector('tr', text: '2016-09-02 (Fri) 0')

    visit(stats_computers_path(
            params: {
              start_time: 'September 1, 2016 12:00 AM',
              end_time: 'September 2, 2016 11:59 PM',
              interval: 'hours',
              grouping: 'location',
            }
    ))
    assert page.assert_selector('.jckl-svg-map .viewport')
    assert page.assert_selector('.map-color-scale', text: '060120')
    assert page.assert_selector('tr', text: '2016-09-01 (Thu) 16:00 120')
    assert page.assert_selector('tr', text: '2016-09-01 (Thu) 19:00 0')
    assert page.assert_selector('tr', text: '2016-09-02 (Fri) 18:00 0')

    visit(stats_computers_path(
            params: {
              start_time: 'September 1, 2016 12:00 AM',
              end_time: 'September 2, 2016 11:59 PM',
              interval: 'weeks',
              grouping: 'location',
            }
    ))
    assert page.assert_selector('.jckl-svg-map .viewport')
    assert page.assert_selector('.map-color-scale', text: '0180360')
    assert page.assert_selector('tr', text: '2016, Week 35 (Aug 28) 360')

    visit(stats_computers_path(
            params: {
              start_time: 'September 1, 2016 12:00 AM',
              end_time: 'September 2, 2016 11:59 PM',
              interval: 'months',
              grouping: 'location',
            }
    ))
    assert page.assert_selector('.jckl-svg-map .viewport')
    assert page.assert_selector('.map-color-scale', text: '0180360')
    assert page.assert_selector('tr', text: '2016-09 360')

    visit(stats_computers_path(
            params: {
              start_time: 'September 1, 2016 12:00 AM',
              end_time: 'September 2, 2016 11:59 PM',
              interval: 'years',
              grouping: 'location',
            }
    ))
    assert page.assert_selector('.jckl-svg-map .viewport')
    assert page.assert_selector('.map-color-scale', text: '0180360')
    assert page.assert_selector('tr', text: '2016 360')

    visit(stats_computers_path(
            params: {
              start_time: 'September 1, 2016 12:00 AM',
              end_time: 'September 2, 2016 11:59 PM',
              interval: 'hour_averages',
              grouping: 'location',
            }
    ))
    assert page.assert_selector('.jckl-svg-map .viewport')
    assert page.assert_selector('.map-color-scale', text: '03060')
    assert page.assert_selector('tr', text: '16:00 60.0')
    assert page.assert_selector('tr', text: '19:00 0.0')

    visit(stats_computers_path(
            params: {
              start_time: 'September 1, 2016 12:00 AM',
              end_time: 'September 2, 2016 11:59 PM',
              interval: 'weekday_averages',
              grouping: 'location',
            }
    ))
    assert page.assert_selector('.jckl-svg-map .viewport')
    assert page.assert_selector('.map-color-scale', text: '0180360')
    assert page.assert_selector('tr', text: 'Thu 360.0')
    assert page.assert_selector('tr', text: 'Fri 0')

    visit(stats_computers_path(
            params: {
              start_time: 'September 1, 2016 12:00 AM',
              end_time: 'September 2, 2016 11:59 PM',
              interval: 'weekday_hour_averages',
              grouping: 'location',
            }
    ))
    assert page.assert_selector('.jckl-svg-map .viewport')
    assert page.assert_selector('.map-color-scale', text: '060120')
    assert page.assert_selector('tr', text: 'Thu 16:00 120.0')
    assert page.assert_selector('tr', text: 'Thu 18:00 120.0')
    assert page.assert_selector('tr', text: 'Thu 20:00 0.0')

  end


  test 'headcount form' do
    sign_in User.find(2)
    visit(stats_headcount_form_path(params: { date: '2015-01-07' }))

    row_2 = page.find_all('tr')[2].find_all('td')

    assert_equal 'Circ', row_2[0].text
    assert_equal '0', row_2[1].find('input').value
    assert_equal '0', row_2[2].find('input').value
    assert_equal '0', row_2[3].find('input').value
    assert_equal '0', row_2[4].find('input').value
    assert_equal '2', row_2[5].find('input').value
    assert_equal '3', row_2[6].find('input').value
    assert_equal '1', row_2[7].find('input').value
    assert_equal '4', row_2[8].find('input').value
  end


  test 'headcount timezone consistency' do
    sign_in User.find(2)

    # Enter some new data
    for i in 1..4
      visit(stats_headcount_form_path(params: { date: "2010-0#{i}-01" }))
      # Fill in the 8 AM HCC Floor field
      fill_in(id: 'headcount_form_headcounts_8am_hccfloor', with: 5)
      page.find('#form-submit-save').click
    end

    # Make sure data when viewed in dashboard has correct timezones
    visit(stats_headcounts_path(
            params: {
              start_time: 'January 1, 2010 12:00 AM',
              end_time: 'April 30, 2010 11:59 PM',
              interval: 'hours',
              grouping: 'location'
            }
    ))

    assert_text '2010-01-01 (Fri) 08:00 0 5 0 0'
    assert_text '2010-02-01 (Mon) 08:00 0 5 0 0'
    assert_text '2010-03-01 (Mon) 08:00 0 5 0 0'
    assert_text '2010-04-01 (Thu) 08:00 0 5 0 0'
  end


end
