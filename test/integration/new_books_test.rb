require 'test_helper'

class NewBooksTest < ActionDispatch::IntegrationTest
  Capybara.register_driver :poltergeist do |app|
    Capybara::Poltergeist::Driver.new(app, {js_errors: false})
  end
  
  test 'index' do
    Capybara.register_driver :poltergeist do |app|
        Capybara::Poltergeist::Driver.new(app, {js_errors: false})
    end
    travel_to(Chronic.parse('2014-10-01 8:00 AM'))

    visit(new_books_path)
    assert page.assert_text('Showing 1 - 25 of 101 new items.')
    assert page.assert_selector('.new-book', count: 25)

    visit(new_books_path(page: 3))
    assert page.assert_text('Showing 51 - 75 of 101 new items.')
    assert page.assert_selector('.new-book', count: 25)

    visit(new_books_path(timeframe: '2014-10'))
    assert page.assert_text('Showing 1 - 25 of 91 new items.')
    assert page.assert_selector('.new-book', count: 25)

    visit(new_books_path(subject: 'history'))
    assert page.assert_text('Showing 1 - 4 of 4 new items.')
    assert page.assert_selector('.new-book', count: 4)

    visit(new_books_path(range: 124))
    assert page.assert_text('Showing 1 - 4 of 4 new items.')
    assert page.assert_selector('.new-book', count: 4)

    travel_back
  end

  test 'mappings' do
    visit(mappings_new_books_path)
    assert page.find_all('.subject').length >= 10
    assert page.find_all('.range').length >= 10
  end
end
