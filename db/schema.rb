# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20191227000000) do

  create_table "absence_dates", force: :cascade do |t|
    t.integer  "absence_id"
    t.date     "absence_date"
    t.time     "start_time"
    t.time     "end_time"
    t.integer  "is_all_day",               limit: 1
    t.integer  "duration_minutes"
    t.text     "google_calendar_event_id", limit: 65535
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "absence_types", force: :cascade do |t|
    t.string  "absence_type_name"
    t.string  "display_name"
    t.integer "sort"
  end

  create_table "absences", force: :cascade do |t|
    t.integer  "employee_id"
    t.integer  "submitter_id"
    t.integer  "editor_id"
    t.integer  "absence_type_id"
    t.text     "background_information", limit: 65535
    t.string   "professional_funding"
    t.boolean  "approved"
    t.boolean  "note_required"
    t.boolean  "note_received"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "reject_note"
    t.integer  "approver_id"
  end

  create_table "call_number_ranges", force: :cascade do |t|
    t.text    "range_start",     limit: 65535
    t.text    "range_end",       limit: 65535
    t.text    "range_name",      limit: 65535
    t.integer "subject_area_id"
    t.index ["subject_area_id"], name: "range_feed_idx"
  end

  create_table "circulation_transactions", force: :cascade do |t|
    t.bigint   "transaction_id"
    t.datetime "transaction_gmt"
    t.string   "application_name"
    t.string   "source_code"
    t.string   "op_code"
    t.bigint   "item_record_id"
    t.bigint   "bib_record_id"
    t.integer  "stat_group_code_num"
    t.text     "best_title_norm",          limit: 65535
    t.string   "best_author_norm"
    t.string   "item_location_code"
    t.string   "call_number"
    t.integer  "itype_code"
    t.string   "itype_name"
    t.integer  "ptype_code"
    t.string   "ptype_name"
    t.integer  "patron_checkout_total"
    t.string   "patron_home_library_code"
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable"
    t.index ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type"
  end

  create_table "clf_book_order_links", force: :cascade do |t|
    t.integer  "book_id"
    t.integer  "order_id"
    t.integer  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "clf_books", force: :cascade do |t|
    t.string   "title"
    t.string   "primary_author"
    t.string   "secondary_author"
    t.string   "illustrator"
    t.string   "isbn"
    t.decimal  "price",                          precision: 10, scale: 2
    t.integer  "festival_year"
    t.text     "summary",          limit: 65535
    t.text     "image_url",        limit: 65535
    t.text     "info_page_url",    limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "grade_level"
  end

  create_table "clf_orders", force: :cascade do |t|
    t.string   "school_name"
    t.string   "contact_name"
    t.string   "contact_email"
    t.string   "contact_phone"
    t.boolean  "tax_exempt"
    t.string   "tax_exempt_number"
    t.text     "comment",           limit: 65535
    t.string   "session_id"
    t.boolean  "is_submitted"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "computer_build_software_links", force: :cascade do |t|
    t.integer "build_id"
    t.integer "software_id"
  end

  create_table "computer_builds", force: :cascade do |t|
    t.string  "build_name"
    t.string  "description"
    t.integer "computer_operating_system_id"
  end

  create_table "computer_locations", force: :cascade do |t|
    t.integer  "computer_id"
    t.integer  "location_id"
    t.datetime "start_date"
    t.datetime "end_date"
  end

  create_table "computer_operating_systems", force: :cascade do |t|
    t.string "os_family_name"
    t.string "os_version_name"
  end

  create_table "computer_sessions", force: :cascade do |t|
    t.integer  "computer_id"
    t.integer  "ping_count"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["computer_id"], name: "index_computer_sessions_on_computer_id"
    t.index ["created_at"], name: "index_computer_sessions_on_created_at"
    t.index ["updated_at"], name: "index_computer_sessions_on_updated_at"
  end

  create_table "computer_software_links", force: :cascade do |t|
    t.integer "computer_id"
    t.integer "software_id"
  end

  create_table "computer_softwares", force: :cascade do |t|
    t.string "display_name"
    t.string "description"
    t.string "version"
  end

  create_table "computers", force: :cascade do |t|
    t.text    "map_tag",           limit: 65535
    t.text    "ip_address",        limit: 65535
    t.text    "computer_name",     limit: 65535
    t.integer "computer_build_id"
    t.integer "active",            limit: 1
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",                      default: 0, null: false
    t.integer  "attempts",                      default: 0, null: false
    t.text     "handler",    limit: 4294967295,             null: false
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "departments", force: :cascade do |t|
    t.text    "department_name",      limit: 65535
    t.string  "display_name"
    t.integer "department_head_id"
    t.integer "parent_department_id"
  end

  create_table "dvd_genre_links", force: :cascade do |t|
    t.integer "dvd_id"
    t.integer "dvd_genre_id"
  end

  create_table "dvd_genres", force: :cascade do |t|
    t.text    "display_name", limit: 65535
    t.integer "tmdb_id"
  end

  create_table "dvds", force: :cascade do |t|
    t.text     "title",                  limit: 65535
    t.string   "subtitle"
    t.string   "title_sort"
    t.string   "copyright_date"
    t.text     "summary",                limit: 65535
    t.string   "image_url"
    t.string   "imdb_id"
    t.string   "tmdb_id"
    t.string   "call_number"
    t.string   "volume"
    t.integer  "location_code_id"
    t.decimal  "rating",                               precision: 3, scale: 1
    t.string   "runtime"
    t.string   "language"
    t.integer  "checkouts"
    t.integer  "ytd_checkouts"
    t.integer  "last_ytd_checkouts"
    t.boolean  "available"
    t.date     "cdate"
    t.bigint   "item_record_id"
    t.bigint   "bib_record_id"
    t.integer  "item_record_num"
    t.integer  "bib_record_num"
    t.boolean  "is_marc_data_harvested"
    t.boolean  "is_tmdb_data_harvested"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "employees", force: :cascade do |t|
    t.text    "first_name",             limit: 65535
    t.text    "last_name",              limit: 65535
    t.text    "email",                  limit: 65535
    t.text    "phone",                  limit: 65535
    t.text    "office",                 limit: 65535
    t.text    "title",                  limit: 65535
    t.text    "rank",                   limit: 65535
    t.integer "is_faculty",             limit: 1
    t.integer "supervisor_id"
    t.integer "department_id"
    t.text    "image_name",             limit: 65535
    t.text    "libguides_profile_url",  limit: 65535
    t.integer "hide_from_public_views", limit: 1
  end

  create_table "file_uploads", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "file_name"
    t.string   "friendly_url"
    t.boolean  "is_public"
    t.boolean  "is_visible_to_employees"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  create_table "fund_code_librarian_links", force: :cascade do |t|
    t.integer "librarian_id"
    t.integer "fund_code_id"
  end

  create_table "fund_codes", force: :cascade do |t|
    t.string "code"
    t.text   "program", limit: 65535
  end

  create_table "headcount_entries", force: :cascade do |t|
    t.integer  "location_id"
    t.integer  "headcount"
    t.datetime "count_time"
  end

  create_table "horses_in_culture_titles", force: :cascade do |t|
    t.string   "title"
    t.integer  "publication_date"
    t.string   "publisher"
    t.string   "publication_place"
    t.integer  "number_pages"
    t.string   "illustrations"
    t.string   "equestrian_discipline"
    t.string   "breed"
    t.string   "ethnicity"
    t.string   "notes"
    t.string   "subjects"
    t.string   "author"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "hours", force: :cascade do |t|
    t.datetime "hour"
  end

  create_table "item_records", force: :cascade do |t|
    t.bigint   "item_record_id"
    t.bigint   "bib_record_id"
    t.bigint   "bib_record_num"
    t.text     "best_title_norm",                  limit: 65535
    t.text     "best_author_norm",                 limit: 65535
    t.integer  "publish_year"
    t.string   "location_code"
    t.string   "call_number"
    t.text     "isns",                             limit: 65535
    t.string   "barcode"
    t.string   "material_code"
    t.string   "itype_code_num"
    t.string   "item_status_code"
    t.decimal  "price",                                          precision: 10
    t.integer  "checkout_total"
    t.integer  "renewal_total"
    t.integer  "internal_use_count"
    t.integer  "year_to_date_checkout_total"
    t.integer  "last_year_to_date_checkout_total"
    t.datetime "inventory_gmt"
    t.datetime "last_checkout_gmt"
    t.datetime "item_record_creation_date_gmt"
    t.datetime "bib_record_creation_date_gmt"
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
  end

  create_table "library_absence_forms", force: :cascade do |t|
    t.integer  "employee_id"
    t.integer  "supervisor_id"
    t.date     "start_date"
    t.date     "end_date"
    t.text     "reason",                limit: 65535
    t.text     "professional_funding",  limit: 65535
    t.text     "additional_info",       limit: 65535
    t.integer  "submitter_id"
    t.datetime "submitted"
    t.boolean  "is_amended_submission"
    t.boolean  "unknown_duration"
    t.string   "duration"
    t.string   "absence_time"
  end

  create_table "library_hours", force: :cascade do |t|
    t.date     "calendar_date"
    t.datetime "open_time"
    t.datetime "close_time"
    t.string   "hours_display_text"
    t.integer  "total_calendar_date_minutes"
    t.integer  "total_operating_date_minutes"
    t.integer  "hour00minutes"
    t.integer  "hour01minutes"
    t.integer  "hour02minutes"
    t.integer  "hour03minutes"
    t.integer  "hour04minutes"
    t.integer  "hour05minutes"
    t.integer  "hour06minutes"
    t.integer  "hour07minutes"
    t.integer  "hour08minutes"
    t.integer  "hour09minutes"
    t.integer  "hour10minutes"
    t.integer  "hour11minutes"
    t.integer  "hour12minutes"
    t.integer  "hour13minutes"
    t.integer  "hour14minutes"
    t.integer  "hour15minutes"
    t.integer  "hour16minutes"
    t.integer  "hour17minutes"
    t.integer  "hour18minutes"
    t.integer  "hour19minutes"
    t.integer  "hour20minutes"
    t.integer  "hour21minutes"
    t.integer  "hour22minutes"
    t.integer  "hour23minutes"
  end

  create_table "location_codes", force: :cascade do |t|
    t.text "location_code", limit: 65535
    t.text "location_name", limit: 65535
  end

  create_table "locations", force: :cascade do |t|
    t.string   "location_name"
    t.string   "display_name"
    t.integer  "floor"
    t.integer  "sort"
    t.datetime "stat_form_start_date"
    t.datetime "stat_form_end_date"
  end

  create_table "meeting_room_bookings", force: :cascade do |t|
    t.text     "requestor_name",  limit: 65535
    t.text     "requestor_email", limit: 65535
    t.text     "contact_name",    limit: 65535
    t.text     "contact_email",   limit: 65535
    t.text     "contact_phone",   limit: 65535
    t.text     "contact_dept",    limit: 65535
    t.text     "contact_address", limit: 65535
    t.integer  "meeting_room_id"
    t.datetime "start_time"
    t.datetime "end_time"
    t.text     "event_name",      limit: 65535
    t.text     "setup_time",      limit: 65535
    t.text     "takedown_time",   limit: 65535
    t.text     "recurrence",      limit: 65535
    t.text     "attendance",      limit: 65535
    t.text     "notes",           limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["meeting_room_id"], name: "map_meeting_room_bookings_map_meeting_rooms_idx"
  end

  create_table "meeting_rooms", force: :cascade do |t|
    t.text     "room_name",          limit: 65535
    t.text     "display_name",       limit: 65535
    t.text     "description",        limit: 65535
    t.text     "google_calendar_id", limit: 65535
    t.integer  "floor"
    t.integer  "library_only",       limit: 1
    t.integer  "faculty_staff_only", limit: 1
    t.integer  "classroom",          limit: 1
    t.integer  "meetings",           limit: 1
    t.integer  "seating"
    t.integer  "desktops"
    t.integer  "laptops"
    t.integer  "projector",          limit: 1
    t.integer  "podium",             limit: 1
    t.integer  "whiteboard",         limit: 1
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.index ["floor"], name: "meeting_room_floor_idx"
  end

  create_table "navigation_menu_items", force: :cascade do |t|
    t.string   "url"
    t.string   "label"
    t.string   "element_class"
    t.string   "element_id"
    t.integer  "position"
    t.boolean  "show_in_footer"
    t.integer  "navigation_menu_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "new_book_requests", force: :cascade do |t|
    t.string   "requestor_name"
    t.string   "requestor_email"
    t.string   "format"
    t.string   "title"
    t.string   "edition"
    t.string   "author"
    t.string   "isbn"
    t.string   "publisher"
    t.string   "year"
    t.string   "series"
    t.string   "volumes"
    t.decimal  "list_price",                    precision: 10
    t.integer  "copies_ordered"
    t.string   "recommender"
    t.string   "approved_by"
    t.integer  "subject_area_id"
    t.text     "notes",           limit: 65535
    t.text     "submitted_by",    limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "fund_code_id"
    t.index ["subject_area_id"], name: "requests_subjects_idx"
  end

  create_table "new_books", force: :cascade do |t|
    t.text     "title",            limit: 65535
    t.text     "author",           limit: 65535
    t.text     "call_number",      limit: 65535
    t.text     "isns",             limit: 65535
    t.text     "year",             limit: 65535
    t.text     "summary",          limit: 65535
    t.integer  "location_code_id"
    t.text     "image_url",        limit: 65535
    t.text     "info_page_url",    limit: 65535
    t.bigint   "record_number"
    t.bigint   "item_record_id"
    t.bigint   "bib_record_id"
    t.datetime "cdate"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "new_books_subject_feeds", force: :cascade do |t|
    t.text    "feed_name",    limit: 65535
    t.text    "display_name", limit: 65535
    t.integer "sort_order"
  end

  create_table "news_items", force: :cascade do |t|
    t.string   "title"
    t.text     "teaser",                  limit: 65535
    t.text     "article",                 limit: 65535
    t.datetime "expiration_date"
    t.string   "friendly_url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "news_image_file_name"
    t.string   "news_image_content_type"
    t.integer  "news_image_file_size"
    t.datetime "news_image_updated_at"
  end

  create_table "offices", force: :cascade do |t|
    t.string  "display_name"
    t.string  "office_name"
    t.integer "department_id"
    t.string  "email"
    t.integer "primary_employee_id"
    t.string  "phone_number"
    t.string  "location"
    t.string  "webpage_url"
  end

  create_table "shelf_column_inventory_dates", force: :cascade do |t|
    t.integer  "shelf_column_id"
    t.datetime "date_inventoried"
    t.integer  "user_id"
  end

  create_table "shelf_columns", force: :cascade do |t|
    t.string "code"
  end

  create_table "special_messages", force: :cascade do |t|
    t.string   "title"
    t.text     "content",         limit: 65535
    t.datetime "expiration_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "study_room_bookings", force: :cascade do |t|
    t.integer  "study_room_id"
    t.string   "email"
    t.datetime "booking_start"
    t.integer  "booking_duration_minutes"
    t.string   "springshare_booking_id"
    t.datetime "booking_end"
  end

  create_table "study_rooms", force: :cascade do |t|
    t.text    "room_name",            limit: 65535
    t.text    "display_name",         limit: 65535
    t.integer "location_id"
    t.boolean "active"
    t.string  "springshare_space_id"
    t.index ["location_id"], name: "study_room_locations_idx"
  end

  create_table "subject_areas", force: :cascade do |t|
    t.string  "subject_name"
    t.string  "display_name"
    t.integer "subject_librarian_id"
    t.integer "parent_subject_id"
    t.text    "fund_code",            limit: 65535
    t.string  "scdp_url"
    t.integer "springshare_id"
    t.string  "scdp_name"
    t.index ["parent_subject_id"], name: "subject_parent_subject_idx"
  end

  create_table "user_roles", force: :cascade do |t|
    t.string  "name",    limit: 45
    t.integer "user_id"
    t.index ["name"], name: "user_roles_roles_idx"
    t.index ["user_id"], name: "user_roles_users_idx"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.integer  "employee_id"
    t.string   "email",                          default: "", null: false
    t.string   "encrypted_password",             default: "", null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                  default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "uid"
    t.string   "provider"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "n700",                limit: 45
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  create_table "website_contacts", force: :cascade do |t|
    t.text     "contact_name",       limit: 65535
    t.text     "contact_email",      limit: 65535
    t.text     "topic",              limit: 65535
    t.string   "recipients"
    t.text     "subject",            limit: 65535
    t.text     "message",            limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "contact_700_number"
  end

end
